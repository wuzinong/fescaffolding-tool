declare interface SvgComponent extends React.StatelessComponent<React.SVGAttributes<SVGElement>> {}

// Make TypeScript aware of .scss files, typing them as CSS modules.
declare module '*.scss' {
	const content: { [className: string]: string };
	export default content;
}

declare module '*.svg' {
	const content: SvgComponent;
	export default content;
}

declare module '*.png' {
	const content: string;
	export default content;
}
