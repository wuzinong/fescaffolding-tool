// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"../node_modules/tslib/tslib.es6.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.__extends = __extends;
exports.__rest = __rest;
exports.__decorate = __decorate;
exports.__param = __param;
exports.__metadata = __metadata;
exports.__awaiter = __awaiter;
exports.__generator = __generator;
exports.__exportStar = __exportStar;
exports.__values = __values;
exports.__read = __read;
exports.__spread = __spread;
exports.__spreadArrays = __spreadArrays;
exports.__spreadArray = __spreadArray;
exports.__await = __await;
exports.__asyncGenerator = __asyncGenerator;
exports.__asyncDelegator = __asyncDelegator;
exports.__asyncValues = __asyncValues;
exports.__makeTemplateObject = __makeTemplateObject;
exports.__importStar = __importStar;
exports.__importDefault = __importDefault;
exports.__classPrivateFieldGet = __classPrivateFieldGet;
exports.__classPrivateFieldSet = __classPrivateFieldSet;
exports.__createBinding = exports.__assign = void 0;

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

/* global Reflect, Promise */
var extendStatics = function (d, b) {
  extendStatics = Object.setPrototypeOf || {
    __proto__: []
  } instanceof Array && function (d, b) {
    d.__proto__ = b;
  } || function (d, b) {
    for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p];
  };

  return extendStatics(d, b);
};

function __extends(d, b) {
  if (typeof b !== "function" && b !== null) throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
  extendStatics(d, b);

  function __() {
    this.constructor = d;
  }

  d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function () {
  exports.__assign = __assign = Object.assign || function __assign(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];

      for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
    }

    return t;
  };

  return __assign.apply(this, arguments);
};

exports.__assign = __assign;

function __rest(s, e) {
  var t = {};

  for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];

  if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
    if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
  }
  return t;
}

function __decorate(decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
  return function (target, key) {
    decorator(target, key, paramIndex);
  };
}

function __metadata(metadataKey, metadataValue) {
  if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
}

function __generator(thisArg, body) {
  var _ = {
    label: 0,
    sent: function () {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) try {
      if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
      if (y = 0, t) op = [op[0] & 2, t.value];

      switch (op[0]) {
        case 0:
        case 1:
          t = op;
          break;

        case 4:
          _.label++;
          return {
            value: op[1],
            done: false
          };

        case 5:
          _.label++;
          y = op[1];
          op = [0];
          continue;

        case 7:
          op = _.ops.pop();

          _.trys.pop();

          continue;

        default:
          if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
            _ = 0;
            continue;
          }

          if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
            _.label = op[1];
            break;
          }

          if (op[0] === 6 && _.label < t[1]) {
            _.label = t[1];
            t = op;
            break;
          }

          if (t && _.label < t[2]) {
            _.label = t[2];

            _.ops.push(op);

            break;
          }

          if (t[2]) _.ops.pop();

          _.trys.pop();

          continue;
      }

      op = body.call(thisArg, _);
    } catch (e) {
      op = [6, e];
      y = 0;
    } finally {
      f = t = 0;
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
}

var __createBinding = Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function () {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
};

exports.__createBinding = __createBinding;

function __exportStar(m, o) {
  for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(o, p)) __createBinding(o, m, p);
}

function __values(o) {
  var s = typeof Symbol === "function" && Symbol.iterator,
      m = s && o[s],
      i = 0;
  if (m) return m.call(o);
  if (o && typeof o.length === "number") return {
    next: function () {
      if (o && i >= o.length) o = void 0;
      return {
        value: o && o[i++],
        done: !o
      };
    }
  };
  throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
  var m = typeof Symbol === "function" && o[Symbol.iterator];
  if (!m) return o;
  var i = m.call(o),
      r,
      ar = [],
      e;

  try {
    while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
  } catch (error) {
    e = {
      error: error
    };
  } finally {
    try {
      if (r && !r.done && (m = i["return"])) m.call(i);
    } finally {
      if (e) throw e.error;
    }
  }

  return ar;
}
/** @deprecated */


function __spread() {
  for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));

  return ar;
}
/** @deprecated */


function __spreadArrays() {
  for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;

  for (var r = Array(s), k = 0, i = 0; i < il; i++) for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) r[k] = a[j];

  return r;
}

function __spreadArray(to, from, pack) {
  if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
    if (ar || !(i in from)) {
      if (!ar) ar = Array.prototype.slice.call(from, 0, i);
      ar[i] = from[i];
    }
  }
  return to.concat(ar || from);
}

function __await(v) {
  return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
  if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
  var g = generator.apply(thisArg, _arguments || []),
      i,
      q = [];
  return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
    return this;
  }, i;

  function verb(n) {
    if (g[n]) i[n] = function (v) {
      return new Promise(function (a, b) {
        q.push([n, v, a, b]) > 1 || resume(n, v);
      });
    };
  }

  function resume(n, v) {
    try {
      step(g[n](v));
    } catch (e) {
      settle(q[0][3], e);
    }
  }

  function step(r) {
    r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
  }

  function fulfill(value) {
    resume("next", value);
  }

  function reject(value) {
    resume("throw", value);
  }

  function settle(f, v) {
    if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
  }
}

function __asyncDelegator(o) {
  var i, p;
  return i = {}, verb("next"), verb("throw", function (e) {
    throw e;
  }), verb("return"), i[Symbol.iterator] = function () {
    return this;
  }, i;

  function verb(n, f) {
    i[n] = o[n] ? function (v) {
      return (p = !p) ? {
        value: __await(o[n](v)),
        done: n === "return"
      } : f ? f(v) : v;
    } : f;
  }
}

function __asyncValues(o) {
  if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
  var m = o[Symbol.asyncIterator],
      i;
  return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
    return this;
  }, i);

  function verb(n) {
    i[n] = o[n] && function (v) {
      return new Promise(function (resolve, reject) {
        v = o[n](v), settle(resolve, reject, v.done, v.value);
      });
    };
  }

  function settle(resolve, reject, d, v) {
    Promise.resolve(v).then(function (v) {
      resolve({
        value: v,
        done: d
      });
    }, reject);
  }
}

function __makeTemplateObject(cooked, raw) {
  if (Object.defineProperty) {
    Object.defineProperty(cooked, "raw", {
      value: raw
    });
  } else {
    cooked.raw = raw;
  }

  return cooked;
}

;

var __setModuleDefault = Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
};

function __importStar(mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);

  __setModuleDefault(result, mod);

  return result;
}

function __importDefault(mod) {
  return mod && mod.__esModule ? mod : {
    default: mod
  };
}

function __classPrivateFieldGet(receiver, state, kind, f) {
  if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
  if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
  return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
}

function __classPrivateFieldSet(receiver, state, value, kind, f) {
  if (kind === "m") throw new TypeError("Private method is not writable");
  if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
  if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
  return kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value), value;
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/CanonicalizeLocaleList.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CanonicalizeLocaleList = CanonicalizeLocaleList;

/**
 * http://ecma-international.org/ecma-402/7.0/index.html#sec-canonicalizelocalelist
 * @param locales
 */
function CanonicalizeLocaleList(locales) {
  // TODO
  return Intl.getCanonicalLocales(locales);
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/CanonicalizeTimeZoneName.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CanonicalizeTimeZoneName = CanonicalizeTimeZoneName;

/**
 * https://tc39.es/ecma402/#sec-canonicalizetimezonename
 * @param tz
 */
function CanonicalizeTimeZoneName(tz, _a) {
  var tzData = _a.tzData,
      uppercaseLinks = _a.uppercaseLinks;
  var uppercasedTz = tz.toUpperCase();
  var uppercasedZones = Object.keys(tzData).reduce(function (all, z) {
    all[z.toUpperCase()] = z;
    return all;
  }, {});
  var ianaTimeZone = uppercaseLinks[uppercasedTz] || uppercasedZones[uppercasedTz];

  if (ianaTimeZone === 'Etc/UTC' || ianaTimeZone === 'Etc/GMT') {
    return 'UTC';
  }

  return ianaTimeZone;
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/262.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ToString = ToString;
exports.ToNumber = ToNumber;
exports.TimeClip = TimeClip;
exports.ToObject = ToObject;
exports.SameValue = SameValue;
exports.ArrayCreate = ArrayCreate;
exports.HasOwnProperty = HasOwnProperty;
exports.Type = Type;
exports.Day = Day;
exports.WeekDay = WeekDay;
exports.DayFromYear = DayFromYear;
exports.TimeFromYear = TimeFromYear;
exports.YearFromTime = YearFromTime;
exports.DaysInYear = DaysInYear;
exports.DayWithinYear = DayWithinYear;
exports.InLeapYear = InLeapYear;
exports.MonthFromTime = MonthFromTime;
exports.DateFromTime = DateFromTime;
exports.HourFromTime = HourFromTime;
exports.MinFromTime = MinFromTime;
exports.SecFromTime = SecFromTime;
exports.OrdinaryHasInstance = OrdinaryHasInstance;
exports.msFromTime = msFromTime;

/**
 * https://tc39.es/ecma262/#sec-tostring
 */
function ToString(o) {
  // Only symbol is irregular...
  if (typeof o === 'symbol') {
    throw TypeError('Cannot convert a Symbol value to a string');
  }

  return String(o);
}
/**
 * https://tc39.es/ecma262/#sec-tonumber
 * @param val
 */


function ToNumber(val) {
  if (val === undefined) {
    return NaN;
  }

  if (val === null) {
    return +0;
  }

  if (typeof val === 'boolean') {
    return val ? 1 : +0;
  }

  if (typeof val === 'number') {
    return val;
  }

  if (typeof val === 'symbol' || typeof val === 'bigint') {
    throw new TypeError('Cannot convert symbol/bigint to number');
  }

  return Number(val);
}
/**
 * https://tc39.es/ecma262/#sec-tointeger
 * @param n
 */


function ToInteger(n) {
  var number = ToNumber(n);

  if (isNaN(number) || SameValue(number, -0)) {
    return 0;
  }

  if (isFinite(number)) {
    return number;
  }

  var integer = Math.floor(Math.abs(number));

  if (number < 0) {
    integer = -integer;
  }

  if (SameValue(integer, -0)) {
    return 0;
  }

  return integer;
}
/**
 * https://tc39.es/ecma262/#sec-timeclip
 * @param time
 */


function TimeClip(time) {
  if (!isFinite(time)) {
    return NaN;
  }

  if (Math.abs(time) > 8.64 * 1e15) {
    return NaN;
  }

  return ToInteger(time);
}
/**
 * https://tc39.es/ecma262/#sec-toobject
 * @param arg
 */


function ToObject(arg) {
  if (arg == null) {
    throw new TypeError('undefined/null cannot be converted to object');
  }

  return Object(arg);
}
/**
 * https://www.ecma-international.org/ecma-262/11.0/index.html#sec-samevalue
 * @param x
 * @param y
 */


function SameValue(x, y) {
  if (Object.is) {
    return Object.is(x, y);
  } // SameValue algorithm


  if (x === y) {
    // Steps 1-5, 7-10
    // Steps 6.b-6.e: +0 != -0
    return x !== 0 || 1 / x === 1 / y;
  } // Step 6.a: NaN == NaN


  return x !== x && y !== y;
}
/**
 * https://www.ecma-international.org/ecma-262/11.0/index.html#sec-arraycreate
 * @param len
 */


function ArrayCreate(len) {
  return new Array(len);
}
/**
 * https://www.ecma-international.org/ecma-262/11.0/index.html#sec-hasownproperty
 * @param o
 * @param prop
 */


function HasOwnProperty(o, prop) {
  return Object.prototype.hasOwnProperty.call(o, prop);
}
/**
 * https://www.ecma-international.org/ecma-262/11.0/index.html#sec-type
 * @param x
 */


function Type(x) {
  if (x === null) {
    return 'Null';
  }

  if (typeof x === 'undefined') {
    return 'Undefined';
  }

  if (typeof x === 'function' || typeof x === 'object') {
    return 'Object';
  }

  if (typeof x === 'number') {
    return 'Number';
  }

  if (typeof x === 'boolean') {
    return 'Boolean';
  }

  if (typeof x === 'string') {
    return 'String';
  }

  if (typeof x === 'symbol') {
    return 'Symbol';
  }

  if (typeof x === 'bigint') {
    return 'BigInt';
  }
}

var MS_PER_DAY = 86400000;
/**
 * https://www.ecma-international.org/ecma-262/11.0/index.html#eqn-modulo
 * @param x
 * @param y
 * @return k of the same sign as y
 */

function mod(x, y) {
  return x - Math.floor(x / y) * y;
}
/**
 * https://tc39.es/ecma262/#eqn-Day
 * @param t
 */


function Day(t) {
  return Math.floor(t / MS_PER_DAY);
}
/**
 * https://tc39.es/ecma262/#sec-week-day
 * @param t
 */


function WeekDay(t) {
  return mod(Day(t) + 4, 7);
}
/**
 * https://tc39.es/ecma262/#sec-year-number
 * @param y
 */


function DayFromYear(y) {
  return Date.UTC(y, 0) / MS_PER_DAY;
}
/**
 * https://tc39.es/ecma262/#sec-year-number
 * @param y
 */


function TimeFromYear(y) {
  return Date.UTC(y, 0);
}
/**
 * https://tc39.es/ecma262/#sec-year-number
 * @param t
 */


function YearFromTime(t) {
  return new Date(t).getUTCFullYear();
}

function DaysInYear(y) {
  if (y % 4 !== 0) {
    return 365;
  }

  if (y % 100 !== 0) {
    return 366;
  }

  if (y % 400 !== 0) {
    return 365;
  }

  return 366;
}

function DayWithinYear(t) {
  return Day(t) - DayFromYear(YearFromTime(t));
}

function InLeapYear(t) {
  return DaysInYear(YearFromTime(t)) === 365 ? 0 : 1;
}
/**
 * https://tc39.es/ecma262/#sec-month-number
 * @param t
 */


function MonthFromTime(t) {
  var dwy = DayWithinYear(t);
  var leap = InLeapYear(t);

  if (dwy >= 0 && dwy < 31) {
    return 0;
  }

  if (dwy < 59 + leap) {
    return 1;
  }

  if (dwy < 90 + leap) {
    return 2;
  }

  if (dwy < 120 + leap) {
    return 3;
  }

  if (dwy < 151 + leap) {
    return 4;
  }

  if (dwy < 181 + leap) {
    return 5;
  }

  if (dwy < 212 + leap) {
    return 6;
  }

  if (dwy < 243 + leap) {
    return 7;
  }

  if (dwy < 273 + leap) {
    return 8;
  }

  if (dwy < 304 + leap) {
    return 9;
  }

  if (dwy < 334 + leap) {
    return 10;
  }

  if (dwy < 365 + leap) {
    return 11;
  }

  throw new Error('Invalid time');
}

function DateFromTime(t) {
  var dwy = DayWithinYear(t);
  var mft = MonthFromTime(t);
  var leap = InLeapYear(t);

  if (mft === 0) {
    return dwy + 1;
  }

  if (mft === 1) {
    return dwy - 30;
  }

  if (mft === 2) {
    return dwy - 58 - leap;
  }

  if (mft === 3) {
    return dwy - 89 - leap;
  }

  if (mft === 4) {
    return dwy - 119 - leap;
  }

  if (mft === 5) {
    return dwy - 150 - leap;
  }

  if (mft === 6) {
    return dwy - 180 - leap;
  }

  if (mft === 7) {
    return dwy - 211 - leap;
  }

  if (mft === 8) {
    return dwy - 242 - leap;
  }

  if (mft === 9) {
    return dwy - 272 - leap;
  }

  if (mft === 10) {
    return dwy - 303 - leap;
  }

  if (mft === 11) {
    return dwy - 333 - leap;
  }

  throw new Error('Invalid time');
}

var HOURS_PER_DAY = 24;
var MINUTES_PER_HOUR = 60;
var SECONDS_PER_MINUTE = 60;
var MS_PER_SECOND = 1e3;
var MS_PER_MINUTE = MS_PER_SECOND * SECONDS_PER_MINUTE;
var MS_PER_HOUR = MS_PER_MINUTE * MINUTES_PER_HOUR;

function HourFromTime(t) {
  return mod(Math.floor(t / MS_PER_HOUR), HOURS_PER_DAY);
}

function MinFromTime(t) {
  return mod(Math.floor(t / MS_PER_MINUTE), MINUTES_PER_HOUR);
}

function SecFromTime(t) {
  return mod(Math.floor(t / MS_PER_SECOND), SECONDS_PER_MINUTE);
}

function IsCallable(fn) {
  return typeof fn === 'function';
}
/**
 * The abstract operation OrdinaryHasInstance implements
 * the default algorithm for determining if an object O
 * inherits from the instance object inheritance path
 * provided by constructor C.
 * @param C class
 * @param O object
 * @param internalSlots internalSlots
 */


function OrdinaryHasInstance(C, O, internalSlots) {
  if (!IsCallable(C)) {
    return false;
  }

  if (internalSlots === null || internalSlots === void 0 ? void 0 : internalSlots.boundTargetFunction) {
    var BC = internalSlots === null || internalSlots === void 0 ? void 0 : internalSlots.boundTargetFunction;
    return O instanceof BC;
  }

  if (typeof O !== 'object') {
    return false;
  }

  var P = C.prototype;

  if (typeof P !== 'object') {
    throw new TypeError('OrdinaryHasInstance called on an object with an invalid prototype property.');
  }

  return Object.prototype.isPrototypeOf.call(P, O);
}

function msFromTime(t) {
  return mod(t, MS_PER_SECOND);
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/CoerceOptionsToObject.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CoerceOptionsToObject = CoerceOptionsToObject;

var _ = require("./262");

/**
 * https://tc39.es/ecma402/#sec-coerceoptionstoobject
 * @param options
 * @returns
 */
function CoerceOptionsToObject(options) {
  if (typeof options === 'undefined') {
    return Object.create(null);
  }

  return (0, _.ToObject)(options);
}
},{"./262":"../node_modules/@formatjs/ecma402-abstract/lib/262.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/DefaultNumberOption.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DefaultNumberOption = DefaultNumberOption;

function DefaultNumberOption(val, min, max, fallback) {
  if (val !== undefined) {
    val = Number(val);

    if (isNaN(val) || val < min || val > max) {
      throw new RangeError(val + " is outside of range [" + min + ", " + max + "]");
    }

    return Math.floor(val);
  }

  return fallback;
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/GetNumberOption.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GetNumberOption = GetNumberOption;

var _DefaultNumberOption = require("./DefaultNumberOption");

/**
 * https://tc39.es/ecma402/#sec-getnumberoption
 * @param options
 * @param property
 * @param min
 * @param max
 * @param fallback
 */
function GetNumberOption(options, property, minimum, maximum, fallback) {
  var val = options[property]; // @ts-expect-error

  return (0, _DefaultNumberOption.DefaultNumberOption)(val, minimum, maximum, fallback);
}
},{"./DefaultNumberOption":"../node_modules/@formatjs/ecma402-abstract/lib/DefaultNumberOption.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/GetOption.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GetOption = GetOption;

var _ = require("./262");

/**
 * https://tc39.es/ecma402/#sec-getoption
 * @param opts
 * @param prop
 * @param type
 * @param values
 * @param fallback
 */
function GetOption(opts, prop, type, values, fallback) {
  if (typeof opts !== 'object') {
    throw new TypeError('Options must be an object');
  }

  var value = opts[prop];

  if (value !== undefined) {
    if (type !== 'boolean' && type !== 'string') {
      throw new TypeError('invalid type');
    }

    if (type === 'boolean') {
      value = Boolean(value);
    }

    if (type === 'string') {
      value = (0, _.ToString)(value);
    }

    if (values !== undefined && !values.filter(function (val) {
      return val == value;
    }).length) {
      throw new RangeError(value + " is not within " + values.join(', '));
    }

    return value;
  }

  return fallback;
}
},{"./262":"../node_modules/@formatjs/ecma402-abstract/lib/262.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/GetOptionsObject.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GetOptionsObject = GetOptionsObject;

/**
 * https://tc39.es/ecma402/#sec-getoptionsobject
 * @param options
 * @returns
 */
function GetOptionsObject(options) {
  if (typeof options === 'undefined') {
    return Object.create(null);
  }

  if (typeof options === 'object') {
    return options;
  }

  throw new TypeError('Options must be an object');
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/IsSanctionedSimpleUnitIdentifier.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.removeUnitNamespace = removeUnitNamespace;
exports.IsSanctionedSimpleUnitIdentifier = IsSanctionedSimpleUnitIdentifier;
exports.SIMPLE_UNITS = exports.SANCTIONED_UNITS = void 0;

/**
 * https://tc39.es/ecma402/#table-sanctioned-simple-unit-identifiers
 */
var SANCTIONED_UNITS = ['angle-degree', 'area-acre', 'area-hectare', 'concentr-percent', 'digital-bit', 'digital-byte', 'digital-gigabit', 'digital-gigabyte', 'digital-kilobit', 'digital-kilobyte', 'digital-megabit', 'digital-megabyte', 'digital-petabyte', 'digital-terabit', 'digital-terabyte', 'duration-day', 'duration-hour', 'duration-millisecond', 'duration-minute', 'duration-month', 'duration-second', 'duration-week', 'duration-year', 'length-centimeter', 'length-foot', 'length-inch', 'length-kilometer', 'length-meter', 'length-mile-scandinavian', 'length-mile', 'length-millimeter', 'length-yard', 'mass-gram', 'mass-kilogram', 'mass-ounce', 'mass-pound', 'mass-stone', 'temperature-celsius', 'temperature-fahrenheit', 'volume-fluid-ounce', 'volume-gallon', 'volume-liter', 'volume-milliliter']; // In CLDR, the unit name always follows the form `namespace-unit` pattern.
// For example: `digital-bit` instead of `bit`. This function removes the namespace prefix.

exports.SANCTIONED_UNITS = SANCTIONED_UNITS;

function removeUnitNamespace(unit) {
  return unit.slice(unit.indexOf('-') + 1);
}
/**
 * https://tc39.es/ecma402/#table-sanctioned-simple-unit-identifiers
 */


var SIMPLE_UNITS = SANCTIONED_UNITS.map(removeUnitNamespace);
/**
 * https://tc39.es/ecma402/#sec-issanctionedsimpleunitidentifier
 */

exports.SIMPLE_UNITS = SIMPLE_UNITS;

function IsSanctionedSimpleUnitIdentifier(unitIdentifier) {
  return SIMPLE_UNITS.indexOf(unitIdentifier) > -1;
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/IsValidTimeZoneName.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IsValidTimeZoneName = IsValidTimeZoneName;

/**
 * https://tc39.es/ecma402/#sec-isvalidtimezonename
 * @param tz
 * @param implDetails implementation details
 */
function IsValidTimeZoneName(tz, _a) {
  var tzData = _a.tzData,
      uppercaseLinks = _a.uppercaseLinks;
  var uppercasedTz = tz.toUpperCase();
  var zoneNames = new Set();
  var linkNames = new Set();
  Object.keys(tzData).map(function (z) {
    return z.toUpperCase();
  }).forEach(function (z) {
    return zoneNames.add(z);
  });
  Object.keys(uppercaseLinks).forEach(function (linkName) {
    linkNames.add(linkName.toUpperCase());
    zoneNames.add(uppercaseLinks[linkName].toUpperCase());
  });
  return zoneNames.has(uppercasedTz) || linkNames.has(uppercasedTz);
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/IsWellFormedCurrencyCode.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IsWellFormedCurrencyCode = IsWellFormedCurrencyCode;

/**
 * This follows https://tc39.es/ecma402/#sec-case-sensitivity-and-case-mapping
 * @param str string to convert
 */
function toUpperCase(str) {
  return str.replace(/([a-z])/g, function (_, c) {
    return c.toUpperCase();
  });
}

var NOT_A_Z_REGEX = /[^A-Z]/;
/**
 * https://tc39.es/ecma402/#sec-iswellformedcurrencycode
 */

function IsWellFormedCurrencyCode(currency) {
  currency = toUpperCase(currency);

  if (currency.length !== 3) {
    return false;
  }

  if (NOT_A_Z_REGEX.test(currency)) {
    return false;
  }

  return true;
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/IsWellFormedUnitIdentifier.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IsWellFormedUnitIdentifier = IsWellFormedUnitIdentifier;

var _IsSanctionedSimpleUnitIdentifier = require("./IsSanctionedSimpleUnitIdentifier");

/**
 * This follows https://tc39.es/ecma402/#sec-case-sensitivity-and-case-mapping
 * @param str string to convert
 */
function toLowerCase(str) {
  return str.replace(/([A-Z])/g, function (_, c) {
    return c.toLowerCase();
  });
}
/**
 * https://tc39.es/ecma402/#sec-iswellformedunitidentifier
 * @param unit
 */


function IsWellFormedUnitIdentifier(unit) {
  unit = toLowerCase(unit);

  if ((0, _IsSanctionedSimpleUnitIdentifier.IsSanctionedSimpleUnitIdentifier)(unit)) {
    return true;
  }

  var units = unit.split('-per-');

  if (units.length !== 2) {
    return false;
  }

  var numerator = units[0],
      denominator = units[1];

  if (!(0, _IsSanctionedSimpleUnitIdentifier.IsSanctionedSimpleUnitIdentifier)(numerator) || !(0, _IsSanctionedSimpleUnitIdentifier.IsSanctionedSimpleUnitIdentifier)(denominator)) {
    return false;
  }

  return true;
}
},{"./IsSanctionedSimpleUnitIdentifier":"../node_modules/@formatjs/ecma402-abstract/lib/IsSanctionedSimpleUnitIdentifier.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/utils.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMagnitude = getMagnitude;
exports.repeat = repeat;
exports.setInternalSlot = setInternalSlot;
exports.setMultiInternalSlots = setMultiInternalSlots;
exports.getInternalSlot = getInternalSlot;
exports.getMultiInternalSlots = getMultiInternalSlots;
exports.isLiteralPart = isLiteralPart;
exports.defineProperty = defineProperty;
exports.invariant = invariant;
exports.UNICODE_EXTENSION_SEQUENCE_REGEX = void 0;

/**
 * Cannot do Math.log(x) / Math.log(10) bc if IEEE floating point issue
 * @param x number
 */
function getMagnitude(x) {
  // Cannot count string length via Number.toString because it may use scientific notation
  // for very small or very large numbers.
  return Math.floor(Math.log(x) * Math.LOG10E);
}

function repeat(s, times) {
  if (typeof s.repeat === 'function') {
    return s.repeat(times);
  }

  var arr = new Array(times);

  for (var i = 0; i < arr.length; i++) {
    arr[i] = s;
  }

  return arr.join('');
}

function setInternalSlot(map, pl, field, value) {
  if (!map.get(pl)) {
    map.set(pl, Object.create(null));
  }

  var slots = map.get(pl);
  slots[field] = value;
}

function setMultiInternalSlots(map, pl, props) {
  for (var _i = 0, _a = Object.keys(props); _i < _a.length; _i++) {
    var k = _a[_i];
    setInternalSlot(map, pl, k, props[k]);
  }
}

function getInternalSlot(map, pl, field) {
  return getMultiInternalSlots(map, pl, field)[field];
}

function getMultiInternalSlots(map, pl) {
  var fields = [];

  for (var _i = 2; _i < arguments.length; _i++) {
    fields[_i - 2] = arguments[_i];
  }

  var slots = map.get(pl);

  if (!slots) {
    throw new TypeError(pl + " InternalSlot has not been initialized");
  }

  return fields.reduce(function (all, f) {
    all[f] = slots[f];
    return all;
  }, Object.create(null));
}

function isLiteralPart(patternPart) {
  return patternPart.type === 'literal';
}
/*
  17 ECMAScript Standard Built-in Objects:
    Every built-in Function object, including constructors, that is not
    identified as an anonymous function has a name property whose value
    is a String.

    Unless otherwise specified, the name property of a built-in Function
    object, if it exists, has the attributes { [[Writable]]: false,
    [[Enumerable]]: false, [[Configurable]]: true }.
*/


function defineProperty(target, name, _a) {
  var value = _a.value;
  Object.defineProperty(target, name, {
    configurable: true,
    enumerable: false,
    writable: true,
    value: value
  });
}

var UNICODE_EXTENSION_SEQUENCE_REGEX = /-u(?:-[0-9a-z]{2,8})+/gi;
exports.UNICODE_EXTENSION_SEQUENCE_REGEX = UNICODE_EXTENSION_SEQUENCE_REGEX;

function invariant(condition, message, Err) {
  if (Err === void 0) {
    Err = Error;
  }

  if (!condition) {
    throw new Err(message);
  }
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ComputeExponentForMagnitude.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ComputeExponentForMagnitude = ComputeExponentForMagnitude;

/**
 * The abstract operation ComputeExponentForMagnitude computes an exponent by which to scale a
 * number of the given magnitude (power of ten of the most significant digit) according to the
 * locale and the desired notation (scientific, engineering, or compact).
 */
function ComputeExponentForMagnitude(numberFormat, magnitude, _a) {
  var getInternalSlots = _a.getInternalSlots;
  var internalSlots = getInternalSlots(numberFormat);
  var notation = internalSlots.notation,
      dataLocaleData = internalSlots.dataLocaleData,
      numberingSystem = internalSlots.numberingSystem;

  switch (notation) {
    case 'standard':
      return 0;

    case 'scientific':
      return magnitude;

    case 'engineering':
      return Math.floor(magnitude / 3) * 3;

    default:
      {
        // Let exponent be an implementation- and locale-dependent (ILD) integer by which to scale a
        // number of the given magnitude in compact notation for the current locale.
        var compactDisplay = internalSlots.compactDisplay,
            style = internalSlots.style,
            currencyDisplay = internalSlots.currencyDisplay;
        var thresholdMap = void 0;

        if (style === 'currency' && currencyDisplay !== 'name') {
          var currency = dataLocaleData.numbers.currency[numberingSystem] || dataLocaleData.numbers.currency[dataLocaleData.numbers.nu[0]];
          thresholdMap = currency.short;
        } else {
          var decimal = dataLocaleData.numbers.decimal[numberingSystem] || dataLocaleData.numbers.decimal[dataLocaleData.numbers.nu[0]];
          thresholdMap = compactDisplay === 'long' ? decimal.long : decimal.short;
        }

        if (!thresholdMap) {
          return 0;
        }

        var num = String(Math.pow(10, magnitude));
        var thresholds = Object.keys(thresholdMap); // TODO: this can be pre-processed

        if (num < thresholds[0]) {
          return 0;
        }

        if (num > thresholds[thresholds.length - 1]) {
          return thresholds[thresholds.length - 1].length - 1;
        }

        var i = thresholds.indexOf(num);

        if (i === -1) {
          return 0;
        } // See https://unicode.org/reports/tr35/tr35-numbers.html#Compact_Number_Formats
        // Special handling if the pattern is precisely `0`.


        var magnitudeKey = thresholds[i]; // TODO: do we need to handle plural here?

        var compactPattern = thresholdMap[magnitudeKey].other;

        if (compactPattern === '0') {
          return 0;
        } // Example: in zh-TW, `10000000` maps to `0000萬`. So we need to return 8 - 4 = 4 here.


        return magnitudeKey.length - thresholdMap[magnitudeKey].other.match(/0+/)[0].length;
      }
  }
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ToRawPrecision.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ToRawPrecision = ToRawPrecision;

var _utils = require("../utils");

function ToRawPrecision(x, minPrecision, maxPrecision) {
  var p = maxPrecision;
  var m;
  var e;
  var xFinal;

  if (x === 0) {
    m = (0, _utils.repeat)('0', p);
    e = 0;
    xFinal = 0;
  } else {
    var xToString = x.toString(); // If xToString is formatted as scientific notation, the number is either very small or very
    // large. If the precision of the formatted string is lower that requested max precision, we
    // should still infer them from the formatted string, otherwise the formatted result might have
    // precision loss (e.g. 1e41 will not have 0 in every trailing digits).

    var xToStringExponentIndex = xToString.indexOf('e');

    var _a = xToString.split('e'),
        xToStringMantissa = _a[0],
        xToStringExponent = _a[1];

    var xToStringMantissaWithoutDecimalPoint = xToStringMantissa.replace('.', '');

    if (xToStringExponentIndex >= 0 && xToStringMantissaWithoutDecimalPoint.length <= p) {
      e = +xToStringExponent;
      m = xToStringMantissaWithoutDecimalPoint + (0, _utils.repeat)('0', p - xToStringMantissaWithoutDecimalPoint.length);
      xFinal = x;
    } else {
      e = (0, _utils.getMagnitude)(x);
      var decimalPlaceOffset = e - p + 1; // n is the integer containing the required precision digits. To derive the formatted string,
      // we will adjust its decimal place in the logic below.

      var n = Math.round(adjustDecimalPlace(x, decimalPlaceOffset)); // The rounding caused the change of magnitude, so we should increment `e` by 1.

      if (adjustDecimalPlace(n, p - 1) >= 10) {
        e = e + 1; // Divide n by 10 to swallow one precision.

        n = Math.floor(n / 10);
      }

      m = n.toString(); // Equivalent of n * 10 ** (e - p + 1)

      xFinal = adjustDecimalPlace(n, p - 1 - e);
    }
  }

  var int;

  if (e >= p - 1) {
    m = m + (0, _utils.repeat)('0', e - p + 1);
    int = e + 1;
  } else if (e >= 0) {
    m = m.slice(0, e + 1) + "." + m.slice(e + 1);
    int = e + 1;
  } else {
    m = "0." + (0, _utils.repeat)('0', -e - 1) + m;
    int = 1;
  }

  if (m.indexOf('.') >= 0 && maxPrecision > minPrecision) {
    var cut = maxPrecision - minPrecision;

    while (cut > 0 && m[m.length - 1] === '0') {
      m = m.slice(0, -1);
      cut--;
    }

    if (m[m.length - 1] === '.') {
      m = m.slice(0, -1);
    }
  }

  return {
    formattedString: m,
    roundedNumber: xFinal,
    integerDigitsCount: int
  }; // x / (10 ** magnitude), but try to preserve as much floating point precision as possible.

  function adjustDecimalPlace(x, magnitude) {
    return magnitude < 0 ? x * Math.pow(10, -magnitude) : x / Math.pow(10, magnitude);
  }
}
},{"../utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ToRawFixed.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ToRawFixed = ToRawFixed;

var _utils = require("../utils");

/**
 * TODO: dedup with intl-pluralrules and support BigInt
 * https://tc39.es/ecma402/#sec-torawfixed
 * @param x a finite non-negative Number or BigInt
 * @param minFraction and integer between 0 and 20
 * @param maxFraction and integer between 0 and 20
 */
function ToRawFixed(x, minFraction, maxFraction) {
  var f = maxFraction;
  var n = Math.round(x * Math.pow(10, f));
  var xFinal = n / Math.pow(10, f); // n is a positive integer, but it is possible to be greater than 1e21.
  // In such case we will go the slow path.
  // See also: https://tc39.es/ecma262/#sec-numeric-types-number-tostring

  var m;

  if (n < 1e21) {
    m = n.toString();
  } else {
    m = n.toString();

    var _a = m.split('e'),
        mantissa = _a[0],
        exponent = _a[1];

    m = mantissa.replace('.', '');
    m = m + (0, _utils.repeat)('0', Math.max(+exponent - m.length + 1, 0));
  }

  var int;

  if (f !== 0) {
    var k = m.length;

    if (k <= f) {
      var z = (0, _utils.repeat)('0', f + 1 - k);
      m = z + m;
      k = f + 1;
    }

    var a = m.slice(0, k - f);
    var b = m.slice(k - f);
    m = a + "." + b;
    int = a.length;
  } else {
    int = m.length;
  }

  var cut = maxFraction - minFraction;

  while (cut > 0 && m[m.length - 1] === '0') {
    m = m.slice(0, -1);
    cut--;
  }

  if (m[m.length - 1] === '.') {
    m = m.slice(0, -1);
  }

  return {
    formattedString: m,
    roundedNumber: xFinal,
    integerDigitsCount: int
  };
}
},{"../utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/FormatNumericToString.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormatNumericToString = FormatNumericToString;

var _ = require("../262");

var _ToRawPrecision = require("./ToRawPrecision");

var _utils = require("../utils");

var _ToRawFixed = require("./ToRawFixed");

/**
 * https://tc39.es/ecma402/#sec-formatnumberstring
 */
function FormatNumericToString(intlObject, x) {
  var isNegative = x < 0 || (0, _.SameValue)(x, -0);

  if (isNegative) {
    x = -x;
  }

  var result;
  var rourndingType = intlObject.roundingType;

  switch (rourndingType) {
    case 'significantDigits':
      result = (0, _ToRawPrecision.ToRawPrecision)(x, intlObject.minimumSignificantDigits, intlObject.maximumSignificantDigits);
      break;

    case 'fractionDigits':
      result = (0, _ToRawFixed.ToRawFixed)(x, intlObject.minimumFractionDigits, intlObject.maximumFractionDigits);
      break;

    default:
      result = (0, _ToRawPrecision.ToRawPrecision)(x, 1, 2);

      if (result.integerDigitsCount > 1) {
        result = (0, _ToRawFixed.ToRawFixed)(x, 0, 0);
      }

      break;
  }

  x = result.roundedNumber;
  var string = result.formattedString;
  var int = result.integerDigitsCount;
  var minInteger = intlObject.minimumIntegerDigits;

  if (int < minInteger) {
    var forwardZeros = (0, _utils.repeat)('0', minInteger - int);
    string = forwardZeros + string;
  }

  if (isNegative) {
    x = -x;
  }

  return {
    roundedNumber: x,
    formattedString: string
  };
}
},{"../262":"../node_modules/@formatjs/ecma402-abstract/lib/262.js","./ToRawPrecision":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ToRawPrecision.js","../utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js","./ToRawFixed":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ToRawFixed.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ComputeExponent.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ComputeExponent = ComputeExponent;

var _utils = require("../utils");

var _ComputeExponentForMagnitude = require("./ComputeExponentForMagnitude");

var _FormatNumericToString = require("./FormatNumericToString");

/**
 * The abstract operation ComputeExponent computes an exponent (power of ten) by which to scale x
 * according to the number formatting settings. It handles cases such as 999 rounding up to 1000,
 * requiring a different exponent.
 *
 * NOT IN SPEC: it returns [exponent, magnitude].
 */
function ComputeExponent(numberFormat, x, _a) {
  var getInternalSlots = _a.getInternalSlots;

  if (x === 0) {
    return [0, 0];
  }

  if (x < 0) {
    x = -x;
  }

  var magnitude = (0, _utils.getMagnitude)(x);
  var exponent = (0, _ComputeExponentForMagnitude.ComputeExponentForMagnitude)(numberFormat, magnitude, {
    getInternalSlots: getInternalSlots
  }); // Preserve more precision by doing multiplication when exponent is negative.

  x = exponent < 0 ? x * Math.pow(10, -exponent) : x / Math.pow(10, exponent);
  var formatNumberResult = (0, _FormatNumericToString.FormatNumericToString)(getInternalSlots(numberFormat), x);

  if (formatNumberResult.roundedNumber === 0) {
    return [exponent, magnitude];
  }

  var newMagnitude = (0, _utils.getMagnitude)(formatNumberResult.roundedNumber);

  if (newMagnitude === magnitude - exponent) {
    return [exponent, magnitude];
  }

  return [(0, _ComputeExponentForMagnitude.ComputeExponentForMagnitude)(numberFormat, magnitude + 1, {
    getInternalSlots: getInternalSlots
  }), magnitude + 1];
}
},{"../utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js","./ComputeExponentForMagnitude":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ComputeExponentForMagnitude.js","./FormatNumericToString":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/FormatNumericToString.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/CurrencyDigits.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CurrencyDigits = CurrencyDigits;

var _ = require("../262");

/**
 * https://tc39.es/ecma402/#sec-currencydigits
 */
function CurrencyDigits(c, _a) {
  var currencyDigitsData = _a.currencyDigitsData;
  return (0, _.HasOwnProperty)(currencyDigitsData, c) ? currencyDigitsData[c] : 2;
}
},{"../262":"../node_modules/@formatjs/ecma402-abstract/lib/262.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/digit-mapping.json":[function(require,module,exports) {
module.exports = { "adlm": ["𞥐", "𞥑", "𞥒", "𞥓", "𞥔", "𞥕", "𞥖", "𞥗", "𞥘", "𞥙"], "ahom": ["𑜰", "𑜱", "𑜲", "𑜳", "𑜴", "𑜵", "𑜶", "𑜷", "𑜸", "𑜹"], "arab": ["٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"], "arabext": ["۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹"], "bali": ["᭐", "᭑", "᭒", "᭓", "᭔", "᭕", "᭖", "᭗", "᭘", "᭙"], "beng": ["০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯"], "bhks": ["𑱐", "𑱑", "𑱒", "𑱓", "𑱔", "𑱕", "𑱖", "𑱗", "𑱘", "𑱙"], "brah": ["𑁦", "𑁧", "𑁨", "𑁩", "𑁪", "𑁫", "𑁬", "𑁭", "𑁮", "𑁯"], "cakm": ["𑄶", "𑄷", "𑄸", "𑄹", "𑄺", "𑄻", "𑄼", "𑄽", "𑄾", "𑄿"], "cham": ["꩐", "꩑", "꩒", "꩓", "꩔", "꩕", "꩖", "꩗", "꩘", "꩙"], "deva": ["०", "१", "२", "३", "४", "५", "६", "७", "८", "९"], "diak": ["𑥐", "𑥑", "𑥒", "𑥓", "𑥔", "𑥕", "𑥖", "𑥗", "𑥘", "𑥙"], "fullwide": ["０", "１", "２", "３", "４", "５", "６", "７", "８", "９"], "gong": ["𑶠", "𑶡", "𑶢", "𑶣", "𑶤", "𑶥", "𑶦", "𑶧", "𑶨", "𑶩"], "gonm": ["𑵐", "𑵑", "𑵒", "𑵓", "𑵔", "𑵕", "𑵖", "𑵗", "𑵘", "𑵙"], "gujr": ["૦", "૧", "૨", "૩", "૪", "૫", "૬", "૭", "૮", "૯"], "guru": ["੦", "੧", "੨", "੩", "੪", "੫", "੬", "੭", "੮", "੯"], "hanidec": ["〇", "一", "二", "三", "四", "五", "六", "七", "八", "九"], "hmng": ["𖭐", "𖭑", "𖭒", "𖭓", "𖭔", "𖭕", "𖭖", "𖭗", "𖭘", "𖭙"], "hmnp": ["𞅀", "𞅁", "𞅂", "𞅃", "𞅄", "𞅅", "𞅆", "𞅇", "𞅈", "𞅉"], "java": ["꧐", "꧑", "꧒", "꧓", "꧔", "꧕", "꧖", "꧗", "꧘", "꧙"], "kali": ["꤀", "꤁", "꤂", "꤃", "꤄", "꤅", "꤆", "꤇", "꤈", "꤉"], "khmr": ["០", "១", "២", "៣", "៤", "៥", "៦", "៧", "៨", "៩"], "knda": ["೦", "೧", "೨", "೩", "೪", "೫", "೬", "೭", "೮", "೯"], "lana": ["᪀", "᪁", "᪂", "᪃", "᪄", "᪅", "᪆", "᪇", "᪈", "᪉"], "lanatham": ["᪐", "᪑", "᪒", "᪓", "᪔", "᪕", "᪖", "᪗", "᪘", "᪙"], "laoo": ["໐", "໑", "໒", "໓", "໔", "໕", "໖", "໗", "໘", "໙"], "lepc": ["᪐", "᪑", "᪒", "᪓", "᪔", "᪕", "᪖", "᪗", "᪘", "᪙"], "limb": ["᥆", "᥇", "᥈", "᥉", "᥊", "᥋", "᥌", "᥍", "᥎", "᥏"], "mathbold": ["𝟎", "𝟏", "𝟐", "𝟑", "𝟒", "𝟓", "𝟔", "𝟕", "𝟖", "𝟗"], "mathdbl": ["𝟘", "𝟙", "𝟚", "𝟛", "𝟜", "𝟝", "𝟞", "𝟟", "𝟠", "𝟡"], "mathmono": ["𝟶", "𝟷", "𝟸", "𝟹", "𝟺", "𝟻", "𝟼", "𝟽", "𝟾", "𝟿"], "mathsanb": ["𝟬", "𝟭", "𝟮", "𝟯", "𝟰", "𝟱", "𝟲", "𝟳", "𝟴", "𝟵"], "mathsans": ["𝟢", "𝟣", "𝟤", "𝟥", "𝟦", "𝟧", "𝟨", "𝟩", "𝟪", "𝟫"], "mlym": ["൦", "൧", "൨", "൩", "൪", "൫", "൬", "൭", "൮", "൯"], "modi": ["𑙐", "𑙑", "𑙒", "𑙓", "𑙔", "𑙕", "𑙖", "𑙗", "𑙘", "𑙙"], "mong": ["᠐", "᠑", "᠒", "᠓", "᠔", "᠕", "᠖", "᠗", "᠘", "᠙"], "mroo": ["𖩠", "𖩡", "𖩢", "𖩣", "𖩤", "𖩥", "𖩦", "𖩧", "𖩨", "𖩩"], "mtei": ["꯰", "꯱", "꯲", "꯳", "꯴", "꯵", "꯶", "꯷", "꯸", "꯹"], "mymr": ["၀", "၁", "၂", "၃", "၄", "၅", "၆", "၇", "၈", "၉"], "mymrshan": ["႐", "႑", "႒", "႓", "႔", "႕", "႖", "႗", "႘", "႙"], "mymrtlng": ["꧰", "꧱", "꧲", "꧳", "꧴", "꧵", "꧶", "꧷", "꧸", "꧹"], "newa": ["𑑐", "𑑑", "𑑒", "𑑓", "𑑔", "𑑕", "𑑖", "𑑗", "𑑘", "𑑙"], "nkoo": ["߀", "߁", "߂", "߃", "߄", "߅", "߆", "߇", "߈", "߉"], "olck": ["᱐", "᱑", "᱒", "᱓", "᱔", "᱕", "᱖", "᱗", "᱘", "᱙"], "orya": ["୦", "୧", "୨", "୩", "୪", "୫", "୬", "୭", "୮", "୯"], "osma": ["𐒠", "𐒡", "𐒢", "𐒣", "𐒤", "𐒥", "𐒦", "𐒧", "𐒨", "𐒩"], "rohg": ["𐴰", "𐴱", "𐴲", "𐴳", "𐴴", "𐴵", "𐴶", "𐴷", "𐴸", "𐴹"], "saur": ["꣐", "꣑", "꣒", "꣓", "꣔", "꣕", "꣖", "꣗", "꣘", "꣙"], "segment": ["🯰", "🯱", "🯲", "🯳", "🯴", "🯵", "🯶", "🯷", "🯸", "🯹"], "shrd": ["𑇐", "𑇑", "𑇒", "𑇓", "𑇔", "𑇕", "𑇖", "𑇗", "𑇘", "𑇙"], "sind": ["𑋰", "𑋱", "𑋲", "𑋳", "𑋴", "𑋵", "𑋶", "𑋷", "𑋸", "𑋹"], "sinh": ["෦", "෧", "෨", "෩", "෪", "෫", "෬", "෭", "෮", "෯"], "sora": ["𑃰", "𑃱", "𑃲", "𑃳", "𑃴", "𑃵", "𑃶", "𑃷", "𑃸", "𑃹"], "sund": ["᮰", "᮱", "᮲", "᮳", "᮴", "᮵", "᮶", "᮷", "᮸", "᮹"], "takr": ["𑛀", "𑛁", "𑛂", "𑛃", "𑛄", "𑛅", "𑛆", "𑛇", "𑛈", "𑛉"], "talu": ["᧐", "᧑", "᧒", "᧓", "᧔", "᧕", "᧖", "᧗", "᧘", "᧙"], "tamldec": ["௦", "௧", "௨", "௩", "௪", "௫", "௬", "௭", "௮", "௯"], "telu": ["౦", "౧", "౨", "౩", "౪", "౫", "౬", "౭", "౮", "౯"], "thai": ["๐", "๑", "๒", "๓", "๔", "๕", "๖", "๗", "๘", "๙"], "tibt": ["༠", "༡", "༢", "༣", "༤", "༥", "༦", "༧", "༨", "༩"], "tirh": ["𑓐", "𑓑", "𑓒", "𑓓", "𑓔", "𑓕", "𑓖", "𑓗", "𑓘", "𑓙"], "vaii": ["ᘠ", "ᘡ", "ᘢ", "ᘣ", "ᘤ", "ᘥ", "ᘦ", "ᘧ", "ᘨ", "ᘩ"], "wara": ["𑣠", "𑣡", "𑣢", "𑣣", "𑣤", "𑣥", "𑣦", "𑣧", "𑣨", "𑣩"], "wcho": ["𞋰", "𞋱", "𞋲", "𞋳", "𞋴", "𞋵", "𞋶", "𞋷", "𞋸", "𞋹"] }
;
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/regex.generated.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.S_UNICODE_REGEX = void 0;
// @generated from regex-gen.ts
var S_UNICODE_REGEX = /[\$\+<->\^`\|~\xA2-\xA6\xA8\xA9\xAC\xAE-\xB1\xB4\xB8\xD7\xF7\u02C2-\u02C5\u02D2-\u02DF\u02E5-\u02EB\u02ED\u02EF-\u02FF\u0375\u0384\u0385\u03F6\u0482\u058D-\u058F\u0606-\u0608\u060B\u060E\u060F\u06DE\u06E9\u06FD\u06FE\u07F6\u07FE\u07FF\u09F2\u09F3\u09FA\u09FB\u0AF1\u0B70\u0BF3-\u0BFA\u0C7F\u0D4F\u0D79\u0E3F\u0F01-\u0F03\u0F13\u0F15-\u0F17\u0F1A-\u0F1F\u0F34\u0F36\u0F38\u0FBE-\u0FC5\u0FC7-\u0FCC\u0FCE\u0FCF\u0FD5-\u0FD8\u109E\u109F\u1390-\u1399\u166D\u17DB\u1940\u19DE-\u19FF\u1B61-\u1B6A\u1B74-\u1B7C\u1FBD\u1FBF-\u1FC1\u1FCD-\u1FCF\u1FDD-\u1FDF\u1FED-\u1FEF\u1FFD\u1FFE\u2044\u2052\u207A-\u207C\u208A-\u208C\u20A0-\u20BF\u2100\u2101\u2103-\u2106\u2108\u2109\u2114\u2116-\u2118\u211E-\u2123\u2125\u2127\u2129\u212E\u213A\u213B\u2140-\u2144\u214A-\u214D\u214F\u218A\u218B\u2190-\u2307\u230C-\u2328\u232B-\u2426\u2440-\u244A\u249C-\u24E9\u2500-\u2767\u2794-\u27C4\u27C7-\u27E5\u27F0-\u2982\u2999-\u29D7\u29DC-\u29FB\u29FE-\u2B73\u2B76-\u2B95\u2B97-\u2BFF\u2CE5-\u2CEA\u2E50\u2E51\u2E80-\u2E99\u2E9B-\u2EF3\u2F00-\u2FD5\u2FF0-\u2FFB\u3004\u3012\u3013\u3020\u3036\u3037\u303E\u303F\u309B\u309C\u3190\u3191\u3196-\u319F\u31C0-\u31E3\u3200-\u321E\u322A-\u3247\u3250\u3260-\u327F\u328A-\u32B0\u32C0-\u33FF\u4DC0-\u4DFF\uA490-\uA4C6\uA700-\uA716\uA720\uA721\uA789\uA78A\uA828-\uA82B\uA836-\uA839\uAA77-\uAA79\uAB5B\uAB6A\uAB6B\uFB29\uFBB2-\uFBC1\uFDFC\uFDFD\uFE62\uFE64-\uFE66\uFE69\uFF04\uFF0B\uFF1C-\uFF1E\uFF3E\uFF40\uFF5C\uFF5E\uFFE0-\uFFE6\uFFE8-\uFFEE\uFFFC\uFFFD]|\uD800[\uDD37-\uDD3F\uDD79-\uDD89\uDD8C-\uDD8E\uDD90-\uDD9C\uDDA0\uDDD0-\uDDFC]|\uD802[\uDC77\uDC78\uDEC8]|\uD805\uDF3F|\uD807[\uDFD5-\uDFF1]|\uD81A[\uDF3C-\uDF3F\uDF45]|\uD82F\uDC9C|\uD834[\uDC00-\uDCF5\uDD00-\uDD26\uDD29-\uDD64\uDD6A-\uDD6C\uDD83\uDD84\uDD8C-\uDDA9\uDDAE-\uDDE8\uDE00-\uDE41\uDE45\uDF00-\uDF56]|\uD835[\uDEC1\uDEDB\uDEFB\uDF15\uDF35\uDF4F\uDF6F\uDF89\uDFA9\uDFC3]|\uD836[\uDC00-\uDDFF\uDE37-\uDE3A\uDE6D-\uDE74\uDE76-\uDE83\uDE85\uDE86]|\uD838[\uDD4F\uDEFF]|\uD83B[\uDCAC\uDCB0\uDD2E\uDEF0\uDEF1]|\uD83C[\uDC00-\uDC2B\uDC30-\uDC93\uDCA0-\uDCAE\uDCB1-\uDCBF\uDCC1-\uDCCF\uDCD1-\uDCF5\uDD0D-\uDDAD\uDDE6-\uDE02\uDE10-\uDE3B\uDE40-\uDE48\uDE50\uDE51\uDE60-\uDE65\uDF00-\uDFFF]|\uD83D[\uDC00-\uDED7\uDEE0-\uDEEC\uDEF0-\uDEFC\uDF00-\uDF73\uDF80-\uDFD8\uDFE0-\uDFEB]|\uD83E[\uDC00-\uDC0B\uDC10-\uDC47\uDC50-\uDC59\uDC60-\uDC87\uDC90-\uDCAD\uDCB0\uDCB1\uDD00-\uDD78\uDD7A-\uDDCB\uDDCD-\uDE53\uDE60-\uDE6D\uDE70-\uDE74\uDE78-\uDE7A\uDE80-\uDE86\uDE90-\uDEA8\uDEB0-\uDEB6\uDEC0-\uDEC2\uDED0-\uDED6\uDF00-\uDF92\uDF94-\uDFCA]/;
exports.S_UNICODE_REGEX = S_UNICODE_REGEX;
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/format_to_parts.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = formatToParts;

var _ToRawFixed = require("./ToRawFixed");

var digitMapping = _interopRequireWildcard(require("./digit-mapping.json"));

var _regex = require("../regex.generated");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

// This is from: unicode-12.1.0/General_Category/Symbol/regex.js
// IE11 does not support unicode flag, otherwise this is just /\p{S}/u.
// /^\p{S}/u
var CARET_S_UNICODE_REGEX = new RegExp("^" + _regex.S_UNICODE_REGEX.source); // /\p{S}$/u

var S_DOLLAR_UNICODE_REGEX = new RegExp(_regex.S_UNICODE_REGEX.source + "$");
var CLDR_NUMBER_PATTERN = /[#0](?:[\.,][#0]+)*/g;

function formatToParts(numberResult, data, pl, options) {
  var sign = numberResult.sign,
      exponent = numberResult.exponent,
      magnitude = numberResult.magnitude;
  var notation = options.notation,
      style = options.style,
      numberingSystem = options.numberingSystem;
  var defaultNumberingSystem = data.numbers.nu[0]; // #region Part 1: partition and interpolate the CLDR number pattern.
  // ----------------------------------------------------------

  var compactNumberPattern = null;

  if (notation === 'compact' && magnitude) {
    compactNumberPattern = getCompactDisplayPattern(numberResult, pl, data, style, options.compactDisplay, options.currencyDisplay, numberingSystem);
  } // This is used multiple times


  var nonNameCurrencyPart;

  if (style === 'currency' && options.currencyDisplay !== 'name') {
    var byCurrencyDisplay = data.currencies[options.currency];

    if (byCurrencyDisplay) {
      switch (options.currencyDisplay) {
        case 'code':
          nonNameCurrencyPart = options.currency;
          break;

        case 'symbol':
          nonNameCurrencyPart = byCurrencyDisplay.symbol;
          break;

        default:
          nonNameCurrencyPart = byCurrencyDisplay.narrow;
          break;
      }
    } else {
      // Fallback for unknown currency
      nonNameCurrencyPart = options.currency;
    }
  }

  var numberPattern;

  if (!compactNumberPattern) {
    // Note: if the style is unit, or is currency and the currency display is name,
    // its unit parts will be interpolated in part 2. So here we can fallback to decimal.
    if (style === 'decimal' || style === 'unit' || style === 'currency' && options.currencyDisplay === 'name') {
      // Shortcut for decimal
      var decimalData = data.numbers.decimal[numberingSystem] || data.numbers.decimal[defaultNumberingSystem];
      numberPattern = getPatternForSign(decimalData.standard, sign);
    } else if (style === 'currency') {
      var currencyData = data.numbers.currency[numberingSystem] || data.numbers.currency[defaultNumberingSystem]; // We replace number pattern part with `0` for easier postprocessing.

      numberPattern = getPatternForSign(currencyData[options.currencySign], sign);
    } else {
      // percent
      var percentPattern = data.numbers.percent[numberingSystem] || data.numbers.percent[defaultNumberingSystem];
      numberPattern = getPatternForSign(percentPattern, sign);
    }
  } else {
    numberPattern = compactNumberPattern;
  } // Extract the decimal number pattern string. It looks like "#,##0,00", which will later be
  // used to infer decimal group sizes.


  var decimalNumberPattern = CLDR_NUMBER_PATTERN.exec(numberPattern)[0]; // Now we start to substitute patterns
  // 1. replace strings like `0` and `#,##0.00` with `{0}`
  // 2. unquote characters (invariant: the quoted characters does not contain the special tokens)

  numberPattern = numberPattern.replace(CLDR_NUMBER_PATTERN, '{0}').replace(/'(.)'/g, '$1'); // Handle currency spacing (both compact and non-compact).

  if (style === 'currency' && options.currencyDisplay !== 'name') {
    var currencyData = data.numbers.currency[numberingSystem] || data.numbers.currency[defaultNumberingSystem]; // See `currencySpacing` substitution rule in TR-35.
    // Here we always assume the currencyMatch is "[:^S:]" and surroundingMatch is "[:digit:]".
    //
    // Example 1: for pattern "#,##0.00¤" with symbol "US$", we replace "¤" with the symbol,
    // but insert an extra non-break space before the symbol, because "[:^S:]" matches "U" in
    // "US$" and "[:digit:]" matches the latn numbering system digits.
    //
    // Example 2: for pattern "¤#,##0.00" with symbol "US$", there is no spacing between symbol
    // and number, because `$` does not match "[:^S:]".
    //
    // Implementation note: here we do the best effort to infer the insertion.
    // We also assume that `beforeInsertBetween` and `afterInsertBetween` will never be `;`.

    var afterCurrency = currencyData.currencySpacing.afterInsertBetween;

    if (afterCurrency && !S_DOLLAR_UNICODE_REGEX.test(nonNameCurrencyPart)) {
      numberPattern = numberPattern.replace('¤{0}', "\u00A4" + afterCurrency + "{0}");
    }

    var beforeCurrency = currencyData.currencySpacing.beforeInsertBetween;

    if (beforeCurrency && !CARET_S_UNICODE_REGEX.test(nonNameCurrencyPart)) {
      numberPattern = numberPattern.replace('{0}¤', "{0}" + beforeCurrency + "\u00A4");
    }
  } // The following tokens are special: `{0}`, `¤`, `%`, `-`, `+`, `{c:...}.


  var numberPatternParts = numberPattern.split(/({c:[^}]+}|\{0\}|[¤%\-\+])/g);
  var numberParts = [];
  var symbols = data.numbers.symbols[numberingSystem] || data.numbers.symbols[defaultNumberingSystem];

  for (var _i = 0, numberPatternParts_1 = numberPatternParts; _i < numberPatternParts_1.length; _i++) {
    var part = numberPatternParts_1[_i];

    if (!part) {
      continue;
    }

    switch (part) {
      case '{0}':
        {
          // We only need to handle scientific and engineering notation here.
          numberParts.push.apply(numberParts, paritionNumberIntoParts(symbols, numberResult, notation, exponent, numberingSystem, // If compact number pattern exists, do not insert group separators.
          !compactNumberPattern && options.useGrouping, decimalNumberPattern));
          break;
        }

      case '-':
        numberParts.push({
          type: 'minusSign',
          value: symbols.minusSign
        });
        break;

      case '+':
        numberParts.push({
          type: 'plusSign',
          value: symbols.plusSign
        });
        break;

      case '%':
        numberParts.push({
          type: 'percentSign',
          value: symbols.percentSign
        });
        break;

      case '¤':
        // Computed above when handling currency spacing.
        numberParts.push({
          type: 'currency',
          value: nonNameCurrencyPart
        });
        break;

      default:
        if (/^\{c:/.test(part)) {
          numberParts.push({
            type: 'compact',
            value: part.substring(3, part.length - 1)
          });
        } else {
          // literal
          numberParts.push({
            type: 'literal',
            value: part
          });
        }

        break;
    }
  } // #endregion
  // #region Part 2: interpolate unit pattern if necessary.
  // ----------------------------------------------


  switch (style) {
    case 'currency':
      {
        // `currencyDisplay: 'name'` has similar pattern handling as units.
        if (options.currencyDisplay === 'name') {
          var unitPattern = (data.numbers.currency[numberingSystem] || data.numbers.currency[defaultNumberingSystem]).unitPattern; // Select plural

          var unitName = void 0;
          var currencyNameData = data.currencies[options.currency];

          if (currencyNameData) {
            unitName = selectPlural(pl, numberResult.roundedNumber * Math.pow(10, exponent), currencyNameData.displayName);
          } else {
            // Fallback for unknown currency
            unitName = options.currency;
          } // Do {0} and {1} substitution


          var unitPatternParts = unitPattern.split(/(\{[01]\})/g);
          var result = [];

          for (var _a = 0, unitPatternParts_1 = unitPatternParts; _a < unitPatternParts_1.length; _a++) {
            var part = unitPatternParts_1[_a];

            switch (part) {
              case '{0}':
                result.push.apply(result, numberParts);
                break;

              case '{1}':
                result.push({
                  type: 'currency',
                  value: unitName
                });
                break;

              default:
                if (part) {
                  result.push({
                    type: 'literal',
                    value: part
                  });
                }

                break;
            }
          }

          return result;
        } else {
          return numberParts;
        }
      }

    case 'unit':
      {
        var unit = options.unit,
            unitDisplay = options.unitDisplay;
        var unitData = data.units.simple[unit];
        var unitPattern = void 0;

        if (unitData) {
          // Simple unit pattern
          unitPattern = selectPlural(pl, numberResult.roundedNumber * Math.pow(10, exponent), data.units.simple[unit][unitDisplay]);
        } else {
          // See: http://unicode.org/reports/tr35/tr35-general.html#perUnitPatterns
          // If cannot find unit in the simple pattern, it must be "per" compound pattern.
          // Implementation note: we are not following TR-35 here because we need to format to parts!
          var _b = unit.split('-per-'),
              numeratorUnit = _b[0],
              denominatorUnit = _b[1];

          unitData = data.units.simple[numeratorUnit];
          var numeratorUnitPattern = selectPlural(pl, numberResult.roundedNumber * Math.pow(10, exponent), data.units.simple[numeratorUnit][unitDisplay]);
          var perUnitPattern = data.units.simple[denominatorUnit].perUnit[unitDisplay];

          if (perUnitPattern) {
            // perUnitPattern exists, combine it with numeratorUnitPattern
            unitPattern = perUnitPattern.replace('{0}', numeratorUnitPattern);
          } else {
            // get compoundUnit pattern (e.g. "{0} per {1}"), repalce {0} with numerator pattern and {1} with
            // the denominator pattern in singular form.
            var perPattern = data.units.compound.per[unitDisplay];
            var denominatorPattern = selectPlural(pl, 1, data.units.simple[denominatorUnit][unitDisplay]);
            unitPattern = unitPattern = perPattern.replace('{0}', numeratorUnitPattern).replace('{1}', denominatorPattern.replace('{0}', ''));
          }
        }

        var result = []; // We need spacing around "{0}" because they are not treated as "unit" parts, but "literal".

        for (var _c = 0, _d = unitPattern.split(/(\s*\{0\}\s*)/); _c < _d.length; _c++) {
          var part = _d[_c];
          var interpolateMatch = /^(\s*)\{0\}(\s*)$/.exec(part);

          if (interpolateMatch) {
            // Space before "{0}"
            if (interpolateMatch[1]) {
              result.push({
                type: 'literal',
                value: interpolateMatch[1]
              });
            } // "{0}" itself


            result.push.apply(result, numberParts); // Space after "{0}"

            if (interpolateMatch[2]) {
              result.push({
                type: 'literal',
                value: interpolateMatch[2]
              });
            }
          } else if (part) {
            result.push({
              type: 'unit',
              value: part
            });
          }
        }

        return result;
      }

    default:
      return numberParts;
  } // #endregion

} // A subset of https://tc39.es/ecma402/#sec-partitionnotationsubpattern
// Plus the exponent parts handling.


function paritionNumberIntoParts(symbols, numberResult, notation, exponent, numberingSystem, useGrouping,
/**
 * This is the decimal number pattern without signs or symbols.
 * It is used to infer the group size when `useGrouping` is true.
 *
 * A typical value looks like "#,##0.00" (primary group size is 3).
 * Some locales like Hindi has secondary group size of 2 (e.g. "#,##,##0.00").
 */
decimalNumberPattern) {
  var result = []; // eslint-disable-next-line prefer-const

  var n = numberResult.formattedString,
      x = numberResult.roundedNumber;

  if (isNaN(x)) {
    return [{
      type: 'nan',
      value: n
    }];
  } else if (!isFinite(x)) {
    return [{
      type: 'infinity',
      value: n
    }];
  }

  var digitReplacementTable = digitMapping[numberingSystem];

  if (digitReplacementTable) {
    n = n.replace(/\d/g, function (digit) {
      return digitReplacementTable[+digit] || digit;
    });
  } // TODO: Else use an implementation dependent algorithm to map n to the appropriate
  // representation of n in the given numbering system.


  var decimalSepIndex = n.indexOf('.');
  var integer;
  var fraction;

  if (decimalSepIndex > 0) {
    integer = n.slice(0, decimalSepIndex);
    fraction = n.slice(decimalSepIndex + 1);
  } else {
    integer = n;
  } // #region Grouping integer digits
  // The weird compact and x >= 10000 check is to ensure consistency with Node.js and Chrome.
  // Note that `de` does not have compact form for thousands, but Node.js does not insert grouping separator
  // unless the rounded number is greater than 10000:
  //   NumberFormat('de', {notation: 'compact', compactDisplay: 'short'}).format(1234) //=> "1234"
  //   NumberFormat('de').format(1234) //=> "1.234"


  if (useGrouping && (notation !== 'compact' || x >= 10000)) {
    var groupSepSymbol = symbols.group;
    var groups = []; // > There may be two different grouping sizes: The primary grouping size used for the least
    // > significant integer group, and the secondary grouping size used for more significant groups.
    // > If a pattern contains multiple grouping separators, the interval between the last one and the
    // > end of the integer defines the primary grouping size, and the interval between the last two
    // > defines the secondary grouping size. All others are ignored.

    var integerNumberPattern = decimalNumberPattern.split('.')[0];
    var patternGroups = integerNumberPattern.split(',');
    var primaryGroupingSize = 3;
    var secondaryGroupingSize = 3;

    if (patternGroups.length > 1) {
      primaryGroupingSize = patternGroups[patternGroups.length - 1].length;
    }

    if (patternGroups.length > 2) {
      secondaryGroupingSize = patternGroups[patternGroups.length - 2].length;
    }

    var i = integer.length - primaryGroupingSize;

    if (i > 0) {
      // Slice the least significant integer group
      groups.push(integer.slice(i, i + primaryGroupingSize)); // Then iteratively push the more signicant groups
      // TODO: handle surrogate pairs in some numbering system digits

      for (i -= secondaryGroupingSize; i > 0; i -= secondaryGroupingSize) {
        groups.push(integer.slice(i, i + secondaryGroupingSize));
      }

      groups.push(integer.slice(0, i + secondaryGroupingSize));
    } else {
      groups.push(integer);
    }

    while (groups.length > 0) {
      var integerGroup = groups.pop();
      result.push({
        type: 'integer',
        value: integerGroup
      });

      if (groups.length > 0) {
        result.push({
          type: 'group',
          value: groupSepSymbol
        });
      }
    }
  } else {
    result.push({
      type: 'integer',
      value: integer
    });
  } // #endregion


  if (fraction !== undefined) {
    result.push({
      type: 'decimal',
      value: symbols.decimal
    }, {
      type: 'fraction',
      value: fraction
    });
  }

  if ((notation === 'scientific' || notation === 'engineering') && isFinite(x)) {
    result.push({
      type: 'exponentSeparator',
      value: symbols.exponential
    });

    if (exponent < 0) {
      result.push({
        type: 'exponentMinusSign',
        value: symbols.minusSign
      });
      exponent = -exponent;
    }

    var exponentResult = (0, _ToRawFixed.ToRawFixed)(exponent, 0, 0);
    result.push({
      type: 'exponentInteger',
      value: exponentResult.formattedString
    });
  }

  return result;
}

function getPatternForSign(pattern, sign) {
  if (pattern.indexOf(';') < 0) {
    pattern = pattern + ";-" + pattern;
  }

  var _a = pattern.split(';'),
      zeroPattern = _a[0],
      negativePattern = _a[1];

  switch (sign) {
    case 0:
      return zeroPattern;

    case -1:
      return negativePattern;

    default:
      return negativePattern.indexOf('-') >= 0 ? negativePattern.replace(/-/g, '+') : "+" + zeroPattern;
  }
} // Find the CLDR pattern for compact notation based on the magnitude of data and style.
//
// Example return value: "¤ {c:laki}000;¤{c:laki} -0" (`sw` locale):
// - Notice the `{c:...}` token that wraps the compact literal.
// - The consecutive zeros are normalized to single zero to match CLDR_NUMBER_PATTERN.
//
// Returning null means the compact display pattern cannot be found.


function getCompactDisplayPattern(numberResult, pl, data, style, compactDisplay, currencyDisplay, numberingSystem) {
  var _a;

  var roundedNumber = numberResult.roundedNumber,
      sign = numberResult.sign,
      magnitude = numberResult.magnitude;
  var magnitudeKey = String(Math.pow(10, magnitude));
  var defaultNumberingSystem = data.numbers.nu[0];
  var pattern;

  if (style === 'currency' && currencyDisplay !== 'name') {
    var byNumberingSystem = data.numbers.currency;
    var currencyData = byNumberingSystem[numberingSystem] || byNumberingSystem[defaultNumberingSystem]; // NOTE: compact notation ignores currencySign!

    var compactPluralRules = (_a = currencyData.short) === null || _a === void 0 ? void 0 : _a[magnitudeKey];

    if (!compactPluralRules) {
      return null;
    }

    pattern = selectPlural(pl, roundedNumber, compactPluralRules);
  } else {
    var byNumberingSystem = data.numbers.decimal;
    var byCompactDisplay = byNumberingSystem[numberingSystem] || byNumberingSystem[defaultNumberingSystem];
    var compactPlaralRule = byCompactDisplay[compactDisplay][magnitudeKey];

    if (!compactPlaralRule) {
      return null;
    }

    pattern = selectPlural(pl, roundedNumber, compactPlaralRule);
  } // See https://unicode.org/reports/tr35/tr35-numbers.html#Compact_Number_Formats
  // > If the value is precisely “0”, either explicit or defaulted, then the normal number format
  // > pattern for that sort of object is supplied.


  if (pattern === '0') {
    return null;
  }

  pattern = getPatternForSign(pattern, sign) // Extract compact literal from the pattern
  .replace(/([^\s;\-\+\d¤]+)/g, '{c:$1}') // We replace one or more zeros with a single zero so it matches `CLDR_NUMBER_PATTERN`.
  .replace(/0+/, '0');
  return pattern;
}

function selectPlural(pl, x, rules) {
  return rules[pl.select(x)] || rules.other;
}
},{"./ToRawFixed":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ToRawFixed.js","./digit-mapping.json":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/digit-mapping.json","../regex.generated":"../node_modules/@formatjs/ecma402-abstract/lib/regex.generated.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/PartitionNumberPattern.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PartitionNumberPattern = PartitionNumberPattern;

var _FormatNumericToString = require("./FormatNumericToString");

var _ = require("../262");

var _ComputeExponent = require("./ComputeExponent");

var _format_to_parts = _interopRequireDefault(require("./format_to_parts"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * https://tc39.es/ecma402/#sec-formatnumberstring
 */
function PartitionNumberPattern(numberFormat, x, _a) {
  var _b;

  var getInternalSlots = _a.getInternalSlots;
  var internalSlots = getInternalSlots(numberFormat);
  var pl = internalSlots.pl,
      dataLocaleData = internalSlots.dataLocaleData,
      numberingSystem = internalSlots.numberingSystem;
  var symbols = dataLocaleData.numbers.symbols[numberingSystem] || dataLocaleData.numbers.symbols[dataLocaleData.numbers.nu[0]];
  var magnitude = 0;
  var exponent = 0;
  var n;

  if (isNaN(x)) {
    n = symbols.nan;
  } else if (!isFinite(x)) {
    n = symbols.infinity;
  } else {
    if (internalSlots.style === 'percent') {
      x *= 100;
    }

    ;
    _b = (0, _ComputeExponent.ComputeExponent)(numberFormat, x, {
      getInternalSlots: getInternalSlots
    }), exponent = _b[0], magnitude = _b[1]; // Preserve more precision by doing multiplication when exponent is negative.

    x = exponent < 0 ? x * Math.pow(10, -exponent) : x / Math.pow(10, exponent);
    var formatNumberResult = (0, _FormatNumericToString.FormatNumericToString)(internalSlots, x);
    n = formatNumberResult.formattedString;
    x = formatNumberResult.roundedNumber;
  } // Based on https://tc39.es/ecma402/#sec-getnumberformatpattern
  // We need to do this before `x` is rounded.


  var sign;
  var signDisplay = internalSlots.signDisplay;

  switch (signDisplay) {
    case 'never':
      sign = 0;
      break;

    case 'auto':
      if ((0, _.SameValue)(x, 0) || x > 0 || isNaN(x)) {
        sign = 0;
      } else {
        sign = -1;
      }

      break;

    case 'always':
      if ((0, _.SameValue)(x, 0) || x > 0 || isNaN(x)) {
        sign = 1;
      } else {
        sign = -1;
      }

      break;

    default:
      // x === 0 -> x is 0 or x is -0
      if (x === 0 || isNaN(x)) {
        sign = 0;
      } else if (x > 0) {
        sign = 1;
      } else {
        sign = -1;
      }

  }

  return (0, _format_to_parts.default)({
    roundedNumber: x,
    formattedString: n,
    exponent: exponent,
    magnitude: magnitude,
    sign: sign
  }, internalSlots.dataLocaleData, pl, internalSlots);
}
},{"./FormatNumericToString":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/FormatNumericToString.js","../262":"../node_modules/@formatjs/ecma402-abstract/lib/262.js","./ComputeExponent":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ComputeExponent.js","./format_to_parts":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/format_to_parts.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/FormatNumericToParts.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormatNumericToParts = FormatNumericToParts;

var _PartitionNumberPattern = require("./PartitionNumberPattern");

var _ = require("../262");

function FormatNumericToParts(nf, x, implDetails) {
  var parts = (0, _PartitionNumberPattern.PartitionNumberPattern)(nf, x, implDetails);
  var result = (0, _.ArrayCreate)(0);

  for (var _i = 0, parts_1 = parts; _i < parts_1.length; _i++) {
    var part = parts_1[_i];
    result.push({
      type: part.type,
      value: part.value
    });
  }

  return result;
}
},{"./PartitionNumberPattern":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/PartitionNumberPattern.js","../262":"../node_modules/@formatjs/ecma402-abstract/lib/262.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/BestAvailableLocale.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BestAvailableLocale = BestAvailableLocale;

/**
 * https://tc39.es/ecma402/#sec-bestavailablelocale
 * @param availableLocales
 * @param locale
 */
function BestAvailableLocale(availableLocales, locale) {
  var candidate = locale;

  while (true) {
    if (availableLocales.has(candidate)) {
      return candidate;
    }

    var pos = candidate.lastIndexOf('-');

    if (!~pos) {
      return undefined;
    }

    if (pos >= 2 && candidate[pos - 2] === '-') {
      pos -= 2;
    }

    candidate = candidate.slice(0, pos);
  }
}
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/LookupMatcher.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LookupMatcher = LookupMatcher;

var _utils = require("./utils");

var _BestAvailableLocale = require("./BestAvailableLocale");

/**
 * https://tc39.es/ecma402/#sec-lookupmatcher
 * @param availableLocales
 * @param requestedLocales
 * @param getDefaultLocale
 */
function LookupMatcher(availableLocales, requestedLocales, getDefaultLocale) {
  var result = {
    locale: ''
  };

  for (var _i = 0, requestedLocales_1 = requestedLocales; _i < requestedLocales_1.length; _i++) {
    var locale = requestedLocales_1[_i];
    var noExtensionLocale = locale.replace(_utils.UNICODE_EXTENSION_SEQUENCE_REGEX, '');
    var availableLocale = (0, _BestAvailableLocale.BestAvailableLocale)(availableLocales, noExtensionLocale);

    if (availableLocale) {
      result.locale = availableLocale;

      if (locale !== noExtensionLocale) {
        result.extension = locale.slice(noExtensionLocale.length + 1, locale.length);
      }

      return result;
    }
  }

  result.locale = getDefaultLocale();
  return result;
}
},{"./utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js","./BestAvailableLocale":"../node_modules/@formatjs/ecma402-abstract/lib/BestAvailableLocale.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/BestFitMatcher.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BestFitMatcher = BestFitMatcher;

var _BestAvailableLocale = require("./BestAvailableLocale");

var _utils = require("./utils");

/**
 * https://tc39.es/ecma402/#sec-bestfitmatcher
 * @param availableLocales
 * @param requestedLocales
 * @param getDefaultLocale
 */
function BestFitMatcher(availableLocales, requestedLocales, getDefaultLocale) {
  var minimizedAvailableLocaleMap = {};
  var minimizedAvailableLocales = new Set();
  availableLocales.forEach(function (locale) {
    var minimizedLocale = new Intl.Locale(locale).minimize().toString();
    minimizedAvailableLocaleMap[minimizedLocale] = locale;
    minimizedAvailableLocales.add(minimizedLocale);
  });
  var foundLocale;

  for (var _i = 0, requestedLocales_1 = requestedLocales; _i < requestedLocales_1.length; _i++) {
    var l = requestedLocales_1[_i];

    if (foundLocale) {
      break;
    }

    var noExtensionLocale = l.replace(_utils.UNICODE_EXTENSION_SEQUENCE_REGEX, '');

    if (availableLocales.has(noExtensionLocale)) {
      foundLocale = noExtensionLocale;
      break;
    }

    if (minimizedAvailableLocales.has(noExtensionLocale)) {
      foundLocale = minimizedAvailableLocaleMap[noExtensionLocale];
      break;
    }

    var locale = new Intl.Locale(noExtensionLocale);
    var maximizedRequestedLocale = locale.maximize().toString();
    var minimizedRequestedLocale = locale.minimize().toString(); // Check minimized locale

    if (minimizedAvailableLocales.has(minimizedRequestedLocale)) {
      foundLocale = minimizedAvailableLocaleMap[minimizedRequestedLocale];
      break;
    } // Lookup algo on maximized locale


    foundLocale = (0, _BestAvailableLocale.BestAvailableLocale)(minimizedAvailableLocales, maximizedRequestedLocale);
  }

  return {
    locale: foundLocale || getDefaultLocale()
  };
}
},{"./BestAvailableLocale":"../node_modules/@formatjs/ecma402-abstract/lib/BestAvailableLocale.js","./utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/UnicodeExtensionValue.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UnicodeExtensionValue = UnicodeExtensionValue;

var _utils = require("./utils");

/**
 * https://tc39.es/ecma402/#sec-unicodeextensionvalue
 * @param extension
 * @param key
 */
function UnicodeExtensionValue(extension, key) {
  (0, _utils.invariant)(key.length === 2, 'key must have 2 elements');
  var size = extension.length;
  var searchValue = "-" + key + "-";
  var pos = extension.indexOf(searchValue);

  if (pos !== -1) {
    var start = pos + 4;
    var end = start;
    var k = start;
    var done = false;

    while (!done) {
      var e = extension.indexOf('-', k);
      var len = void 0;

      if (e === -1) {
        len = size - k;
      } else {
        len = e - k;
      }

      if (len === 2) {
        done = true;
      } else if (e === -1) {
        end = size;
        done = true;
      } else {
        end = e;
        k = e + 1;
      }
    }

    return extension.slice(start, end);
  }

  searchValue = "-" + key;
  pos = extension.indexOf(searchValue);

  if (pos !== -1 && pos + 3 === size) {
    return '';
  }

  return undefined;
}
},{"./utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/ResolveLocale.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ResolveLocale = ResolveLocale;

var _LookupMatcher = require("./LookupMatcher");

var _BestFitMatcher = require("./BestFitMatcher");

var _utils = require("./utils");

var _UnicodeExtensionValue = require("./UnicodeExtensionValue");

/**
 * https://tc39.es/ecma402/#sec-resolvelocale
 */
function ResolveLocale(availableLocales, requestedLocales, options, relevantExtensionKeys, localeData, getDefaultLocale) {
  var matcher = options.localeMatcher;
  var r;

  if (matcher === 'lookup') {
    r = (0, _LookupMatcher.LookupMatcher)(availableLocales, requestedLocales, getDefaultLocale);
  } else {
    r = (0, _BestFitMatcher.BestFitMatcher)(availableLocales, requestedLocales, getDefaultLocale);
  }

  var foundLocale = r.locale;
  var result = {
    locale: '',
    dataLocale: foundLocale
  };
  var supportedExtension = '-u';

  for (var _i = 0, relevantExtensionKeys_1 = relevantExtensionKeys; _i < relevantExtensionKeys_1.length; _i++) {
    var key = relevantExtensionKeys_1[_i];
    (0, _utils.invariant)(foundLocale in localeData, "Missing locale data for " + foundLocale);
    var foundLocaleData = localeData[foundLocale];
    (0, _utils.invariant)(typeof foundLocaleData === 'object' && foundLocaleData !== null, "locale data " + key + " must be an object");
    var keyLocaleData = foundLocaleData[key];
    (0, _utils.invariant)(Array.isArray(keyLocaleData), "keyLocaleData for " + key + " must be an array");
    var value = keyLocaleData[0];
    (0, _utils.invariant)(typeof value === 'string' || value === null, "value must be string or null but got " + typeof value + " in key " + key);
    var supportedExtensionAddition = '';

    if (r.extension) {
      var requestedValue = (0, _UnicodeExtensionValue.UnicodeExtensionValue)(r.extension, key);

      if (requestedValue !== undefined) {
        if (requestedValue !== '') {
          if (~keyLocaleData.indexOf(requestedValue)) {
            value = requestedValue;
            supportedExtensionAddition = "-" + key + "-" + value;
          }
        } else if (~requestedValue.indexOf('true')) {
          value = 'true';
          supportedExtensionAddition = "-" + key;
        }
      }
    }

    if (key in options) {
      var optionsValue = options[key];
      (0, _utils.invariant)(typeof optionsValue === 'string' || typeof optionsValue === 'undefined' || optionsValue === null, 'optionsValue must be String, Undefined or Null');

      if (~keyLocaleData.indexOf(optionsValue)) {
        if (optionsValue !== value) {
          value = optionsValue;
          supportedExtensionAddition = '';
        }
      }
    }

    result[key] = value;
    supportedExtension += supportedExtensionAddition;
  }

  if (supportedExtension.length > 2) {
    var privateIndex = foundLocale.indexOf('-x-');

    if (privateIndex === -1) {
      foundLocale = foundLocale + supportedExtension;
    } else {
      var preExtension = foundLocale.slice(0, privateIndex);
      var postExtension = foundLocale.slice(privateIndex, foundLocale.length);
      foundLocale = preExtension + supportedExtension + postExtension;
    }

    foundLocale = Intl.getCanonicalLocales(foundLocale)[0];
  }

  result.locale = foundLocale;
  return result;
}
},{"./LookupMatcher":"../node_modules/@formatjs/ecma402-abstract/lib/LookupMatcher.js","./BestFitMatcher":"../node_modules/@formatjs/ecma402-abstract/lib/BestFitMatcher.js","./utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js","./UnicodeExtensionValue":"../node_modules/@formatjs/ecma402-abstract/lib/UnicodeExtensionValue.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/SetNumberFormatUnitOptions.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SetNumberFormatUnitOptions = SetNumberFormatUnitOptions;

var _GetOption = require("../GetOption");

var _IsWellFormedCurrencyCode = require("../IsWellFormedCurrencyCode");

var _IsWellFormedUnitIdentifier = require("../IsWellFormedUnitIdentifier");

/**
 * https://tc39.es/ecma402/#sec-setnumberformatunitoptions
 */
function SetNumberFormatUnitOptions(nf, options, _a) {
  if (options === void 0) {
    options = Object.create(null);
  }

  var getInternalSlots = _a.getInternalSlots;
  var internalSlots = getInternalSlots(nf);
  var style = (0, _GetOption.GetOption)(options, 'style', 'string', ['decimal', 'percent', 'currency', 'unit'], 'decimal');
  internalSlots.style = style;
  var currency = (0, _GetOption.GetOption)(options, 'currency', 'string', undefined, undefined);

  if (currency !== undefined && !(0, _IsWellFormedCurrencyCode.IsWellFormedCurrencyCode)(currency)) {
    throw RangeError('Malformed currency code');
  }

  if (style === 'currency' && currency === undefined) {
    throw TypeError('currency cannot be undefined');
  }

  var currencyDisplay = (0, _GetOption.GetOption)(options, 'currencyDisplay', 'string', ['code', 'symbol', 'narrowSymbol', 'name'], 'symbol');
  var currencySign = (0, _GetOption.GetOption)(options, 'currencySign', 'string', ['standard', 'accounting'], 'standard');
  var unit = (0, _GetOption.GetOption)(options, 'unit', 'string', undefined, undefined);

  if (unit !== undefined && !(0, _IsWellFormedUnitIdentifier.IsWellFormedUnitIdentifier)(unit)) {
    throw RangeError('Invalid unit argument for Intl.NumberFormat()');
  }

  if (style === 'unit' && unit === undefined) {
    throw TypeError('unit cannot be undefined');
  }

  var unitDisplay = (0, _GetOption.GetOption)(options, 'unitDisplay', 'string', ['short', 'narrow', 'long'], 'short');

  if (style === 'currency') {
    internalSlots.currency = currency.toUpperCase();
    internalSlots.currencyDisplay = currencyDisplay;
    internalSlots.currencySign = currencySign;
  }

  if (style === 'unit') {
    internalSlots.unit = unit;
    internalSlots.unitDisplay = unitDisplay;
  }
}
},{"../GetOption":"../node_modules/@formatjs/ecma402-abstract/lib/GetOption.js","../IsWellFormedCurrencyCode":"../node_modules/@formatjs/ecma402-abstract/lib/IsWellFormedCurrencyCode.js","../IsWellFormedUnitIdentifier":"../node_modules/@formatjs/ecma402-abstract/lib/IsWellFormedUnitIdentifier.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/SetNumberFormatDigitOptions.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SetNumberFormatDigitOptions = SetNumberFormatDigitOptions;

var _GetNumberOption = require("../GetNumberOption");

var _DefaultNumberOption = require("../DefaultNumberOption");

/**
 * https://tc39.es/ecma402/#sec-setnfdigitoptions
 */
function SetNumberFormatDigitOptions(internalSlots, opts, mnfdDefault, mxfdDefault, notation) {
  var mnid = (0, _GetNumberOption.GetNumberOption)(opts, 'minimumIntegerDigits', 1, 21, 1);
  var mnfd = opts.minimumFractionDigits;
  var mxfd = opts.maximumFractionDigits;
  var mnsd = opts.minimumSignificantDigits;
  var mxsd = opts.maximumSignificantDigits;
  internalSlots.minimumIntegerDigits = mnid;

  if (mnsd !== undefined || mxsd !== undefined) {
    internalSlots.roundingType = 'significantDigits';
    mnsd = (0, _DefaultNumberOption.DefaultNumberOption)(mnsd, 1, 21, 1);
    mxsd = (0, _DefaultNumberOption.DefaultNumberOption)(mxsd, mnsd, 21, 21);
    internalSlots.minimumSignificantDigits = mnsd;
    internalSlots.maximumSignificantDigits = mxsd;
  } else if (mnfd !== undefined || mxfd !== undefined) {
    internalSlots.roundingType = 'fractionDigits';
    mnfd = (0, _DefaultNumberOption.DefaultNumberOption)(mnfd, 0, 20, mnfdDefault);
    var mxfdActualDefault = Math.max(mnfd, mxfdDefault);
    mxfd = (0, _DefaultNumberOption.DefaultNumberOption)(mxfd, mnfd, 20, mxfdActualDefault);
    internalSlots.minimumFractionDigits = mnfd;
    internalSlots.maximumFractionDigits = mxfd;
  } else if (notation === 'compact') {
    internalSlots.roundingType = 'compactRounding';
  } else {
    internalSlots.roundingType = 'fractionDigits';
    internalSlots.minimumFractionDigits = mnfdDefault;
    internalSlots.maximumFractionDigits = mxfdDefault;
  }
}
},{"../GetNumberOption":"../node_modules/@formatjs/ecma402-abstract/lib/GetNumberOption.js","../DefaultNumberOption":"../node_modules/@formatjs/ecma402-abstract/lib/DefaultNumberOption.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/InitializeNumberFormat.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InitializeNumberFormat = InitializeNumberFormat;

var _CanonicalizeLocaleList = require("../CanonicalizeLocaleList");

var _GetOption = require("../GetOption");

var _ResolveLocale = require("../ResolveLocale");

var _SetNumberFormatUnitOptions = require("./SetNumberFormatUnitOptions");

var _CurrencyDigits = require("./CurrencyDigits");

var _SetNumberFormatDigitOptions = require("./SetNumberFormatDigitOptions");

var _utils = require("../utils");

var _CoerceOptionsToObject = require("../CoerceOptionsToObject");

/**
 * https://tc39.es/ecma402/#sec-initializenumberformat
 */
function InitializeNumberFormat(nf, locales, opts, _a) {
  var getInternalSlots = _a.getInternalSlots,
      localeData = _a.localeData,
      availableLocales = _a.availableLocales,
      numberingSystemNames = _a.numberingSystemNames,
      getDefaultLocale = _a.getDefaultLocale,
      currencyDigitsData = _a.currencyDigitsData; // @ts-ignore

  var requestedLocales = (0, _CanonicalizeLocaleList.CanonicalizeLocaleList)(locales);
  var options = (0, _CoerceOptionsToObject.CoerceOptionsToObject)(opts);
  var opt = Object.create(null);
  var matcher = (0, _GetOption.GetOption)(options, 'localeMatcher', 'string', ['lookup', 'best fit'], 'best fit');
  opt.localeMatcher = matcher;
  var numberingSystem = (0, _GetOption.GetOption)(options, 'numberingSystem', 'string', undefined, undefined);

  if (numberingSystem !== undefined && numberingSystemNames.indexOf(numberingSystem) < 0) {
    // 8.a. If numberingSystem does not match the Unicode Locale Identifier type nonterminal,
    // throw a RangeError exception.
    throw RangeError("Invalid numberingSystems: " + numberingSystem);
  }

  opt.nu = numberingSystem;
  var r = (0, _ResolveLocale.ResolveLocale)(availableLocales, requestedLocales, opt, // [[RelevantExtensionKeys]] slot, which is a constant
  ['nu'], localeData, getDefaultLocale);
  var dataLocaleData = localeData[r.dataLocale];
  (0, _utils.invariant)(!!dataLocaleData, "Missing locale data for " + r.dataLocale);
  var internalSlots = getInternalSlots(nf);
  internalSlots.locale = r.locale;
  internalSlots.dataLocale = r.dataLocale;
  internalSlots.numberingSystem = r.nu;
  internalSlots.dataLocaleData = dataLocaleData;
  (0, _SetNumberFormatUnitOptions.SetNumberFormatUnitOptions)(nf, options, {
    getInternalSlots: getInternalSlots
  });
  var style = internalSlots.style;
  var mnfdDefault;
  var mxfdDefault;

  if (style === 'currency') {
    var currency = internalSlots.currency;
    var cDigits = (0, _CurrencyDigits.CurrencyDigits)(currency, {
      currencyDigitsData: currencyDigitsData
    });
    mnfdDefault = cDigits;
    mxfdDefault = cDigits;
  } else {
    mnfdDefault = 0;
    mxfdDefault = style === 'percent' ? 0 : 3;
  }

  var notation = (0, _GetOption.GetOption)(options, 'notation', 'string', ['standard', 'scientific', 'engineering', 'compact'], 'standard');
  internalSlots.notation = notation;
  (0, _SetNumberFormatDigitOptions.SetNumberFormatDigitOptions)(internalSlots, options, mnfdDefault, mxfdDefault, notation);
  var compactDisplay = (0, _GetOption.GetOption)(options, 'compactDisplay', 'string', ['short', 'long'], 'short');

  if (notation === 'compact') {
    internalSlots.compactDisplay = compactDisplay;
  }

  var useGrouping = (0, _GetOption.GetOption)(options, 'useGrouping', 'boolean', undefined, true);
  internalSlots.useGrouping = useGrouping;
  var signDisplay = (0, _GetOption.GetOption)(options, 'signDisplay', 'string', ['auto', 'never', 'always', 'exceptZero'], 'auto');
  internalSlots.signDisplay = signDisplay;
  return nf;
}
},{"../CanonicalizeLocaleList":"../node_modules/@formatjs/ecma402-abstract/lib/CanonicalizeLocaleList.js","../GetOption":"../node_modules/@formatjs/ecma402-abstract/lib/GetOption.js","../ResolveLocale":"../node_modules/@formatjs/ecma402-abstract/lib/ResolveLocale.js","./SetNumberFormatUnitOptions":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/SetNumberFormatUnitOptions.js","./CurrencyDigits":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/CurrencyDigits.js","./SetNumberFormatDigitOptions":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/SetNumberFormatDigitOptions.js","../utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js","../CoerceOptionsToObject":"../node_modules/@formatjs/ecma402-abstract/lib/CoerceOptionsToObject.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/PartitionPattern.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PartitionPattern = PartitionPattern;

var _utils = require("./utils");

/**
 * https://tc39.es/ecma402/#sec-partitionpattern
 * @param pattern
 */
function PartitionPattern(pattern) {
  var result = [];
  var beginIndex = pattern.indexOf('{');
  var endIndex = 0;
  var nextIndex = 0;
  var length = pattern.length;

  while (beginIndex < pattern.length && beginIndex > -1) {
    endIndex = pattern.indexOf('}', beginIndex);
    (0, _utils.invariant)(endIndex > beginIndex, "Invalid pattern " + pattern);

    if (beginIndex > nextIndex) {
      result.push({
        type: 'literal',
        value: pattern.substring(nextIndex, beginIndex)
      });
    }

    result.push({
      type: pattern.substring(beginIndex + 1, endIndex),
      value: undefined
    });
    nextIndex = endIndex + 1;
    beginIndex = pattern.indexOf('{', nextIndex);
  }

  if (nextIndex < length) {
    result.push({
      type: 'literal',
      value: pattern.substring(nextIndex, length)
    });
  }

  return result;
}
},{"./utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/LookupSupportedLocales.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LookupSupportedLocales = LookupSupportedLocales;

var _utils = require("./utils");

var _BestAvailableLocale = require("./BestAvailableLocale");

/**
 * https://tc39.es/ecma402/#sec-lookupsupportedlocales
 * @param availableLocales
 * @param requestedLocales
 */
function LookupSupportedLocales(availableLocales, requestedLocales) {
  var subset = [];

  for (var _i = 0, requestedLocales_1 = requestedLocales; _i < requestedLocales_1.length; _i++) {
    var locale = requestedLocales_1[_i];
    var noExtensionLocale = locale.replace(_utils.UNICODE_EXTENSION_SEQUENCE_REGEX, '');
    var availableLocale = (0, _BestAvailableLocale.BestAvailableLocale)(availableLocales, noExtensionLocale);

    if (availableLocale) {
      subset.push(availableLocale);
    }
  }

  return subset;
}
},{"./utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js","./BestAvailableLocale":"../node_modules/@formatjs/ecma402-abstract/lib/BestAvailableLocale.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/SupportedLocales.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SupportedLocales = SupportedLocales;

var _ = require("./262");

var _GetOption = require("./GetOption");

var _LookupSupportedLocales = require("./LookupSupportedLocales");

/**
 * https://tc39.es/ecma402/#sec-supportedlocales
 * @param availableLocales
 * @param requestedLocales
 * @param options
 */
function SupportedLocales(availableLocales, requestedLocales, options) {
  var matcher = 'best fit';

  if (options !== undefined) {
    options = (0, _.ToObject)(options);
    matcher = (0, _GetOption.GetOption)(options, 'localeMatcher', 'string', ['lookup', 'best fit'], 'best fit');
  }

  if (matcher === 'best fit') {
    return (0, _LookupSupportedLocales.LookupSupportedLocales)(availableLocales, requestedLocales);
  }

  return (0, _LookupSupportedLocales.LookupSupportedLocales)(availableLocales, requestedLocales);
}
},{"./262":"../node_modules/@formatjs/ecma402-abstract/lib/262.js","./GetOption":"../node_modules/@formatjs/ecma402-abstract/lib/GetOption.js","./LookupSupportedLocales":"../node_modules/@formatjs/ecma402-abstract/lib/LookupSupportedLocales.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/data.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isMissingLocaleDataError = isMissingLocaleDataError;

var _tslib = require("tslib");

var MissingLocaleDataError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(MissingLocaleDataError, _super);

  function MissingLocaleDataError() {
    var _this = _super !== null && _super.apply(this, arguments) || this;

    _this.type = 'MISSING_LOCALE_DATA';
    return _this;
  }

  return MissingLocaleDataError;
}(Error);

function isMissingLocaleDataError(e) {
  return e.type === 'MISSING_LOCALE_DATA';
}
},{"tslib":"../node_modules/tslib/tslib.es6.js"}],"../node_modules/@formatjs/ecma402-abstract/lib/types/relative-time.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/types/date-time.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RangePatternType = void 0;
var RangePatternType;
exports.RangePatternType = RangePatternType;

(function (RangePatternType) {
  RangePatternType["startRange"] = "startRange";
  RangePatternType["shared"] = "shared";
  RangePatternType["endRange"] = "endRange";
})(RangePatternType || (exports.RangePatternType = RangePatternType = {}));
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/types/list.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/types/plural-rules.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/types/number.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/types/displaynames.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
},{}],"../node_modules/@formatjs/ecma402-abstract/lib/index.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  _formatToParts: true,
  getInternalSlot: true,
  getMultiInternalSlots: true,
  isLiteralPart: true,
  setInternalSlot: true,
  setMultiInternalSlots: true,
  getMagnitude: true,
  defineProperty: true,
  invariant: true,
  isMissingLocaleDataError: true
};
Object.defineProperty(exports, "_formatToParts", {
  enumerable: true,
  get: function () {
    return _format_to_parts.default;
  }
});
Object.defineProperty(exports, "getInternalSlot", {
  enumerable: true,
  get: function () {
    return _utils.getInternalSlot;
  }
});
Object.defineProperty(exports, "getMultiInternalSlots", {
  enumerable: true,
  get: function () {
    return _utils.getMultiInternalSlots;
  }
});
Object.defineProperty(exports, "isLiteralPart", {
  enumerable: true,
  get: function () {
    return _utils.isLiteralPart;
  }
});
Object.defineProperty(exports, "setInternalSlot", {
  enumerable: true,
  get: function () {
    return _utils.setInternalSlot;
  }
});
Object.defineProperty(exports, "setMultiInternalSlots", {
  enumerable: true,
  get: function () {
    return _utils.setMultiInternalSlots;
  }
});
Object.defineProperty(exports, "getMagnitude", {
  enumerable: true,
  get: function () {
    return _utils.getMagnitude;
  }
});
Object.defineProperty(exports, "defineProperty", {
  enumerable: true,
  get: function () {
    return _utils.defineProperty;
  }
});
Object.defineProperty(exports, "invariant", {
  enumerable: true,
  get: function () {
    return _utils.invariant;
  }
});
Object.defineProperty(exports, "isMissingLocaleDataError", {
  enumerable: true,
  get: function () {
    return _data.isMissingLocaleDataError;
  }
});

var _CanonicalizeLocaleList = require("./CanonicalizeLocaleList");

Object.keys(_CanonicalizeLocaleList).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _CanonicalizeLocaleList[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _CanonicalizeLocaleList[key];
    }
  });
});

var _CanonicalizeTimeZoneName = require("./CanonicalizeTimeZoneName");

Object.keys(_CanonicalizeTimeZoneName).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _CanonicalizeTimeZoneName[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _CanonicalizeTimeZoneName[key];
    }
  });
});

var _CoerceOptionsToObject = require("./CoerceOptionsToObject");

Object.keys(_CoerceOptionsToObject).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _CoerceOptionsToObject[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _CoerceOptionsToObject[key];
    }
  });
});

var _GetNumberOption = require("./GetNumberOption");

Object.keys(_GetNumberOption).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _GetNumberOption[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _GetNumberOption[key];
    }
  });
});

var _GetOption = require("./GetOption");

Object.keys(_GetOption).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _GetOption[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _GetOption[key];
    }
  });
});

var _GetOptionsObject = require("./GetOptionsObject");

Object.keys(_GetOptionsObject).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _GetOptionsObject[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _GetOptionsObject[key];
    }
  });
});

var _IsSanctionedSimpleUnitIdentifier = require("./IsSanctionedSimpleUnitIdentifier");

Object.keys(_IsSanctionedSimpleUnitIdentifier).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _IsSanctionedSimpleUnitIdentifier[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _IsSanctionedSimpleUnitIdentifier[key];
    }
  });
});

var _IsValidTimeZoneName = require("./IsValidTimeZoneName");

Object.keys(_IsValidTimeZoneName).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _IsValidTimeZoneName[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _IsValidTimeZoneName[key];
    }
  });
});

var _IsWellFormedCurrencyCode = require("./IsWellFormedCurrencyCode");

Object.keys(_IsWellFormedCurrencyCode).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _IsWellFormedCurrencyCode[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _IsWellFormedCurrencyCode[key];
    }
  });
});

var _IsWellFormedUnitIdentifier = require("./IsWellFormedUnitIdentifier");

Object.keys(_IsWellFormedUnitIdentifier).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _IsWellFormedUnitIdentifier[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _IsWellFormedUnitIdentifier[key];
    }
  });
});

var _ComputeExponent = require("./NumberFormat/ComputeExponent");

Object.keys(_ComputeExponent).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _ComputeExponent[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _ComputeExponent[key];
    }
  });
});

var _ComputeExponentForMagnitude = require("./NumberFormat/ComputeExponentForMagnitude");

Object.keys(_ComputeExponentForMagnitude).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _ComputeExponentForMagnitude[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _ComputeExponentForMagnitude[key];
    }
  });
});

var _CurrencyDigits = require("./NumberFormat/CurrencyDigits");

Object.keys(_CurrencyDigits).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _CurrencyDigits[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _CurrencyDigits[key];
    }
  });
});

var _FormatNumericToParts = require("./NumberFormat/FormatNumericToParts");

Object.keys(_FormatNumericToParts).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _FormatNumericToParts[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _FormatNumericToParts[key];
    }
  });
});

var _FormatNumericToString = require("./NumberFormat/FormatNumericToString");

Object.keys(_FormatNumericToString).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _FormatNumericToString[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _FormatNumericToString[key];
    }
  });
});

var _InitializeNumberFormat = require("./NumberFormat/InitializeNumberFormat");

Object.keys(_InitializeNumberFormat).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _InitializeNumberFormat[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _InitializeNumberFormat[key];
    }
  });
});

var _PartitionNumberPattern = require("./NumberFormat/PartitionNumberPattern");

Object.keys(_PartitionNumberPattern).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _PartitionNumberPattern[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _PartitionNumberPattern[key];
    }
  });
});

var _SetNumberFormatDigitOptions = require("./NumberFormat/SetNumberFormatDigitOptions");

Object.keys(_SetNumberFormatDigitOptions).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _SetNumberFormatDigitOptions[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _SetNumberFormatDigitOptions[key];
    }
  });
});

var _SetNumberFormatUnitOptions = require("./NumberFormat/SetNumberFormatUnitOptions");

Object.keys(_SetNumberFormatUnitOptions).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _SetNumberFormatUnitOptions[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _SetNumberFormatUnitOptions[key];
    }
  });
});

var _ToRawFixed = require("./NumberFormat/ToRawFixed");

Object.keys(_ToRawFixed).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _ToRawFixed[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _ToRawFixed[key];
    }
  });
});

var _ToRawPrecision = require("./NumberFormat/ToRawPrecision");

Object.keys(_ToRawPrecision).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _ToRawPrecision[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _ToRawPrecision[key];
    }
  });
});

var _format_to_parts = _interopRequireDefault(require("./NumberFormat/format_to_parts"));

var _PartitionPattern = require("./PartitionPattern");

Object.keys(_PartitionPattern).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _PartitionPattern[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _PartitionPattern[key];
    }
  });
});

var _ResolveLocale = require("./ResolveLocale");

Object.keys(_ResolveLocale).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _ResolveLocale[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _ResolveLocale[key];
    }
  });
});

var _SupportedLocales = require("./SupportedLocales");

Object.keys(_SupportedLocales).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _SupportedLocales[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _SupportedLocales[key];
    }
  });
});

var _utils = require("./utils");

var _data = require("./data");

var _relativeTime = require("./types/relative-time");

Object.keys(_relativeTime).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _relativeTime[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _relativeTime[key];
    }
  });
});

var _dateTime = require("./types/date-time");

Object.keys(_dateTime).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _dateTime[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _dateTime[key];
    }
  });
});

var _list = require("./types/list");

Object.keys(_list).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _list[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _list[key];
    }
  });
});

var _pluralRules = require("./types/plural-rules");

Object.keys(_pluralRules).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _pluralRules[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _pluralRules[key];
    }
  });
});

var _number = require("./types/number");

Object.keys(_number).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _number[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _number[key];
    }
  });
});

var _displaynames = require("./types/displaynames");

Object.keys(_displaynames).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _displaynames[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _displaynames[key];
    }
  });
});

var _ = require("./262");

Object.keys(_).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _[key];
    }
  });
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
},{"./CanonicalizeLocaleList":"../node_modules/@formatjs/ecma402-abstract/lib/CanonicalizeLocaleList.js","./CanonicalizeTimeZoneName":"../node_modules/@formatjs/ecma402-abstract/lib/CanonicalizeTimeZoneName.js","./CoerceOptionsToObject":"../node_modules/@formatjs/ecma402-abstract/lib/CoerceOptionsToObject.js","./GetNumberOption":"../node_modules/@formatjs/ecma402-abstract/lib/GetNumberOption.js","./GetOption":"../node_modules/@formatjs/ecma402-abstract/lib/GetOption.js","./GetOptionsObject":"../node_modules/@formatjs/ecma402-abstract/lib/GetOptionsObject.js","./IsSanctionedSimpleUnitIdentifier":"../node_modules/@formatjs/ecma402-abstract/lib/IsSanctionedSimpleUnitIdentifier.js","./IsValidTimeZoneName":"../node_modules/@formatjs/ecma402-abstract/lib/IsValidTimeZoneName.js","./IsWellFormedCurrencyCode":"../node_modules/@formatjs/ecma402-abstract/lib/IsWellFormedCurrencyCode.js","./IsWellFormedUnitIdentifier":"../node_modules/@formatjs/ecma402-abstract/lib/IsWellFormedUnitIdentifier.js","./NumberFormat/ComputeExponent":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ComputeExponent.js","./NumberFormat/ComputeExponentForMagnitude":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ComputeExponentForMagnitude.js","./NumberFormat/CurrencyDigits":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/CurrencyDigits.js","./NumberFormat/FormatNumericToParts":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/FormatNumericToParts.js","./NumberFormat/FormatNumericToString":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/FormatNumericToString.js","./NumberFormat/InitializeNumberFormat":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/InitializeNumberFormat.js","./NumberFormat/PartitionNumberPattern":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/PartitionNumberPattern.js","./NumberFormat/SetNumberFormatDigitOptions":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/SetNumberFormatDigitOptions.js","./NumberFormat/SetNumberFormatUnitOptions":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/SetNumberFormatUnitOptions.js","./NumberFormat/ToRawFixed":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ToRawFixed.js","./NumberFormat/ToRawPrecision":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/ToRawPrecision.js","./NumberFormat/format_to_parts":"../node_modules/@formatjs/ecma402-abstract/lib/NumberFormat/format_to_parts.js","./PartitionPattern":"../node_modules/@formatjs/ecma402-abstract/lib/PartitionPattern.js","./ResolveLocale":"../node_modules/@formatjs/ecma402-abstract/lib/ResolveLocale.js","./SupportedLocales":"../node_modules/@formatjs/ecma402-abstract/lib/SupportedLocales.js","./utils":"../node_modules/@formatjs/ecma402-abstract/lib/utils.js","./data":"../node_modules/@formatjs/ecma402-abstract/lib/data.js","./types/relative-time":"../node_modules/@formatjs/ecma402-abstract/lib/types/relative-time.js","./types/date-time":"../node_modules/@formatjs/ecma402-abstract/lib/types/date-time.js","./types/list":"../node_modules/@formatjs/ecma402-abstract/lib/types/list.js","./types/plural-rules":"../node_modules/@formatjs/ecma402-abstract/lib/types/plural-rules.js","./types/number":"../node_modules/@formatjs/ecma402-abstract/lib/types/number.js","./types/displaynames":"../node_modules/@formatjs/ecma402-abstract/lib/types/displaynames.js","./262":"../node_modules/@formatjs/ecma402-abstract/lib/262.js"}],"../node_modules/@formatjs/intl/lib/src/types.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
},{}],"../node_modules/@formatjs/icu-messageformat-parser/lib/error.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ErrorKind = void 0;
var ErrorKind;
exports.ErrorKind = ErrorKind;

(function (ErrorKind) {
  /** Argument is unclosed (e.g. `{0`) */
  ErrorKind[ErrorKind["EXPECT_ARGUMENT_CLOSING_BRACE"] = 1] = "EXPECT_ARGUMENT_CLOSING_BRACE";
  /** Argument is empty (e.g. `{}`). */

  ErrorKind[ErrorKind["EMPTY_ARGUMENT"] = 2] = "EMPTY_ARGUMENT";
  /** Argument is malformed (e.g. `{foo!}``) */

  ErrorKind[ErrorKind["MALFORMED_ARGUMENT"] = 3] = "MALFORMED_ARGUMENT";
  /** Expect an argument type (e.g. `{foo,}`) */

  ErrorKind[ErrorKind["EXPECT_ARGUMENT_TYPE"] = 4] = "EXPECT_ARGUMENT_TYPE";
  /** Unsupported argument type (e.g. `{foo,foo}`) */

  ErrorKind[ErrorKind["INVALID_ARGUMENT_TYPE"] = 5] = "INVALID_ARGUMENT_TYPE";
  /** Expect an argument style (e.g. `{foo, number, }`) */

  ErrorKind[ErrorKind["EXPECT_ARGUMENT_STYLE"] = 6] = "EXPECT_ARGUMENT_STYLE";
  /** The number skeleton is invalid. */

  ErrorKind[ErrorKind["INVALID_NUMBER_SKELETON"] = 7] = "INVALID_NUMBER_SKELETON";
  /** The date time skeleton is invalid. */

  ErrorKind[ErrorKind["INVALID_DATE_TIME_SKELETON"] = 8] = "INVALID_DATE_TIME_SKELETON";
  /** Exepct a number skeleton following the `::` (e.g. `{foo, number, ::}`) */

  ErrorKind[ErrorKind["EXPECT_NUMBER_SKELETON"] = 9] = "EXPECT_NUMBER_SKELETON";
  /** Exepct a date time skeleton following the `::` (e.g. `{foo, date, ::}`) */

  ErrorKind[ErrorKind["EXPECT_DATE_TIME_SKELETON"] = 10] = "EXPECT_DATE_TIME_SKELETON";
  /** Unmatched apostrophes in the argument style (e.g. `{foo, number, 'test`) */

  ErrorKind[ErrorKind["UNCLOSED_QUOTE_IN_ARGUMENT_STYLE"] = 11] = "UNCLOSED_QUOTE_IN_ARGUMENT_STYLE";
  /** Missing select argument options (e.g. `{foo, select}`) */

  ErrorKind[ErrorKind["EXPECT_SELECT_ARGUMENT_OPTIONS"] = 12] = "EXPECT_SELECT_ARGUMENT_OPTIONS";
  /** Expecting an offset value in `plural` or `selectordinal` argument (e.g `{foo, plural, offset}`) */

  ErrorKind[ErrorKind["EXPECT_PLURAL_ARGUMENT_OFFSET_VALUE"] = 13] = "EXPECT_PLURAL_ARGUMENT_OFFSET_VALUE";
  /** Offset value in `plural` or `selectordinal` is invalid (e.g. `{foo, plural, offset: x}`) */

  ErrorKind[ErrorKind["INVALID_PLURAL_ARGUMENT_OFFSET_VALUE"] = 14] = "INVALID_PLURAL_ARGUMENT_OFFSET_VALUE";
  /** Expecting a selector in `select` argument (e.g `{foo, select}`) */

  ErrorKind[ErrorKind["EXPECT_SELECT_ARGUMENT_SELECTOR"] = 15] = "EXPECT_SELECT_ARGUMENT_SELECTOR";
  /** Expecting a selector in `plural` or `selectordinal` argument (e.g `{foo, plural}`) */

  ErrorKind[ErrorKind["EXPECT_PLURAL_ARGUMENT_SELECTOR"] = 16] = "EXPECT_PLURAL_ARGUMENT_SELECTOR";
  /** Expecting a message fragment after the `select` selector (e.g. `{foo, select, apple}`) */

  ErrorKind[ErrorKind["EXPECT_SELECT_ARGUMENT_SELECTOR_FRAGMENT"] = 17] = "EXPECT_SELECT_ARGUMENT_SELECTOR_FRAGMENT";
  /**
   * Expecting a message fragment after the `plural` or `selectordinal` selector
   * (e.g. `{foo, plural, one}`)
   */

  ErrorKind[ErrorKind["EXPECT_PLURAL_ARGUMENT_SELECTOR_FRAGMENT"] = 18] = "EXPECT_PLURAL_ARGUMENT_SELECTOR_FRAGMENT";
  /** Selector in `plural` or `selectordinal` is malformed (e.g. `{foo, plural, =x {#}}`) */

  ErrorKind[ErrorKind["INVALID_PLURAL_ARGUMENT_SELECTOR"] = 19] = "INVALID_PLURAL_ARGUMENT_SELECTOR";
  /**
   * Duplicate selectors in `plural` or `selectordinal` argument.
   * (e.g. {foo, plural, one {#} one {#}})
   */

  ErrorKind[ErrorKind["DUPLICATE_PLURAL_ARGUMENT_SELECTOR"] = 20] = "DUPLICATE_PLURAL_ARGUMENT_SELECTOR";
  /** Duplicate selectors in `select` argument.
   * (e.g. {foo, select, apple {apple} apple {apple}})
   */

  ErrorKind[ErrorKind["DUPLICATE_SELECT_ARGUMENT_SELECTOR"] = 21] = "DUPLICATE_SELECT_ARGUMENT_SELECTOR";
  /** Plural or select argument option must have `other` clause. */

  ErrorKind[ErrorKind["MISSING_OTHER_CLAUSE"] = 22] = "MISSING_OTHER_CLAUSE";
  /** The tag is malformed. (e.g. `<bold!>foo</bold!>) */

  ErrorKind[ErrorKind["INVALID_TAG"] = 23] = "INVALID_TAG";
  /** The tag name is invalid. (e.g. `<123>foo</123>`) */

  ErrorKind[ErrorKind["INVALID_TAG_NAME"] = 25] = "INVALID_TAG_NAME";
  /** The closing tag does not match the opening tag. (e.g. `<bold>foo</italic>`) */

  ErrorKind[ErrorKind["UNMATCHED_CLOSING_TAG"] = 26] = "UNMATCHED_CLOSING_TAG";
  /** The opening tag has unmatched closing tag. (e.g. `<bold>foo`) */

  ErrorKind[ErrorKind["UNCLOSED_TAG"] = 27] = "UNCLOSED_TAG";
})(ErrorKind || (exports.ErrorKind = ErrorKind = {}));
},{}],"../node_modules/@formatjs/icu-messageformat-parser/lib/types.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isLiteralElement = isLiteralElement;
exports.isArgumentElement = isArgumentElement;
exports.isNumberElement = isNumberElement;
exports.isDateElement = isDateElement;
exports.isTimeElement = isTimeElement;
exports.isSelectElement = isSelectElement;
exports.isPluralElement = isPluralElement;
exports.isPoundElement = isPoundElement;
exports.isTagElement = isTagElement;
exports.isNumberSkeleton = isNumberSkeleton;
exports.isDateTimeSkeleton = isDateTimeSkeleton;
exports.createLiteralElement = createLiteralElement;
exports.createNumberElement = createNumberElement;
exports.SKELETON_TYPE = exports.TYPE = void 0;
var TYPE;
exports.TYPE = TYPE;

(function (TYPE) {
  /**
   * Raw text
   */
  TYPE[TYPE["literal"] = 0] = "literal";
  /**
   * Variable w/o any format, e.g `var` in `this is a {var}`
   */

  TYPE[TYPE["argument"] = 1] = "argument";
  /**
   * Variable w/ number format
   */

  TYPE[TYPE["number"] = 2] = "number";
  /**
   * Variable w/ date format
   */

  TYPE[TYPE["date"] = 3] = "date";
  /**
   * Variable w/ time format
   */

  TYPE[TYPE["time"] = 4] = "time";
  /**
   * Variable w/ select format
   */

  TYPE[TYPE["select"] = 5] = "select";
  /**
   * Variable w/ plural format
   */

  TYPE[TYPE["plural"] = 6] = "plural";
  /**
   * Only possible within plural argument.
   * This is the `#` symbol that will be substituted with the count.
   */

  TYPE[TYPE["pound"] = 7] = "pound";
  /**
   * XML-like tag
   */

  TYPE[TYPE["tag"] = 8] = "tag";
})(TYPE || (exports.TYPE = TYPE = {}));

var SKELETON_TYPE;
exports.SKELETON_TYPE = SKELETON_TYPE;

(function (SKELETON_TYPE) {
  SKELETON_TYPE[SKELETON_TYPE["number"] = 0] = "number";
  SKELETON_TYPE[SKELETON_TYPE["dateTime"] = 1] = "dateTime";
})(SKELETON_TYPE || (exports.SKELETON_TYPE = SKELETON_TYPE = {}));
/**
 * Type Guards
 */


function isLiteralElement(el) {
  return el.type === TYPE.literal;
}

function isArgumentElement(el) {
  return el.type === TYPE.argument;
}

function isNumberElement(el) {
  return el.type === TYPE.number;
}

function isDateElement(el) {
  return el.type === TYPE.date;
}

function isTimeElement(el) {
  return el.type === TYPE.time;
}

function isSelectElement(el) {
  return el.type === TYPE.select;
}

function isPluralElement(el) {
  return el.type === TYPE.plural;
}

function isPoundElement(el) {
  return el.type === TYPE.pound;
}

function isTagElement(el) {
  return el.type === TYPE.tag;
}

function isNumberSkeleton(el) {
  return !!(el && typeof el === 'object' && el.type === SKELETON_TYPE.number);
}

function isDateTimeSkeleton(el) {
  return !!(el && typeof el === 'object' && el.type === SKELETON_TYPE.dateTime);
}

function createLiteralElement(value) {
  return {
    type: TYPE.literal,
    value: value
  };
}

function createNumberElement(value, style) {
  return {
    type: TYPE.number,
    value: value,
    style: style
  };
}
},{}],"../node_modules/@formatjs/icu-messageformat-parser/lib/regex.generated.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WHITE_SPACE_REGEX = exports.SPACE_SEPARATOR_REGEX = void 0;
// @generated from regex-gen.ts
var SPACE_SEPARATOR_REGEX = /[ \xA0\u1680\u2000-\u200A\u202F\u205F\u3000]/;
exports.SPACE_SEPARATOR_REGEX = SPACE_SEPARATOR_REGEX;
var WHITE_SPACE_REGEX = /[\t-\r \x85\u200E\u200F\u2028\u2029]/;
exports.WHITE_SPACE_REGEX = WHITE_SPACE_REGEX;
},{}],"../node_modules/@formatjs/icu-skeleton-parser/lib/date-time.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parseDateTimeSkeleton = parseDateTimeSkeleton;

/**
 * https://unicode.org/reports/tr35/tr35-dates.html#Date_Field_Symbol_Table
 * Credit: https://github.com/caridy/intl-datetimeformat-pattern/blob/master/index.js
 * with some tweaks
 */
var DATE_TIME_REGEX = /(?:[Eec]{1,6}|G{1,5}|[Qq]{1,5}|(?:[yYur]+|U{1,5})|[ML]{1,5}|d{1,2}|D{1,3}|F{1}|[abB]{1,5}|[hkHK]{1,2}|w{1,2}|W{1}|m{1,2}|s{1,2}|[zZOvVxX]{1,4})(?=([^']*'[^']*')*[^']*$)/g;
/**
 * Parse Date time skeleton into Intl.DateTimeFormatOptions
 * Ref: https://unicode.org/reports/tr35/tr35-dates.html#Date_Field_Symbol_Table
 * @public
 * @param skeleton skeleton string
 */

function parseDateTimeSkeleton(skeleton) {
  var result = {};
  skeleton.replace(DATE_TIME_REGEX, function (match) {
    var len = match.length;

    switch (match[0]) {
      // Era
      case 'G':
        result.era = len === 4 ? 'long' : len === 5 ? 'narrow' : 'short';
        break;
      // Year

      case 'y':
        result.year = len === 2 ? '2-digit' : 'numeric';
        break;

      case 'Y':
      case 'u':
      case 'U':
      case 'r':
        throw new RangeError('`Y/u/U/r` (year) patterns are not supported, use `y` instead');
      // Quarter

      case 'q':
      case 'Q':
        throw new RangeError('`q/Q` (quarter) patterns are not supported');
      // Month

      case 'M':
      case 'L':
        result.month = ['numeric', '2-digit', 'short', 'long', 'narrow'][len - 1];
        break;
      // Week

      case 'w':
      case 'W':
        throw new RangeError('`w/W` (week) patterns are not supported');

      case 'd':
        result.day = ['numeric', '2-digit'][len - 1];
        break;

      case 'D':
      case 'F':
      case 'g':
        throw new RangeError('`D/F/g` (day) patterns are not supported, use `d` instead');
      // Weekday

      case 'E':
        result.weekday = len === 4 ? 'short' : len === 5 ? 'narrow' : 'short';
        break;

      case 'e':
        if (len < 4) {
          throw new RangeError('`e..eee` (weekday) patterns are not supported');
        }

        result.weekday = ['short', 'long', 'narrow', 'short'][len - 4];
        break;

      case 'c':
        if (len < 4) {
          throw new RangeError('`c..ccc` (weekday) patterns are not supported');
        }

        result.weekday = ['short', 'long', 'narrow', 'short'][len - 4];
        break;
      // Period

      case 'a':
        // AM, PM
        result.hour12 = true;
        break;

      case 'b': // am, pm, noon, midnight

      case 'B':
        // flexible day periods
        throw new RangeError('`b/B` (period) patterns are not supported, use `a` instead');
      // Hour

      case 'h':
        result.hourCycle = 'h12';
        result.hour = ['numeric', '2-digit'][len - 1];
        break;

      case 'H':
        result.hourCycle = 'h23';
        result.hour = ['numeric', '2-digit'][len - 1];
        break;

      case 'K':
        result.hourCycle = 'h11';
        result.hour = ['numeric', '2-digit'][len - 1];
        break;

      case 'k':
        result.hourCycle = 'h24';
        result.hour = ['numeric', '2-digit'][len - 1];
        break;

      case 'j':
      case 'J':
      case 'C':
        throw new RangeError('`j/J/C` (hour) patterns are not supported, use `h/H/K/k` instead');
      // Minute

      case 'm':
        result.minute = ['numeric', '2-digit'][len - 1];
        break;
      // Second

      case 's':
        result.second = ['numeric', '2-digit'][len - 1];
        break;

      case 'S':
      case 'A':
        throw new RangeError('`S/A` (second) patterns are not supported, use `s` instead');
      // Zone

      case 'z':
        // 1..3, 4: specific non-location format
        result.timeZoneName = len < 4 ? 'short' : 'long';
        break;

      case 'Z': // 1..3, 4, 5: The ISO8601 varios formats

      case 'O': // 1, 4: miliseconds in day short, long

      case 'v': // 1, 4: generic non-location format

      case 'V': // 1, 2, 3, 4: time zone ID or city

      case 'X': // 1, 2, 3, 4: The ISO8601 varios formats

      case 'x':
        // 1, 2, 3, 4: The ISO8601 varios formats
        throw new RangeError('`Z/O/v/V/X/x` (timeZone) patterns are not supported, use `z` instead');
    }

    return '';
  });
  return result;
}
},{}],"../node_modules/@formatjs/icu-skeleton-parser/lib/regex.generated.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WHITE_SPACE_REGEX = void 0;
// @generated from regex-gen.ts
var WHITE_SPACE_REGEX = /[\t-\r \x85\u200E\u200F\u2028\u2029]/i;
exports.WHITE_SPACE_REGEX = WHITE_SPACE_REGEX;
},{}],"../node_modules/@formatjs/icu-skeleton-parser/lib/number.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parseNumberSkeletonFromString = parseNumberSkeletonFromString;
exports.parseNumberSkeleton = parseNumberSkeleton;

var _tslib = require("tslib");

var _regex = require("./regex.generated");

function parseNumberSkeletonFromString(skeleton) {
  if (skeleton.length === 0) {
    throw new Error('Number skeleton cannot be empty');
  } // Parse the skeleton


  var stringTokens = skeleton.split(_regex.WHITE_SPACE_REGEX).filter(function (x) {
    return x.length > 0;
  });
  var tokens = [];

  for (var _i = 0, stringTokens_1 = stringTokens; _i < stringTokens_1.length; _i++) {
    var stringToken = stringTokens_1[_i];
    var stemAndOptions = stringToken.split('/');

    if (stemAndOptions.length === 0) {
      throw new Error('Invalid number skeleton');
    }

    var stem = stemAndOptions[0],
        options = stemAndOptions.slice(1);

    for (var _a = 0, options_1 = options; _a < options_1.length; _a++) {
      var option = options_1[_a];

      if (option.length === 0) {
        throw new Error('Invalid number skeleton');
      }
    }

    tokens.push({
      stem: stem,
      options: options
    });
  }

  return tokens;
}

function icuUnitToEcma(unit) {
  return unit.replace(/^(.*?)-/, '');
}

var FRACTION_PRECISION_REGEX = /^\.(?:(0+)(\*)?|(#+)|(0+)(#+))$/g;
var SIGNIFICANT_PRECISION_REGEX = /^(@+)?(\+|#+)?$/g;
var INTEGER_WIDTH_REGEX = /(\*)(0+)|(#+)(0+)|(0+)/g;
var CONCISE_INTEGER_WIDTH_REGEX = /^(0+)$/;

function parseSignificantPrecision(str) {
  var result = {};
  str.replace(SIGNIFICANT_PRECISION_REGEX, function (_, g1, g2) {
    // @@@ case
    if (typeof g2 !== 'string') {
      result.minimumSignificantDigits = g1.length;
      result.maximumSignificantDigits = g1.length;
    } // @@@+ case
    else if (g2 === '+') {
        result.minimumSignificantDigits = g1.length;
      } // .### case
      else if (g1[0] === '#') {
          result.maximumSignificantDigits = g1.length;
        } // .@@## or .@@@ case
        else {
            result.minimumSignificantDigits = g1.length;
            result.maximumSignificantDigits = g1.length + (typeof g2 === 'string' ? g2.length : 0);
          }

    return '';
  });
  return result;
}

function parseSign(str) {
  switch (str) {
    case 'sign-auto':
      return {
        signDisplay: 'auto'
      };

    case 'sign-accounting':
    case '()':
      return {
        currencySign: 'accounting'
      };

    case 'sign-always':
    case '+!':
      return {
        signDisplay: 'always'
      };

    case 'sign-accounting-always':
    case '()!':
      return {
        signDisplay: 'always',
        currencySign: 'accounting'
      };

    case 'sign-except-zero':
    case '+?':
      return {
        signDisplay: 'exceptZero'
      };

    case 'sign-accounting-except-zero':
    case '()?':
      return {
        signDisplay: 'exceptZero',
        currencySign: 'accounting'
      };

    case 'sign-never':
    case '+_':
      return {
        signDisplay: 'never'
      };
  }
}

function parseConciseScientificAndEngineeringStem(stem) {
  // Engineering
  var result;

  if (stem[0] === 'E' && stem[1] === 'E') {
    result = {
      notation: 'engineering'
    };
    stem = stem.slice(2);
  } else if (stem[0] === 'E') {
    result = {
      notation: 'scientific'
    };
    stem = stem.slice(1);
  }

  if (result) {
    var signDisplay = stem.slice(0, 2);

    if (signDisplay === '+!') {
      result.signDisplay = 'always';
      stem = stem.slice(2);
    } else if (signDisplay === '+?') {
      result.signDisplay = 'exceptZero';
      stem = stem.slice(2);
    }

    if (!CONCISE_INTEGER_WIDTH_REGEX.test(stem)) {
      throw new Error('Malformed concise eng/scientific notation');
    }

    result.minimumIntegerDigits = stem.length;
  }

  return result;
}

function parseNotationOptions(opt) {
  var result = {};
  var signOpts = parseSign(opt);

  if (signOpts) {
    return signOpts;
  }

  return result;
}
/**
 * https://github.com/unicode-org/icu/blob/master/docs/userguide/format_parse/numbers/skeletons.md#skeleton-stems-and-options
 */


function parseNumberSkeleton(tokens) {
  var result = {};

  for (var _i = 0, tokens_1 = tokens; _i < tokens_1.length; _i++) {
    var token = tokens_1[_i];

    switch (token.stem) {
      case 'percent':
      case '%':
        result.style = 'percent';
        continue;

      case '%x100':
        result.style = 'percent';
        result.scale = 100;
        continue;

      case 'currency':
        result.style = 'currency';
        result.currency = token.options[0];
        continue;

      case 'group-off':
      case ',_':
        result.useGrouping = false;
        continue;

      case 'precision-integer':
      case '.':
        result.maximumFractionDigits = 0;
        continue;

      case 'measure-unit':
      case 'unit':
        result.style = 'unit';
        result.unit = icuUnitToEcma(token.options[0]);
        continue;

      case 'compact-short':
      case 'K':
        result.notation = 'compact';
        result.compactDisplay = 'short';
        continue;

      case 'compact-long':
      case 'KK':
        result.notation = 'compact';
        result.compactDisplay = 'long';
        continue;

      case 'scientific':
        result = (0, _tslib.__assign)((0, _tslib.__assign)((0, _tslib.__assign)({}, result), {
          notation: 'scientific'
        }), token.options.reduce(function (all, opt) {
          return (0, _tslib.__assign)((0, _tslib.__assign)({}, all), parseNotationOptions(opt));
        }, {}));
        continue;

      case 'engineering':
        result = (0, _tslib.__assign)((0, _tslib.__assign)((0, _tslib.__assign)({}, result), {
          notation: 'engineering'
        }), token.options.reduce(function (all, opt) {
          return (0, _tslib.__assign)((0, _tslib.__assign)({}, all), parseNotationOptions(opt));
        }, {}));
        continue;

      case 'notation-simple':
        result.notation = 'standard';
        continue;
      // https://github.com/unicode-org/icu/blob/master/icu4c/source/i18n/unicode/unumberformatter.h

      case 'unit-width-narrow':
        result.currencyDisplay = 'narrowSymbol';
        result.unitDisplay = 'narrow';
        continue;

      case 'unit-width-short':
        result.currencyDisplay = 'code';
        result.unitDisplay = 'short';
        continue;

      case 'unit-width-full-name':
        result.currencyDisplay = 'name';
        result.unitDisplay = 'long';
        continue;

      case 'unit-width-iso-code':
        result.currencyDisplay = 'symbol';
        continue;

      case 'scale':
        result.scale = parseFloat(token.options[0]);
        continue;
      // https://unicode-org.github.io/icu/userguide/format_parse/numbers/skeletons.html#integer-width

      case 'integer-width':
        if (token.options.length > 1) {
          throw new RangeError('integer-width stems only accept a single optional option');
        }

        token.options[0].replace(INTEGER_WIDTH_REGEX, function (_, g1, g2, g3, g4, g5) {
          if (g1) {
            result.minimumIntegerDigits = g2.length;
          } else if (g3 && g4) {
            throw new Error('We currently do not support maximum integer digits');
          } else if (g5) {
            throw new Error('We currently do not support exact integer digits');
          }

          return '';
        });
        continue;
    } // https://unicode-org.github.io/icu/userguide/format_parse/numbers/skeletons.html#integer-width


    if (CONCISE_INTEGER_WIDTH_REGEX.test(token.stem)) {
      result.minimumIntegerDigits = token.stem.length;
      continue;
    }

    if (FRACTION_PRECISION_REGEX.test(token.stem)) {
      // Precision
      // https://unicode-org.github.io/icu/userguide/format_parse/numbers/skeletons.html#fraction-precision
      // precision-integer case
      if (token.options.length > 1) {
        throw new RangeError('Fraction-precision stems only accept a single optional option');
      }

      token.stem.replace(FRACTION_PRECISION_REGEX, function (_, g1, g2, g3, g4, g5) {
        // .000* case (before ICU67 it was .000+)
        if (g2 === '*') {
          result.minimumFractionDigits = g1.length;
        } // .### case
        else if (g3 && g3[0] === '#') {
            result.maximumFractionDigits = g3.length;
          } // .00## case
          else if (g4 && g5) {
              result.minimumFractionDigits = g4.length;
              result.maximumFractionDigits = g4.length + g5.length;
            } else {
              result.minimumFractionDigits = g1.length;
              result.maximumFractionDigits = g1.length;
            }

        return '';
      });

      if (token.options.length) {
        result = (0, _tslib.__assign)((0, _tslib.__assign)({}, result), parseSignificantPrecision(token.options[0]));
      }

      continue;
    } // https://unicode-org.github.io/icu/userguide/format_parse/numbers/skeletons.html#significant-digits-precision


    if (SIGNIFICANT_PRECISION_REGEX.test(token.stem)) {
      result = (0, _tslib.__assign)((0, _tslib.__assign)({}, result), parseSignificantPrecision(token.stem));
      continue;
    }

    var signOpts = parseSign(token.stem);

    if (signOpts) {
      result = (0, _tslib.__assign)((0, _tslib.__assign)({}, result), signOpts);
    }

    var conciseScientificAndEngineeringOpts = parseConciseScientificAndEngineeringStem(token.stem);

    if (conciseScientificAndEngineeringOpts) {
      result = (0, _tslib.__assign)((0, _tslib.__assign)({}, result), conciseScientificAndEngineeringOpts);
    }
  }

  return result;
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","./regex.generated":"../node_modules/@formatjs/icu-skeleton-parser/lib/regex.generated.js"}],"../node_modules/@formatjs/icu-skeleton-parser/lib/index.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _dateTime = require("./date-time");

Object.keys(_dateTime).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (key in exports && exports[key] === _dateTime[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _dateTime[key];
    }
  });
});

var _number = require("./number");

Object.keys(_number).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (key in exports && exports[key] === _number[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _number[key];
    }
  });
});
},{"./date-time":"../node_modules/@formatjs/icu-skeleton-parser/lib/date-time.js","./number":"../node_modules/@formatjs/icu-skeleton-parser/lib/number.js"}],"../node_modules/@formatjs/icu-messageformat-parser/lib/parser.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Parser = void 0;

var _tslib = require("tslib");

var _error = require("./error");

var _types = require("./types");

var _regex = require("./regex.generated");

var _icuSkeletonParser = require("@formatjs/icu-skeleton-parser");

var _a;

var SPACE_SEPARATOR_START_REGEX = new RegExp("^" + _regex.SPACE_SEPARATOR_REGEX.source + "*");
var SPACE_SEPARATOR_END_REGEX = new RegExp(_regex.SPACE_SEPARATOR_REGEX.source + "*$");

function createLocation(start, end) {
  return {
    start: start,
    end: end
  };
} // #region Ponyfills
// Consolidate these variables up top for easier toggling during debugging


var hasNativeStartsWith = !!String.prototype.startsWith;
var hasNativeFromCodePoint = !!String.fromCodePoint;
var hasNativeFromEntries = !!Object.fromEntries;
var hasNativeCodePointAt = !!String.prototype.codePointAt;
var hasTrimStart = !!String.prototype.trimStart;
var hasTrimEnd = !!String.prototype.trimEnd;
var hasNativeIsSafeInteger = !!Number.isSafeInteger;
var isSafeInteger = hasNativeIsSafeInteger ? Number.isSafeInteger : function (n) {
  return typeof n === 'number' && isFinite(n) && Math.floor(n) === n && Math.abs(n) <= 0x1fffffffffffff;
}; // IE11 does not support y and u.

var REGEX_SUPPORTS_U_AND_Y = true;

try {
  var re = RE('([^\\p{White_Space}\\p{Pattern_Syntax}]*)', 'yu');
  /**
   * legacy Edge or Xbox One browser
   * Unicode flag support: supported
   * Pattern_Syntax support: not supported
   * See https://github.com/formatjs/formatjs/issues/2822
   */

  REGEX_SUPPORTS_U_AND_Y = ((_a = re.exec('a')) === null || _a === void 0 ? void 0 : _a[0]) === 'a';
} catch (_) {
  REGEX_SUPPORTS_U_AND_Y = false;
}

var startsWith = hasNativeStartsWith ? // Native
function startsWith(s, search, position) {
  return s.startsWith(search, position);
} : // For IE11
function startsWith(s, search, position) {
  return s.slice(position, position + search.length) === search;
};
var fromCodePoint = hasNativeFromCodePoint ? String.fromCodePoint : // IE11
function fromCodePoint() {
  var codePoints = [];

  for (var _i = 0; _i < arguments.length; _i++) {
    codePoints[_i] = arguments[_i];
  }

  var elements = '';
  var length = codePoints.length;
  var i = 0;
  var code;

  while (length > i) {
    code = codePoints[i++];
    if (code > 0x10ffff) throw RangeError(code + ' is not a valid code point');
    elements += code < 0x10000 ? String.fromCharCode(code) : String.fromCharCode(((code -= 0x10000) >> 10) + 0xd800, code % 0x400 + 0xdc00);
  }

  return elements;
};
var fromEntries = // native
hasNativeFromEntries ? Object.fromEntries : // Ponyfill
function fromEntries(entries) {
  var obj = {};

  for (var _i = 0, entries_1 = entries; _i < entries_1.length; _i++) {
    var _a = entries_1[_i],
        k = _a[0],
        v = _a[1];
    obj[k] = v;
  }

  return obj;
};
var codePointAt = hasNativeCodePointAt ? // Native
function codePointAt(s, index) {
  return s.codePointAt(index);
} : // IE 11
function codePointAt(s, index) {
  var size = s.length;

  if (index < 0 || index >= size) {
    return undefined;
  }

  var first = s.charCodeAt(index);
  var second;
  return first < 0xd800 || first > 0xdbff || index + 1 === size || (second = s.charCodeAt(index + 1)) < 0xdc00 || second > 0xdfff ? first : (first - 0xd800 << 10) + (second - 0xdc00) + 0x10000;
};
var trimStart = hasTrimStart ? // Native
function trimStart(s) {
  return s.trimStart();
} : // Ponyfill
function trimStart(s) {
  return s.replace(SPACE_SEPARATOR_START_REGEX, '');
};
var trimEnd = hasTrimEnd ? // Native
function trimEnd(s) {
  return s.trimEnd();
} : // Ponyfill
function trimEnd(s) {
  return s.replace(SPACE_SEPARATOR_END_REGEX, '');
}; // Prevent minifier to translate new RegExp to literal form that might cause syntax error on IE11.

function RE(s, flag) {
  return new RegExp(s, flag);
} // #endregion


var matchIdentifierAtIndex;

if (REGEX_SUPPORTS_U_AND_Y) {
  // Native
  var IDENTIFIER_PREFIX_RE_1 = RE('([^\\p{White_Space}\\p{Pattern_Syntax}]*)', 'yu');

  matchIdentifierAtIndex = function matchIdentifierAtIndex(s, index) {
    var _a;

    IDENTIFIER_PREFIX_RE_1.lastIndex = index;
    var match = IDENTIFIER_PREFIX_RE_1.exec(s);
    return (_a = match[1]) !== null && _a !== void 0 ? _a : '';
  };
} else {
  // IE11
  matchIdentifierAtIndex = function matchIdentifierAtIndex(s, index) {
    var match = [];

    while (true) {
      var c = codePointAt(s, index);

      if (c === undefined || _isWhiteSpace(c) || _isPatternSyntax(c)) {
        break;
      }

      match.push(c);
      index += c >= 0x10000 ? 2 : 1;
    }

    return fromCodePoint.apply(void 0, match);
  };
}

var Parser =
/** @class */
function () {
  function Parser(message, options) {
    if (options === void 0) {
      options = {};
    }

    this.message = message;
    this.position = {
      offset: 0,
      line: 1,
      column: 1
    };
    this.ignoreTag = !!options.ignoreTag;
    this.requiresOtherClause = !!options.requiresOtherClause;
    this.shouldParseSkeletons = !!options.shouldParseSkeletons;
  }

  Parser.prototype.parse = function () {
    if (this.offset() !== 0) {
      throw Error('parser can only be used once');
    }

    return this.parseMessage(0, '', false);
  };

  Parser.prototype.parseMessage = function (nestingLevel, parentArgType, expectingCloseTag) {
    var elements = [];

    while (!this.isEOF()) {
      var char = this.char();

      if (char === 123
      /* `{` */
      ) {
          var result = this.parseArgument(nestingLevel, expectingCloseTag);

          if (result.err) {
            return result;
          }

          elements.push(result.val);
        } else if (char === 125
      /* `}` */
      && nestingLevel > 0) {
        break;
      } else if (char === 35
      /* `#` */
      && (parentArgType === 'plural' || parentArgType === 'selectordinal')) {
        var position = this.clonePosition();
        this.bump();
        elements.push({
          type: _types.TYPE.pound,
          location: createLocation(position, this.clonePosition())
        });
      } else if (char === 60
      /* `<` */
      && !this.ignoreTag && this.peek() === 47 // char code for '/'
      ) {
          if (expectingCloseTag) {
            break;
          } else {
            return this.error(_error.ErrorKind.UNMATCHED_CLOSING_TAG, createLocation(this.clonePosition(), this.clonePosition()));
          }
        } else if (char === 60
      /* `<` */
      && !this.ignoreTag && _isAlpha(this.peek() || 0)) {
        var result = this.parseTag(nestingLevel, parentArgType);

        if (result.err) {
          return result;
        }

        elements.push(result.val);
      } else {
        var result = this.parseLiteral(nestingLevel, parentArgType);

        if (result.err) {
          return result;
        }

        elements.push(result.val);
      }
    }

    return {
      val: elements,
      err: null
    };
  };
  /**
   * A tag name must start with an ASCII lower/upper case letter. The grammar is based on the
   * [custom element name][] except that a dash is NOT always mandatory and uppercase letters
   * are accepted:
   *
   * ```
   * tag ::= "<" tagName (whitespace)* "/>" | "<" tagName (whitespace)* ">" message "</" tagName (whitespace)* ">"
   * tagName ::= [a-z] (PENChar)*
   * PENChar ::=
   *     "-" | "." | [0-9] | "_" | [a-z] | [A-Z] | #xB7 | [#xC0-#xD6] | [#xD8-#xF6] | [#xF8-#x37D] |
   *     [#x37F-#x1FFF] | [#x200C-#x200D] | [#x203F-#x2040] | [#x2070-#x218F] | [#x2C00-#x2FEF] |
   *     [#x3001-#xD7FF] | [#xF900-#xFDCF] | [#xFDF0-#xFFFD] | [#x10000-#xEFFFF]
   * ```
   *
   * [custom element name]: https://html.spec.whatwg.org/multipage/custom-elements.html#valid-custom-element-name
   * NOTE: We're a bit more lax here since HTML technically does not allow uppercase HTML element but we do
   * since other tag-based engines like React allow it
   */


  Parser.prototype.parseTag = function (nestingLevel, parentArgType) {
    var startPosition = this.clonePosition();
    this.bump(); // `<`

    var tagName = this.parseTagName();
    this.bumpSpace();

    if (this.bumpIf('/>')) {
      // Self closing tag
      return {
        val: {
          type: _types.TYPE.literal,
          value: "<" + tagName + "/>",
          location: createLocation(startPosition, this.clonePosition())
        },
        err: null
      };
    } else if (this.bumpIf('>')) {
      var childrenResult = this.parseMessage(nestingLevel + 1, parentArgType, true);

      if (childrenResult.err) {
        return childrenResult;
      }

      var children = childrenResult.val; // Expecting a close tag

      var endTagStartPosition = this.clonePosition();

      if (this.bumpIf('</')) {
        if (this.isEOF() || !_isAlpha(this.char())) {
          return this.error(_error.ErrorKind.INVALID_TAG, createLocation(endTagStartPosition, this.clonePosition()));
        }

        var closingTagNameStartPosition = this.clonePosition();
        var closingTagName = this.parseTagName();

        if (tagName !== closingTagName) {
          return this.error(_error.ErrorKind.UNMATCHED_CLOSING_TAG, createLocation(closingTagNameStartPosition, this.clonePosition()));
        }

        this.bumpSpace();

        if (!this.bumpIf('>')) {
          return this.error(_error.ErrorKind.INVALID_TAG, createLocation(endTagStartPosition, this.clonePosition()));
        }

        return {
          val: {
            type: _types.TYPE.tag,
            value: tagName,
            children: children,
            location: createLocation(startPosition, this.clonePosition())
          },
          err: null
        };
      } else {
        return this.error(_error.ErrorKind.UNCLOSED_TAG, createLocation(startPosition, this.clonePosition()));
      }
    } else {
      return this.error(_error.ErrorKind.INVALID_TAG, createLocation(startPosition, this.clonePosition()));
    }
  };
  /**
   * This method assumes that the caller has peeked ahead for the first tag character.
   */


  Parser.prototype.parseTagName = function () {
    var startOffset = this.offset();
    this.bump(); // the first tag name character

    while (!this.isEOF() && _isPotentialElementNameChar(this.char())) {
      this.bump();
    }

    return this.message.slice(startOffset, this.offset());
  };

  Parser.prototype.parseLiteral = function (nestingLevel, parentArgType) {
    var start = this.clonePosition();
    var value = '';

    while (true) {
      var parseQuoteResult = this.tryParseQuote(parentArgType);

      if (parseQuoteResult) {
        value += parseQuoteResult;
        continue;
      }

      var parseUnquotedResult = this.tryParseUnquoted(nestingLevel, parentArgType);

      if (parseUnquotedResult) {
        value += parseUnquotedResult;
        continue;
      }

      var parseLeftAngleResult = this.tryParseLeftAngleBracket();

      if (parseLeftAngleResult) {
        value += parseLeftAngleResult;
        continue;
      }

      break;
    }

    var location = createLocation(start, this.clonePosition());
    return {
      val: {
        type: _types.TYPE.literal,
        value: value,
        location: location
      },
      err: null
    };
  };

  Parser.prototype.tryParseLeftAngleBracket = function () {
    if (!this.isEOF() && this.char() === 60
    /* `<` */
    && (this.ignoreTag || // If at the opening tag or closing tag position, bail.
    !_isAlphaOrSlash(this.peek() || 0))) {
      this.bump(); // `<`

      return '<';
    }

    return null;
  };
  /**
   * Starting with ICU 4.8, an ASCII apostrophe only starts quoted text if it immediately precedes
   * a character that requires quoting (that is, "only where needed"), and works the same in
   * nested messages as on the top level of the pattern. The new behavior is otherwise compatible.
   */


  Parser.prototype.tryParseQuote = function (parentArgType) {
    if (this.isEOF() || this.char() !== 39
    /* `'` */
    ) {
        return null;
      } // Parse escaped char following the apostrophe, or early return if there is no escaped char.
    // Check if is valid escaped character


    switch (this.peek()) {
      case 39
      /* `'` */
      :
        // double quote, should return as a single quote.
        this.bump();
        this.bump();
        return "'";
      // '{', '<', '>', '}'

      case 123:
      case 60:
      case 62:
      case 125:
        break;

      case 35:
        // '#'
        if (parentArgType === 'plural' || parentArgType === 'selectordinal') {
          break;
        }

        return null;

      default:
        return null;
    }

    this.bump(); // apostrophe

    var codePoints = [this.char()]; // escaped char

    this.bump(); // read chars until the optional closing apostrophe is found

    while (!this.isEOF()) {
      var ch = this.char();

      if (ch === 39
      /* `'` */
      ) {
          if (this.peek() === 39
          /* `'` */
          ) {
              codePoints.push(39); // Bump one more time because we need to skip 2 characters.

              this.bump();
            } else {
            // Optional closing apostrophe.
            this.bump();
            break;
          }
        } else {
        codePoints.push(ch);
      }

      this.bump();
    }

    return fromCodePoint.apply(void 0, codePoints);
  };

  Parser.prototype.tryParseUnquoted = function (nestingLevel, parentArgType) {
    if (this.isEOF()) {
      return null;
    }

    var ch = this.char();

    if (ch === 60
    /* `<` */
    || ch === 123
    /* `{` */
    || ch === 35
    /* `#` */
    && (parentArgType === 'plural' || parentArgType === 'selectordinal') || ch === 125
    /* `}` */
    && nestingLevel > 0) {
      return null;
    } else {
      this.bump();
      return fromCodePoint(ch);
    }
  };

  Parser.prototype.parseArgument = function (nestingLevel, expectingCloseTag) {
    var openingBracePosition = this.clonePosition();
    this.bump(); // `{`

    this.bumpSpace();

    if (this.isEOF()) {
      return this.error(_error.ErrorKind.EXPECT_ARGUMENT_CLOSING_BRACE, createLocation(openingBracePosition, this.clonePosition()));
    }

    if (this.char() === 125
    /* `}` */
    ) {
        this.bump();
        return this.error(_error.ErrorKind.EMPTY_ARGUMENT, createLocation(openingBracePosition, this.clonePosition()));
      } // argument name


    var value = this.parseIdentifierIfPossible().value;

    if (!value) {
      return this.error(_error.ErrorKind.MALFORMED_ARGUMENT, createLocation(openingBracePosition, this.clonePosition()));
    }

    this.bumpSpace();

    if (this.isEOF()) {
      return this.error(_error.ErrorKind.EXPECT_ARGUMENT_CLOSING_BRACE, createLocation(openingBracePosition, this.clonePosition()));
    }

    switch (this.char()) {
      // Simple argument: `{name}`
      case 125
      /* `}` */
      :
        {
          this.bump(); // `}`

          return {
            val: {
              type: _types.TYPE.argument,
              // value does not include the opening and closing braces.
              value: value,
              location: createLocation(openingBracePosition, this.clonePosition())
            },
            err: null
          };
        }
      // Argument with options: `{name, format, ...}`

      case 44
      /* `,` */
      :
        {
          this.bump(); // `,`

          this.bumpSpace();

          if (this.isEOF()) {
            return this.error(_error.ErrorKind.EXPECT_ARGUMENT_CLOSING_BRACE, createLocation(openingBracePosition, this.clonePosition()));
          }

          return this.parseArgumentOptions(nestingLevel, expectingCloseTag, value, openingBracePosition);
        }

      default:
        return this.error(_error.ErrorKind.MALFORMED_ARGUMENT, createLocation(openingBracePosition, this.clonePosition()));
    }
  };
  /**
   * Advance the parser until the end of the identifier, if it is currently on
   * an identifier character. Return an empty string otherwise.
   */


  Parser.prototype.parseIdentifierIfPossible = function () {
    var startingPosition = this.clonePosition();
    var startOffset = this.offset();
    var value = matchIdentifierAtIndex(this.message, startOffset);
    var endOffset = startOffset + value.length;
    this.bumpTo(endOffset);
    var endPosition = this.clonePosition();
    var location = createLocation(startingPosition, endPosition);
    return {
      value: value,
      location: location
    };
  };

  Parser.prototype.parseArgumentOptions = function (nestingLevel, expectingCloseTag, value, openingBracePosition) {
    var _a; // Parse this range:
    // {name, type, style}
    //        ^---^


    var typeStartPosition = this.clonePosition();
    var argType = this.parseIdentifierIfPossible().value;
    var typeEndPosition = this.clonePosition();

    switch (argType) {
      case '':
        // Expecting a style string number, date, time, plural, selectordinal, or select.
        return this.error(_error.ErrorKind.EXPECT_ARGUMENT_TYPE, createLocation(typeStartPosition, typeEndPosition));

      case 'number':
      case 'date':
      case 'time':
        {
          // Parse this range:
          // {name, number, style}
          //              ^-------^
          this.bumpSpace();
          var styleAndLocation = null;

          if (this.bumpIf(',')) {
            this.bumpSpace();
            var styleStartPosition = this.clonePosition();
            var result = this.parseSimpleArgStyleIfPossible();

            if (result.err) {
              return result;
            }

            var style = trimEnd(result.val);

            if (style.length === 0) {
              return this.error(_error.ErrorKind.EXPECT_ARGUMENT_STYLE, createLocation(this.clonePosition(), this.clonePosition()));
            }

            var styleLocation = createLocation(styleStartPosition, this.clonePosition());
            styleAndLocation = {
              style: style,
              styleLocation: styleLocation
            };
          }

          var argCloseResult = this.tryParseArgumentClose(openingBracePosition);

          if (argCloseResult.err) {
            return argCloseResult;
          }

          var location_1 = createLocation(openingBracePosition, this.clonePosition()); // Extract style or skeleton

          if (styleAndLocation && startsWith(styleAndLocation === null || styleAndLocation === void 0 ? void 0 : styleAndLocation.style, '::', 0)) {
            // Skeleton starts with `::`.
            var skeleton = trimStart(styleAndLocation.style.slice(2));

            if (argType === 'number') {
              var result = this.parseNumberSkeletonFromString(skeleton, styleAndLocation.styleLocation);

              if (result.err) {
                return result;
              }

              return {
                val: {
                  type: _types.TYPE.number,
                  value: value,
                  location: location_1,
                  style: result.val
                },
                err: null
              };
            } else {
              if (skeleton.length === 0) {
                return this.error(_error.ErrorKind.EXPECT_DATE_TIME_SKELETON, location_1);
              }

              var style = {
                type: _types.SKELETON_TYPE.dateTime,
                pattern: skeleton,
                location: styleAndLocation.styleLocation,
                parsedOptions: this.shouldParseSkeletons ? (0, _icuSkeletonParser.parseDateTimeSkeleton)(skeleton) : {}
              };
              var type = argType === 'date' ? _types.TYPE.date : _types.TYPE.time;
              return {
                val: {
                  type: type,
                  value: value,
                  location: location_1,
                  style: style
                },
                err: null
              };
            }
          } // Regular style or no style.


          return {
            val: {
              type: argType === 'number' ? _types.TYPE.number : argType === 'date' ? _types.TYPE.date : _types.TYPE.time,
              value: value,
              location: location_1,
              style: (_a = styleAndLocation === null || styleAndLocation === void 0 ? void 0 : styleAndLocation.style) !== null && _a !== void 0 ? _a : null
            },
            err: null
          };
        }

      case 'plural':
      case 'selectordinal':
      case 'select':
        {
          // Parse this range:
          // {name, plural, options}
          //              ^---------^
          var typeEndPosition_1 = this.clonePosition();
          this.bumpSpace();

          if (!this.bumpIf(',')) {
            return this.error(_error.ErrorKind.EXPECT_SELECT_ARGUMENT_OPTIONS, createLocation(typeEndPosition_1, (0, _tslib.__assign)({}, typeEndPosition_1)));
          }

          this.bumpSpace(); // Parse offset:
          // {name, plural, offset:1, options}
          //                ^-----^
          //
          // or the first option:
          //
          // {name, plural, one {...} other {...}}
          //                ^--^

          var identifierAndLocation = this.parseIdentifierIfPossible();
          var pluralOffset = 0;

          if (argType !== 'select' && identifierAndLocation.value === 'offset') {
            if (!this.bumpIf(':')) {
              return this.error(_error.ErrorKind.EXPECT_PLURAL_ARGUMENT_OFFSET_VALUE, createLocation(this.clonePosition(), this.clonePosition()));
            }

            this.bumpSpace();
            var result = this.tryParseDecimalInteger(_error.ErrorKind.EXPECT_PLURAL_ARGUMENT_OFFSET_VALUE, _error.ErrorKind.INVALID_PLURAL_ARGUMENT_OFFSET_VALUE);

            if (result.err) {
              return result;
            } // Parse another identifier for option parsing


            this.bumpSpace();
            identifierAndLocation = this.parseIdentifierIfPossible();
            pluralOffset = result.val;
          }

          var optionsResult = this.tryParsePluralOrSelectOptions(nestingLevel, argType, expectingCloseTag, identifierAndLocation);

          if (optionsResult.err) {
            return optionsResult;
          }

          var argCloseResult = this.tryParseArgumentClose(openingBracePosition);

          if (argCloseResult.err) {
            return argCloseResult;
          }

          var location_2 = createLocation(openingBracePosition, this.clonePosition());

          if (argType === 'select') {
            return {
              val: {
                type: _types.TYPE.select,
                value: value,
                options: fromEntries(optionsResult.val),
                location: location_2
              },
              err: null
            };
          } else {
            return {
              val: {
                type: _types.TYPE.plural,
                value: value,
                options: fromEntries(optionsResult.val),
                offset: pluralOffset,
                pluralType: argType === 'plural' ? 'cardinal' : 'ordinal',
                location: location_2
              },
              err: null
            };
          }
        }

      default:
        return this.error(_error.ErrorKind.INVALID_ARGUMENT_TYPE, createLocation(typeStartPosition, typeEndPosition));
    }
  };

  Parser.prototype.tryParseArgumentClose = function (openingBracePosition) {
    // Parse: {value, number, ::currency/GBP }
    //
    if (this.isEOF() || this.char() !== 125
    /* `}` */
    ) {
        return this.error(_error.ErrorKind.EXPECT_ARGUMENT_CLOSING_BRACE, createLocation(openingBracePosition, this.clonePosition()));
      }

    this.bump(); // `}`

    return {
      val: true,
      err: null
    };
  };
  /**
   * See: https://github.com/unicode-org/icu/blob/af7ed1f6d2298013dc303628438ec4abe1f16479/icu4c/source/common/messagepattern.cpp#L659
   */


  Parser.prototype.parseSimpleArgStyleIfPossible = function () {
    var nestedBraces = 0;
    var startPosition = this.clonePosition();

    while (!this.isEOF()) {
      var ch = this.char();

      switch (ch) {
        case 39
        /* `'` */
        :
          {
            // Treat apostrophe as quoting but include it in the style part.
            // Find the end of the quoted literal text.
            this.bump();
            var apostrophePosition = this.clonePosition();

            if (!this.bumpUntil("'")) {
              return this.error(_error.ErrorKind.UNCLOSED_QUOTE_IN_ARGUMENT_STYLE, createLocation(apostrophePosition, this.clonePosition()));
            }

            this.bump();
            break;
          }

        case 123
        /* `{` */
        :
          {
            nestedBraces += 1;
            this.bump();
            break;
          }

        case 125
        /* `}` */
        :
          {
            if (nestedBraces > 0) {
              nestedBraces -= 1;
            } else {
              return {
                val: this.message.slice(startPosition.offset, this.offset()),
                err: null
              };
            }

            break;
          }

        default:
          this.bump();
          break;
      }
    }

    return {
      val: this.message.slice(startPosition.offset, this.offset()),
      err: null
    };
  };

  Parser.prototype.parseNumberSkeletonFromString = function (skeleton, location) {
    var tokens = [];

    try {
      tokens = (0, _icuSkeletonParser.parseNumberSkeletonFromString)(skeleton);
    } catch (e) {
      return this.error(_error.ErrorKind.INVALID_NUMBER_SKELETON, location);
    }

    return {
      val: {
        type: _types.SKELETON_TYPE.number,
        tokens: tokens,
        location: location,
        parsedOptions: this.shouldParseSkeletons ? (0, _icuSkeletonParser.parseNumberSkeleton)(tokens) : {}
      },
      err: null
    };
  };
  /**
   * @param nesting_level The current nesting level of messages.
   *     This can be positive when parsing message fragment in select or plural argument options.
   * @param parent_arg_type The parent argument's type.
   * @param parsed_first_identifier If provided, this is the first identifier-like selector of
   *     the argument. It is a by-product of a previous parsing attempt.
   * @param expecting_close_tag If true, this message is directly or indirectly nested inside
   *     between a pair of opening and closing tags. The nested message will not parse beyond
   *     the closing tag boundary.
   */


  Parser.prototype.tryParsePluralOrSelectOptions = function (nestingLevel, parentArgType, expectCloseTag, parsedFirstIdentifier) {
    var _a;

    var hasOtherClause = false;
    var options = [];
    var parsedSelectors = new Set();
    var selector = parsedFirstIdentifier.value,
        selectorLocation = parsedFirstIdentifier.location; // Parse:
    // one {one apple}
    // ^--^

    while (true) {
      if (selector.length === 0) {
        var startPosition = this.clonePosition();

        if (parentArgType !== 'select' && this.bumpIf('=')) {
          // Try parse `={number}` selector
          var result = this.tryParseDecimalInteger(_error.ErrorKind.EXPECT_PLURAL_ARGUMENT_SELECTOR, _error.ErrorKind.INVALID_PLURAL_ARGUMENT_SELECTOR);

          if (result.err) {
            return result;
          }

          selectorLocation = createLocation(startPosition, this.clonePosition());
          selector = this.message.slice(startPosition.offset, this.offset());
        } else {
          break;
        }
      } // Duplicate selector clauses


      if (parsedSelectors.has(selector)) {
        return this.error(parentArgType === 'select' ? _error.ErrorKind.DUPLICATE_SELECT_ARGUMENT_SELECTOR : _error.ErrorKind.DUPLICATE_PLURAL_ARGUMENT_SELECTOR, selectorLocation);
      }

      if (selector === 'other') {
        hasOtherClause = true;
      } // Parse:
      // one {one apple}
      //     ^----------^


      this.bumpSpace();
      var openingBracePosition = this.clonePosition();

      if (!this.bumpIf('{')) {
        return this.error(parentArgType === 'select' ? _error.ErrorKind.EXPECT_SELECT_ARGUMENT_SELECTOR_FRAGMENT : _error.ErrorKind.EXPECT_PLURAL_ARGUMENT_SELECTOR_FRAGMENT, createLocation(this.clonePosition(), this.clonePosition()));
      }

      var fragmentResult = this.parseMessage(nestingLevel + 1, parentArgType, expectCloseTag);

      if (fragmentResult.err) {
        return fragmentResult;
      }

      var argCloseResult = this.tryParseArgumentClose(openingBracePosition);

      if (argCloseResult.err) {
        return argCloseResult;
      }

      options.push([selector, {
        value: fragmentResult.val,
        location: createLocation(openingBracePosition, this.clonePosition())
      }]); // Keep track of the existing selectors

      parsedSelectors.add(selector); // Prep next selector clause.

      this.bumpSpace();
      _a = this.parseIdentifierIfPossible(), selector = _a.value, selectorLocation = _a.location;
    }

    if (options.length === 0) {
      return this.error(parentArgType === 'select' ? _error.ErrorKind.EXPECT_SELECT_ARGUMENT_SELECTOR : _error.ErrorKind.EXPECT_PLURAL_ARGUMENT_SELECTOR, createLocation(this.clonePosition(), this.clonePosition()));
    }

    if (this.requiresOtherClause && !hasOtherClause) {
      return this.error(_error.ErrorKind.MISSING_OTHER_CLAUSE, createLocation(this.clonePosition(), this.clonePosition()));
    }

    return {
      val: options,
      err: null
    };
  };

  Parser.prototype.tryParseDecimalInteger = function (expectNumberError, invalidNumberError) {
    var sign = 1;
    var startingPosition = this.clonePosition();

    if (this.bumpIf('+')) {} else if (this.bumpIf('-')) {
      sign = -1;
    }

    var hasDigits = false;
    var decimal = 0;

    while (!this.isEOF()) {
      var ch = this.char();

      if (ch >= 48
      /* `0` */
      && ch <= 57
      /* `9` */
      ) {
          hasDigits = true;
          decimal = decimal * 10 + (ch - 48);
          this.bump();
        } else {
        break;
      }
    }

    var location = createLocation(startingPosition, this.clonePosition());

    if (!hasDigits) {
      return this.error(expectNumberError, location);
    }

    decimal *= sign;

    if (!isSafeInteger(decimal)) {
      return this.error(invalidNumberError, location);
    }

    return {
      val: decimal,
      err: null
    };
  };

  Parser.prototype.offset = function () {
    return this.position.offset;
  };

  Parser.prototype.isEOF = function () {
    return this.offset() === this.message.length;
  };

  Parser.prototype.clonePosition = function () {
    // This is much faster than `Object.assign` or spread.
    return {
      offset: this.position.offset,
      line: this.position.line,
      column: this.position.column
    };
  };
  /**
   * Return the code point at the current position of the parser.
   * Throws if the index is out of bound.
   */


  Parser.prototype.char = function () {
    var offset = this.position.offset;

    if (offset >= this.message.length) {
      throw Error('out of bound');
    }

    var code = codePointAt(this.message, offset);

    if (code === undefined) {
      throw Error("Offset " + offset + " is at invalid UTF-16 code unit boundary");
    }

    return code;
  };

  Parser.prototype.error = function (kind, location) {
    return {
      val: null,
      err: {
        kind: kind,
        message: this.message,
        location: location
      }
    };
  };
  /** Bump the parser to the next UTF-16 code unit. */


  Parser.prototype.bump = function () {
    if (this.isEOF()) {
      return;
    }

    var code = this.char();

    if (code === 10
    /* '\n' */
    ) {
        this.position.line += 1;
        this.position.column = 1;
        this.position.offset += 1;
      } else {
      this.position.column += 1; // 0 ~ 0x10000 -> unicode BMP, otherwise skip the surrogate pair.

      this.position.offset += code < 0x10000 ? 1 : 2;
    }
  };
  /**
   * If the substring starting at the current position of the parser has
   * the given prefix, then bump the parser to the character immediately
   * following the prefix and return true. Otherwise, don't bump the parser
   * and return false.
   */


  Parser.prototype.bumpIf = function (prefix) {
    if (startsWith(this.message, prefix, this.offset())) {
      for (var i = 0; i < prefix.length; i++) {
        this.bump();
      }

      return true;
    }

    return false;
  };
  /**
   * Bump the parser until the pattern character is found and return `true`.
   * Otherwise bump to the end of the file and return `false`.
   */


  Parser.prototype.bumpUntil = function (pattern) {
    var currentOffset = this.offset();
    var index = this.message.indexOf(pattern, currentOffset);

    if (index >= 0) {
      this.bumpTo(index);
      return true;
    } else {
      this.bumpTo(this.message.length);
      return false;
    }
  };
  /**
   * Bump the parser to the target offset.
   * If target offset is beyond the end of the input, bump the parser to the end of the input.
   */


  Parser.prototype.bumpTo = function (targetOffset) {
    if (this.offset() > targetOffset) {
      throw Error("targetOffset " + targetOffset + " must be greater than or equal to the current offset " + this.offset());
    }

    targetOffset = Math.min(targetOffset, this.message.length);

    while (true) {
      var offset = this.offset();

      if (offset === targetOffset) {
        break;
      }

      if (offset > targetOffset) {
        throw Error("targetOffset " + targetOffset + " is at invalid UTF-16 code unit boundary");
      }

      this.bump();

      if (this.isEOF()) {
        break;
      }
    }
  };
  /** advance the parser through all whitespace to the next non-whitespace code unit. */


  Parser.prototype.bumpSpace = function () {
    while (!this.isEOF() && _isWhiteSpace(this.char())) {
      this.bump();
    }
  };
  /**
   * Peek at the *next* Unicode codepoint in the input without advancing the parser.
   * If the input has been exhausted, then this returns null.
   */


  Parser.prototype.peek = function () {
    if (this.isEOF()) {
      return null;
    }

    var code = this.char();
    var offset = this.offset();
    var nextCode = this.message.charCodeAt(offset + (code >= 0x10000 ? 2 : 1));
    return nextCode !== null && nextCode !== void 0 ? nextCode : null;
  };

  return Parser;
}();

exports.Parser = Parser;

/**
 * This check if codepoint is alphabet (lower & uppercase)
 * @param codepoint
 * @returns
 */
function _isAlpha(codepoint) {
  return codepoint >= 97 && codepoint <= 122 || codepoint >= 65 && codepoint <= 90;
}

function _isAlphaOrSlash(codepoint) {
  return _isAlpha(codepoint) || codepoint === 47;
  /* '/' */
}
/** See `parseTag` function docs. */


function _isPotentialElementNameChar(c) {
  return c === 45
  /* '-' */
  || c === 46
  /* '.' */
  || c >= 48 && c <= 57
  /* 0..9 */
  || c === 95
  /* '_' */
  || c >= 97 && c <= 122
  /** a..z */
  || c >= 65 && c <= 90
  /* A..Z */
  || c == 0xb7 || c >= 0xc0 && c <= 0xd6 || c >= 0xd8 && c <= 0xf6 || c >= 0xf8 && c <= 0x37d || c >= 0x37f && c <= 0x1fff || c >= 0x200c && c <= 0x200d || c >= 0x203f && c <= 0x2040 || c >= 0x2070 && c <= 0x218f || c >= 0x2c00 && c <= 0x2fef || c >= 0x3001 && c <= 0xd7ff || c >= 0xf900 && c <= 0xfdcf || c >= 0xfdf0 && c <= 0xfffd || c >= 0x10000 && c <= 0xeffff;
}
/**
 * Code point equivalent of regex `\p{White_Space}`.
 * From: https://www.unicode.org/Public/UCD/latest/ucd/PropList.txt
 */


function _isWhiteSpace(c) {
  return c >= 0x0009 && c <= 0x000d || c === 0x0020 || c === 0x0085 || c >= 0x200e && c <= 0x200f || c === 0x2028 || c === 0x2029;
}
/**
 * Code point equivalent of regex `\p{Pattern_Syntax}`.
 * See https://www.unicode.org/Public/UCD/latest/ucd/PropList.txt
 */


function _isPatternSyntax(c) {
  return c >= 0x0021 && c <= 0x0023 || c === 0x0024 || c >= 0x0025 && c <= 0x0027 || c === 0x0028 || c === 0x0029 || c === 0x002a || c === 0x002b || c === 0x002c || c === 0x002d || c >= 0x002e && c <= 0x002f || c >= 0x003a && c <= 0x003b || c >= 0x003c && c <= 0x003e || c >= 0x003f && c <= 0x0040 || c === 0x005b || c === 0x005c || c === 0x005d || c === 0x005e || c === 0x0060 || c === 0x007b || c === 0x007c || c === 0x007d || c === 0x007e || c === 0x00a1 || c >= 0x00a2 && c <= 0x00a5 || c === 0x00a6 || c === 0x00a7 || c === 0x00a9 || c === 0x00ab || c === 0x00ac || c === 0x00ae || c === 0x00b0 || c === 0x00b1 || c === 0x00b6 || c === 0x00bb || c === 0x00bf || c === 0x00d7 || c === 0x00f7 || c >= 0x2010 && c <= 0x2015 || c >= 0x2016 && c <= 0x2017 || c === 0x2018 || c === 0x2019 || c === 0x201a || c >= 0x201b && c <= 0x201c || c === 0x201d || c === 0x201e || c === 0x201f || c >= 0x2020 && c <= 0x2027 || c >= 0x2030 && c <= 0x2038 || c === 0x2039 || c === 0x203a || c >= 0x203b && c <= 0x203e || c >= 0x2041 && c <= 0x2043 || c === 0x2044 || c === 0x2045 || c === 0x2046 || c >= 0x2047 && c <= 0x2051 || c === 0x2052 || c === 0x2053 || c >= 0x2055 && c <= 0x205e || c >= 0x2190 && c <= 0x2194 || c >= 0x2195 && c <= 0x2199 || c >= 0x219a && c <= 0x219b || c >= 0x219c && c <= 0x219f || c === 0x21a0 || c >= 0x21a1 && c <= 0x21a2 || c === 0x21a3 || c >= 0x21a4 && c <= 0x21a5 || c === 0x21a6 || c >= 0x21a7 && c <= 0x21ad || c === 0x21ae || c >= 0x21af && c <= 0x21cd || c >= 0x21ce && c <= 0x21cf || c >= 0x21d0 && c <= 0x21d1 || c === 0x21d2 || c === 0x21d3 || c === 0x21d4 || c >= 0x21d5 && c <= 0x21f3 || c >= 0x21f4 && c <= 0x22ff || c >= 0x2300 && c <= 0x2307 || c === 0x2308 || c === 0x2309 || c === 0x230a || c === 0x230b || c >= 0x230c && c <= 0x231f || c >= 0x2320 && c <= 0x2321 || c >= 0x2322 && c <= 0x2328 || c === 0x2329 || c === 0x232a || c >= 0x232b && c <= 0x237b || c === 0x237c || c >= 0x237d && c <= 0x239a || c >= 0x239b && c <= 0x23b3 || c >= 0x23b4 && c <= 0x23db || c >= 0x23dc && c <= 0x23e1 || c >= 0x23e2 && c <= 0x2426 || c >= 0x2427 && c <= 0x243f || c >= 0x2440 && c <= 0x244a || c >= 0x244b && c <= 0x245f || c >= 0x2500 && c <= 0x25b6 || c === 0x25b7 || c >= 0x25b8 && c <= 0x25c0 || c === 0x25c1 || c >= 0x25c2 && c <= 0x25f7 || c >= 0x25f8 && c <= 0x25ff || c >= 0x2600 && c <= 0x266e || c === 0x266f || c >= 0x2670 && c <= 0x2767 || c === 0x2768 || c === 0x2769 || c === 0x276a || c === 0x276b || c === 0x276c || c === 0x276d || c === 0x276e || c === 0x276f || c === 0x2770 || c === 0x2771 || c === 0x2772 || c === 0x2773 || c === 0x2774 || c === 0x2775 || c >= 0x2794 && c <= 0x27bf || c >= 0x27c0 && c <= 0x27c4 || c === 0x27c5 || c === 0x27c6 || c >= 0x27c7 && c <= 0x27e5 || c === 0x27e6 || c === 0x27e7 || c === 0x27e8 || c === 0x27e9 || c === 0x27ea || c === 0x27eb || c === 0x27ec || c === 0x27ed || c === 0x27ee || c === 0x27ef || c >= 0x27f0 && c <= 0x27ff || c >= 0x2800 && c <= 0x28ff || c >= 0x2900 && c <= 0x2982 || c === 0x2983 || c === 0x2984 || c === 0x2985 || c === 0x2986 || c === 0x2987 || c === 0x2988 || c === 0x2989 || c === 0x298a || c === 0x298b || c === 0x298c || c === 0x298d || c === 0x298e || c === 0x298f || c === 0x2990 || c === 0x2991 || c === 0x2992 || c === 0x2993 || c === 0x2994 || c === 0x2995 || c === 0x2996 || c === 0x2997 || c === 0x2998 || c >= 0x2999 && c <= 0x29d7 || c === 0x29d8 || c === 0x29d9 || c === 0x29da || c === 0x29db || c >= 0x29dc && c <= 0x29fb || c === 0x29fc || c === 0x29fd || c >= 0x29fe && c <= 0x2aff || c >= 0x2b00 && c <= 0x2b2f || c >= 0x2b30 && c <= 0x2b44 || c >= 0x2b45 && c <= 0x2b46 || c >= 0x2b47 && c <= 0x2b4c || c >= 0x2b4d && c <= 0x2b73 || c >= 0x2b74 && c <= 0x2b75 || c >= 0x2b76 && c <= 0x2b95 || c === 0x2b96 || c >= 0x2b97 && c <= 0x2bff || c >= 0x2e00 && c <= 0x2e01 || c === 0x2e02 || c === 0x2e03 || c === 0x2e04 || c === 0x2e05 || c >= 0x2e06 && c <= 0x2e08 || c === 0x2e09 || c === 0x2e0a || c === 0x2e0b || c === 0x2e0c || c === 0x2e0d || c >= 0x2e0e && c <= 0x2e16 || c === 0x2e17 || c >= 0x2e18 && c <= 0x2e19 || c === 0x2e1a || c === 0x2e1b || c === 0x2e1c || c === 0x2e1d || c >= 0x2e1e && c <= 0x2e1f || c === 0x2e20 || c === 0x2e21 || c === 0x2e22 || c === 0x2e23 || c === 0x2e24 || c === 0x2e25 || c === 0x2e26 || c === 0x2e27 || c === 0x2e28 || c === 0x2e29 || c >= 0x2e2a && c <= 0x2e2e || c === 0x2e2f || c >= 0x2e30 && c <= 0x2e39 || c >= 0x2e3a && c <= 0x2e3b || c >= 0x2e3c && c <= 0x2e3f || c === 0x2e40 || c === 0x2e41 || c === 0x2e42 || c >= 0x2e43 && c <= 0x2e4f || c >= 0x2e50 && c <= 0x2e51 || c === 0x2e52 || c >= 0x2e53 && c <= 0x2e7f || c >= 0x3001 && c <= 0x3003 || c === 0x3008 || c === 0x3009 || c === 0x300a || c === 0x300b || c === 0x300c || c === 0x300d || c === 0x300e || c === 0x300f || c === 0x3010 || c === 0x3011 || c >= 0x3012 && c <= 0x3013 || c === 0x3014 || c === 0x3015 || c === 0x3016 || c === 0x3017 || c === 0x3018 || c === 0x3019 || c === 0x301a || c === 0x301b || c === 0x301c || c === 0x301d || c >= 0x301e && c <= 0x301f || c === 0x3020 || c === 0x3030 || c === 0xfd3e || c === 0xfd3f || c >= 0xfe45 && c <= 0xfe46;
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","./error":"../node_modules/@formatjs/icu-messageformat-parser/lib/error.js","./types":"../node_modules/@formatjs/icu-messageformat-parser/lib/types.js","./regex.generated":"../node_modules/@formatjs/icu-messageformat-parser/lib/regex.generated.js","@formatjs/icu-skeleton-parser":"../node_modules/@formatjs/icu-skeleton-parser/lib/index.js"}],"../node_modules/@formatjs/icu-messageformat-parser/lib/index.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  parse: true
};
exports.parse = parse;

var _tslib = require("tslib");

var _error = require("./error");

var _parser = require("./parser");

var _types = require("./types");

Object.keys(_types).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _types[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _types[key];
    }
  });
});

function pruneLocation(els) {
  els.forEach(function (el) {
    delete el.location;

    if ((0, _types.isSelectElement)(el) || (0, _types.isPluralElement)(el)) {
      for (var k in el.options) {
        delete el.options[k].location;
        pruneLocation(el.options[k].value);
      }
    } else if ((0, _types.isNumberElement)(el) && (0, _types.isNumberSkeleton)(el.style)) {
      delete el.style.location;
    } else if (((0, _types.isDateElement)(el) || (0, _types.isTimeElement)(el)) && (0, _types.isDateTimeSkeleton)(el.style)) {
      delete el.style.location;
    } else if ((0, _types.isTagElement)(el)) {
      pruneLocation(el.children);
    }
  });
}

function parse(message, opts) {
  if (opts === void 0) {
    opts = {};
  }

  opts = (0, _tslib.__assign)({
    shouldParseSkeletons: true,
    requiresOtherClause: true
  }, opts);
  var result = new _parser.Parser(message, opts).parse();

  if (result.err) {
    var error = SyntaxError(_error.ErrorKind[result.err.kind]); // @ts-expect-error Assign to error object

    error.location = result.err.location; // @ts-expect-error Assign to error object

    error.originalMessage = result.err.message;
    throw error;
  }

  if (!(opts === null || opts === void 0 ? void 0 : opts.captureLocation)) {
    pruneLocation(result.val);
  }

  return result.val;
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","./error":"../node_modules/@formatjs/icu-messageformat-parser/lib/error.js","./parser":"../node_modules/@formatjs/icu-messageformat-parser/lib/parser.js","./types":"../node_modules/@formatjs/icu-messageformat-parser/lib/types.js"}],"../node_modules/@formatjs/fast-memoize/lib/index.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = memoize;
exports.strategies = void 0;

//
// Main
//
function memoize(fn, options) {
  var cache = options && options.cache ? options.cache : cacheDefault;
  var serializer = options && options.serializer ? options.serializer : serializerDefault;
  var strategy = options && options.strategy ? options.strategy : strategyDefault;
  return strategy(fn, {
    cache: cache,
    serializer: serializer
  });
} //
// Strategy
//


function isPrimitive(value) {
  return value == null || typeof value === 'number' || typeof value === 'boolean'; // || typeof value === "string" 'unsafe' primitive for our needs
}

function monadic(fn, cache, serializer, arg) {
  var cacheKey = isPrimitive(arg) ? arg : serializer(arg);
  var computedValue = cache.get(cacheKey);

  if (typeof computedValue === 'undefined') {
    computedValue = fn.call(this, arg);
    cache.set(cacheKey, computedValue);
  }

  return computedValue;
}

function variadic(fn, cache, serializer) {
  var args = Array.prototype.slice.call(arguments, 3);
  var cacheKey = serializer(args);
  var computedValue = cache.get(cacheKey);

  if (typeof computedValue === 'undefined') {
    computedValue = fn.apply(this, args);
    cache.set(cacheKey, computedValue);
  }

  return computedValue;
}

function assemble(fn, context, strategy, cache, serialize) {
  return strategy.bind(context, fn, cache, serialize);
}

function strategyDefault(fn, options) {
  var strategy = fn.length === 1 ? monadic : variadic;
  return assemble(fn, this, strategy, options.cache.create(), options.serializer);
}

function strategyVariadic(fn, options) {
  return assemble(fn, this, variadic, options.cache.create(), options.serializer);
}

function strategyMonadic(fn, options) {
  return assemble(fn, this, monadic, options.cache.create(), options.serializer);
} //
// Serializer
//


var serializerDefault = function () {
  return JSON.stringify(arguments);
}; //
// Cache
//


function ObjectWithoutPrototypeCache() {
  this.cache = Object.create(null);
}

ObjectWithoutPrototypeCache.prototype.has = function (key) {
  return key in this.cache;
};

ObjectWithoutPrototypeCache.prototype.get = function (key) {
  return this.cache[key];
};

ObjectWithoutPrototypeCache.prototype.set = function (key, value) {
  this.cache[key] = value;
};

var cacheDefault = {
  create: function create() {
    // @ts-ignore
    return new ObjectWithoutPrototypeCache();
  }
};
var strategies = {
  variadic: strategyVariadic,
  monadic: strategyMonadic
};
exports.strategies = strategies;
},{}],"../node_modules/intl-messageformat/lib/src/error.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MissingValueError = exports.InvalidValueTypeError = exports.InvalidValueError = exports.FormatError = exports.ErrorCode = void 0;

var _tslib = require("tslib");

var ErrorCode;
exports.ErrorCode = ErrorCode;

(function (ErrorCode) {
  // When we have a placeholder but no value to format
  ErrorCode["MISSING_VALUE"] = "MISSING_VALUE"; // When value supplied is invalid

  ErrorCode["INVALID_VALUE"] = "INVALID_VALUE"; // When we need specific Intl API but it's not available

  ErrorCode["MISSING_INTL_API"] = "MISSING_INTL_API";
})(ErrorCode || (exports.ErrorCode = ErrorCode = {}));

var FormatError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(FormatError, _super);

  function FormatError(msg, code, originalMessage) {
    var _this = _super.call(this, msg) || this;

    _this.code = code;
    _this.originalMessage = originalMessage;
    return _this;
  }

  FormatError.prototype.toString = function () {
    return "[formatjs Error: " + this.code + "] " + this.message;
  };

  return FormatError;
}(Error);

exports.FormatError = FormatError;

var InvalidValueError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(InvalidValueError, _super);

  function InvalidValueError(variableId, value, options, originalMessage) {
    return _super.call(this, "Invalid values for \"" + variableId + "\": \"" + value + "\". Options are \"" + Object.keys(options).join('", "') + "\"", ErrorCode.INVALID_VALUE, originalMessage) || this;
  }

  return InvalidValueError;
}(FormatError);

exports.InvalidValueError = InvalidValueError;

var InvalidValueTypeError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(InvalidValueTypeError, _super);

  function InvalidValueTypeError(value, type, originalMessage) {
    return _super.call(this, "Value for \"" + value + "\" must be of type " + type, ErrorCode.INVALID_VALUE, originalMessage) || this;
  }

  return InvalidValueTypeError;
}(FormatError);

exports.InvalidValueTypeError = InvalidValueTypeError;

var MissingValueError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(MissingValueError, _super);

  function MissingValueError(variableId, originalMessage) {
    return _super.call(this, "The intl string context variable \"" + variableId + "\" was not provided to the string \"" + originalMessage + "\"", ErrorCode.MISSING_VALUE, originalMessage) || this;
  }

  return MissingValueError;
}(FormatError);

exports.MissingValueError = MissingValueError;
},{"tslib":"../node_modules/tslib/tslib.es6.js"}],"../node_modules/intl-messageformat/lib/src/formatters.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isFormatXMLElementFn = isFormatXMLElementFn;
exports.formatToParts = formatToParts;
exports.PART_TYPE = void 0;

var _icuMessageformatParser = require("@formatjs/icu-messageformat-parser");

var _error = require("./error");

var PART_TYPE;
exports.PART_TYPE = PART_TYPE;

(function (PART_TYPE) {
  PART_TYPE[PART_TYPE["literal"] = 0] = "literal";
  PART_TYPE[PART_TYPE["object"] = 1] = "object";
})(PART_TYPE || (exports.PART_TYPE = PART_TYPE = {}));

function mergeLiteral(parts) {
  if (parts.length < 2) {
    return parts;
  }

  return parts.reduce(function (all, part) {
    var lastPart = all[all.length - 1];

    if (!lastPart || lastPart.type !== PART_TYPE.literal || part.type !== PART_TYPE.literal) {
      all.push(part);
    } else {
      lastPart.value += part.value;
    }

    return all;
  }, []);
}

function isFormatXMLElementFn(el) {
  return typeof el === 'function';
} // TODO(skeleton): add skeleton support


function formatToParts(els, locales, formatters, formats, values, currentPluralValue, // For debugging
originalMessage) {
  // Hot path for straight simple msg translations
  if (els.length === 1 && (0, _icuMessageformatParser.isLiteralElement)(els[0])) {
    return [{
      type: PART_TYPE.literal,
      value: els[0].value
    }];
  }

  var result = [];

  for (var _i = 0, els_1 = els; _i < els_1.length; _i++) {
    var el = els_1[_i]; // Exit early for string parts.

    if ((0, _icuMessageformatParser.isLiteralElement)(el)) {
      result.push({
        type: PART_TYPE.literal,
        value: el.value
      });
      continue;
    } // TODO: should this part be literal type?
    // Replace `#` in plural rules with the actual numeric value.


    if ((0, _icuMessageformatParser.isPoundElement)(el)) {
      if (typeof currentPluralValue === 'number') {
        result.push({
          type: PART_TYPE.literal,
          value: formatters.getNumberFormat(locales).format(currentPluralValue)
        });
      }

      continue;
    }

    var varName = el.value; // Enforce that all required values are provided by the caller.

    if (!(values && varName in values)) {
      throw new _error.MissingValueError(varName, originalMessage);
    }

    var value = values[varName];

    if ((0, _icuMessageformatParser.isArgumentElement)(el)) {
      if (!value || typeof value === 'string' || typeof value === 'number') {
        value = typeof value === 'string' || typeof value === 'number' ? String(value) : '';
      }

      result.push({
        type: typeof value === 'string' ? PART_TYPE.literal : PART_TYPE.object,
        value: value
      });
      continue;
    } // Recursively format plural and select parts' option — which can be a
    // nested pattern structure. The choosing of the option to use is
    // abstracted-by and delegated-to the part helper object.


    if ((0, _icuMessageformatParser.isDateElement)(el)) {
      var style = typeof el.style === 'string' ? formats.date[el.style] : (0, _icuMessageformatParser.isDateTimeSkeleton)(el.style) ? el.style.parsedOptions : undefined;
      result.push({
        type: PART_TYPE.literal,
        value: formatters.getDateTimeFormat(locales, style).format(value)
      });
      continue;
    }

    if ((0, _icuMessageformatParser.isTimeElement)(el)) {
      var style = typeof el.style === 'string' ? formats.time[el.style] : (0, _icuMessageformatParser.isDateTimeSkeleton)(el.style) ? el.style.parsedOptions : undefined;
      result.push({
        type: PART_TYPE.literal,
        value: formatters.getDateTimeFormat(locales, style).format(value)
      });
      continue;
    }

    if ((0, _icuMessageformatParser.isNumberElement)(el)) {
      var style = typeof el.style === 'string' ? formats.number[el.style] : (0, _icuMessageformatParser.isNumberSkeleton)(el.style) ? el.style.parsedOptions : undefined;

      if (style && style.scale) {
        value = value * (style.scale || 1);
      }

      result.push({
        type: PART_TYPE.literal,
        value: formatters.getNumberFormat(locales, style).format(value)
      });
      continue;
    }

    if ((0, _icuMessageformatParser.isTagElement)(el)) {
      var children = el.children,
          value_1 = el.value;
      var formatFn = values[value_1];

      if (!isFormatXMLElementFn(formatFn)) {
        throw new _error.InvalidValueTypeError(value_1, 'function', originalMessage);
      }

      var parts = formatToParts(children, locales, formatters, formats, values, currentPluralValue);
      var chunks = formatFn(parts.map(function (p) {
        return p.value;
      }));

      if (!Array.isArray(chunks)) {
        chunks = [chunks];
      }

      result.push.apply(result, chunks.map(function (c) {
        return {
          type: typeof c === 'string' ? PART_TYPE.literal : PART_TYPE.object,
          value: c
        };
      }));
    }

    if ((0, _icuMessageformatParser.isSelectElement)(el)) {
      var opt = el.options[value] || el.options.other;

      if (!opt) {
        throw new _error.InvalidValueError(el.value, value, Object.keys(el.options), originalMessage);
      }

      result.push.apply(result, formatToParts(opt.value, locales, formatters, formats, values));
      continue;
    }

    if ((0, _icuMessageformatParser.isPluralElement)(el)) {
      var opt = el.options["=" + value];

      if (!opt) {
        if (!Intl.PluralRules) {
          throw new _error.FormatError("Intl.PluralRules is not available in this environment.\nTry polyfilling it using \"@formatjs/intl-pluralrules\"\n", _error.ErrorCode.MISSING_INTL_API, originalMessage);
        }

        var rule = formatters.getPluralRules(locales, {
          type: el.pluralType
        }).select(value - (el.offset || 0));
        opt = el.options[rule] || el.options.other;
      }

      if (!opt) {
        throw new _error.InvalidValueError(el.value, value, Object.keys(el.options), originalMessage);
      }

      result.push.apply(result, formatToParts(opt.value, locales, formatters, formats, values, value - (el.offset || 0)));
      continue;
    }
  }

  return mergeLiteral(result);
}
},{"@formatjs/icu-messageformat-parser":"../node_modules/@formatjs/icu-messageformat-parser/lib/index.js","./error":"../node_modules/intl-messageformat/lib/src/error.js"}],"../node_modules/intl-messageformat/lib/src/core.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IntlMessageFormat = void 0;

var _tslib = require("tslib");

var _icuMessageformatParser = require("@formatjs/icu-messageformat-parser");

var _fastMemoize = _interopRequireWildcard(require("@formatjs/fast-memoize"));

var _formatters = require("./formatters");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

/*
Copyright (c) 2014, Yahoo! Inc. All rights reserved.
Copyrights licensed under the New BSD License.
See the accompanying LICENSE file for terms.
*/
// -- MessageFormat --------------------------------------------------------
function mergeConfig(c1, c2) {
  if (!c2) {
    return c1;
  }

  return (0, _tslib.__assign)((0, _tslib.__assign)((0, _tslib.__assign)({}, c1 || {}), c2 || {}), Object.keys(c1).reduce(function (all, k) {
    all[k] = (0, _tslib.__assign)((0, _tslib.__assign)({}, c1[k]), c2[k] || {});
    return all;
  }, {}));
}

function mergeConfigs(defaultConfig, configs) {
  if (!configs) {
    return defaultConfig;
  }

  return Object.keys(defaultConfig).reduce(function (all, k) {
    all[k] = mergeConfig(defaultConfig[k], configs[k]);
    return all;
  }, (0, _tslib.__assign)({}, defaultConfig));
}

function createFastMemoizeCache(store) {
  return {
    create: function () {
      return {
        has: function (key) {
          return key in store;
        },
        get: function (key) {
          return store[key];
        },
        set: function (key, value) {
          store[key] = value;
        }
      };
    }
  };
}

function createDefaultFormatters(cache) {
  if (cache === void 0) {
    cache = {
      number: {},
      dateTime: {},
      pluralRules: {}
    };
  }

  return {
    getNumberFormat: (0, _fastMemoize.default)(function () {
      var _a;

      var args = [];

      for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }

      return new ((_a = Intl.NumberFormat).bind.apply(_a, (0, _tslib.__spreadArray)([void 0], args)))();
    }, {
      cache: createFastMemoizeCache(cache.number),
      strategy: _fastMemoize.strategies.variadic
    }),
    getDateTimeFormat: (0, _fastMemoize.default)(function () {
      var _a;

      var args = [];

      for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }

      return new ((_a = Intl.DateTimeFormat).bind.apply(_a, (0, _tslib.__spreadArray)([void 0], args)))();
    }, {
      cache: createFastMemoizeCache(cache.dateTime),
      strategy: _fastMemoize.strategies.variadic
    }),
    getPluralRules: (0, _fastMemoize.default)(function () {
      var _a;

      var args = [];

      for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }

      return new ((_a = Intl.PluralRules).bind.apply(_a, (0, _tslib.__spreadArray)([void 0], args)))();
    }, {
      cache: createFastMemoizeCache(cache.pluralRules),
      strategy: _fastMemoize.strategies.variadic
    })
  };
}

var IntlMessageFormat =
/** @class */
function () {
  function IntlMessageFormat(message, locales, overrideFormats, opts) {
    var _this = this;

    if (locales === void 0) {
      locales = IntlMessageFormat.defaultLocale;
    }

    this.formatterCache = {
      number: {},
      dateTime: {},
      pluralRules: {}
    };

    this.format = function (values) {
      var parts = _this.formatToParts(values); // Hot path for straight simple msg translations


      if (parts.length === 1) {
        return parts[0].value;
      }

      var result = parts.reduce(function (all, part) {
        if (!all.length || part.type !== _formatters.PART_TYPE.literal || typeof all[all.length - 1] !== 'string') {
          all.push(part.value);
        } else {
          all[all.length - 1] += part.value;
        }

        return all;
      }, []);

      if (result.length <= 1) {
        return result[0] || '';
      }

      return result;
    };

    this.formatToParts = function (values) {
      return (0, _formatters.formatToParts)(_this.ast, _this.locales, _this.formatters, _this.formats, values, undefined, _this.message);
    };

    this.resolvedOptions = function () {
      return {
        locale: Intl.NumberFormat.supportedLocalesOf(_this.locales)[0]
      };
    };

    this.getAst = function () {
      return _this.ast;
    };

    if (typeof message === 'string') {
      this.message = message;

      if (!IntlMessageFormat.__parse) {
        throw new TypeError('IntlMessageFormat.__parse must be set to process `message` of type `string`');
      } // Parse string messages into an AST.


      this.ast = IntlMessageFormat.__parse(message, {
        ignoreTag: opts === null || opts === void 0 ? void 0 : opts.ignoreTag
      });
    } else {
      this.ast = message;
    }

    if (!Array.isArray(this.ast)) {
      throw new TypeError('A message must be provided as a String or AST.');
    } // Creates a new object with the specified `formats` merged with the default
    // formats.


    this.formats = mergeConfigs(IntlMessageFormat.formats, overrideFormats); // Defined first because it's used to build the format pattern.

    this.locales = locales;
    this.formatters = opts && opts.formatters || createDefaultFormatters(this.formatterCache);
  }

  Object.defineProperty(IntlMessageFormat, "defaultLocale", {
    get: function () {
      if (!IntlMessageFormat.memoizedDefaultLocale) {
        IntlMessageFormat.memoizedDefaultLocale = new Intl.NumberFormat().resolvedOptions().locale;
      }

      return IntlMessageFormat.memoizedDefaultLocale;
    },
    enumerable: false,
    configurable: true
  });
  IntlMessageFormat.memoizedDefaultLocale = null;
  IntlMessageFormat.__parse = _icuMessageformatParser.parse; // Default format options used as the prototype of the `formats` provided to the
  // constructor. These are used when constructing the internal Intl.NumberFormat
  // and Intl.DateTimeFormat instances.

  IntlMessageFormat.formats = {
    number: {
      integer: {
        maximumFractionDigits: 0
      },
      currency: {
        style: 'currency'
      },
      percent: {
        style: 'percent'
      }
    },
    date: {
      short: {
        month: 'numeric',
        day: 'numeric',
        year: '2-digit'
      },
      medium: {
        month: 'short',
        day: 'numeric',
        year: 'numeric'
      },
      long: {
        month: 'long',
        day: 'numeric',
        year: 'numeric'
      },
      full: {
        weekday: 'long',
        month: 'long',
        day: 'numeric',
        year: 'numeric'
      }
    },
    time: {
      short: {
        hour: 'numeric',
        minute: 'numeric'
      },
      medium: {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric'
      },
      long: {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric',
        timeZoneName: 'short'
      },
      full: {
        hour: 'numeric',
        minute: 'numeric',
        second: 'numeric',
        timeZoneName: 'short'
      }
    }
  };
  return IntlMessageFormat;
}();

exports.IntlMessageFormat = IntlMessageFormat;
},{"tslib":"../node_modules/tslib/tslib.es6.js","@formatjs/icu-messageformat-parser":"../node_modules/@formatjs/icu-messageformat-parser/lib/index.js","@formatjs/fast-memoize":"../node_modules/@formatjs/fast-memoize/lib/index.js","./formatters":"../node_modules/intl-messageformat/lib/src/formatters.js"}],"../node_modules/intl-messageformat/lib/index.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {};
exports.default = void 0;

var _core = require("./src/core");

Object.keys(_core).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _core[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _core[key];
    }
  });
});

var _formatters = require("./src/formatters");

Object.keys(_formatters).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _formatters[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _formatters[key];
    }
  });
});

var _error = require("./src/error");

Object.keys(_error).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _error[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _error[key];
    }
  });
});

/*
Copyright (c) 2014, Yahoo! Inc. All rights reserved.
Copyrights licensed under the New BSD License.
See the accompanying LICENSE file for terms.
*/
var _default = _core.IntlMessageFormat;
exports.default = _default;
},{"./src/core":"../node_modules/intl-messageformat/lib/src/core.js","./src/formatters":"../node_modules/intl-messageformat/lib/src/formatters.js","./src/error":"../node_modules/intl-messageformat/lib/src/error.js"}],"../node_modules/@formatjs/intl/lib/src/error.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MissingTranslationError = exports.MessageFormatError = exports.MissingDataError = exports.InvalidConfigError = exports.UnsupportedFormatterError = exports.IntlError = exports.IntlErrorCode = void 0;

var _tslib = require("tslib");

var IntlErrorCode;
exports.IntlErrorCode = IntlErrorCode;

(function (IntlErrorCode) {
  IntlErrorCode["FORMAT_ERROR"] = "FORMAT_ERROR";
  IntlErrorCode["UNSUPPORTED_FORMATTER"] = "UNSUPPORTED_FORMATTER";
  IntlErrorCode["INVALID_CONFIG"] = "INVALID_CONFIG";
  IntlErrorCode["MISSING_DATA"] = "MISSING_DATA";
  IntlErrorCode["MISSING_TRANSLATION"] = "MISSING_TRANSLATION";
})(IntlErrorCode || (exports.IntlErrorCode = IntlErrorCode = {}));

var IntlError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(IntlError, _super);

  function IntlError(code, message, exception) {
    var _this = _super.call(this, "[@formatjs/intl Error " + code + "] " + message + " \n" + (exception ? "\n" + exception.message + "\n" + exception.stack : '')) || this;

    _this.code = code;

    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(_this, IntlError);
    }

    return _this;
  }

  return IntlError;
}(Error);

exports.IntlError = IntlError;

var UnsupportedFormatterError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(UnsupportedFormatterError, _super);

  function UnsupportedFormatterError(message, exception) {
    return _super.call(this, IntlErrorCode.UNSUPPORTED_FORMATTER, message, exception) || this;
  }

  return UnsupportedFormatterError;
}(IntlError);

exports.UnsupportedFormatterError = UnsupportedFormatterError;

var InvalidConfigError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(InvalidConfigError, _super);

  function InvalidConfigError(message, exception) {
    return _super.call(this, IntlErrorCode.INVALID_CONFIG, message, exception) || this;
  }

  return InvalidConfigError;
}(IntlError);

exports.InvalidConfigError = InvalidConfigError;

var MissingDataError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(MissingDataError, _super);

  function MissingDataError(message, exception) {
    return _super.call(this, IntlErrorCode.MISSING_DATA, message, exception) || this;
  }

  return MissingDataError;
}(IntlError);

exports.MissingDataError = MissingDataError;

var MessageFormatError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(MessageFormatError, _super);

  function MessageFormatError(message, locale, descriptor, exception) {
    var _this = _super.call(this, IntlErrorCode.FORMAT_ERROR, message + " \nLocale: " + locale + "\nMessageID: " + (descriptor === null || descriptor === void 0 ? void 0 : descriptor.id) + "\nDefault Message: " + (descriptor === null || descriptor === void 0 ? void 0 : descriptor.defaultMessage) + "\nDescription: " + (descriptor === null || descriptor === void 0 ? void 0 : descriptor.description) + " \n", exception) || this;

    _this.descriptor = descriptor;
    return _this;
  }

  return MessageFormatError;
}(IntlError);

exports.MessageFormatError = MessageFormatError;

var MissingTranslationError =
/** @class */
function (_super) {
  (0, _tslib.__extends)(MissingTranslationError, _super);

  function MissingTranslationError(descriptor, locale) {
    var _this = _super.call(this, IntlErrorCode.MISSING_TRANSLATION, "Missing message: \"" + descriptor.id + "\" for locale \"" + locale + "\", using " + (descriptor.defaultMessage ? 'default message' : 'id') + " as fallback.") || this;

    _this.descriptor = descriptor;
    return _this;
  }

  return MissingTranslationError;
}(IntlError);

exports.MissingTranslationError = MissingTranslationError;
},{"tslib":"../node_modules/tslib/tslib.es6.js"}],"../node_modules/@formatjs/intl/lib/src/utils.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filterProps = filterProps;
exports.createIntlCache = createIntlCache;
exports.createFormatters = createFormatters;
exports.getNamedFormat = getNamedFormat;
exports.DEFAULT_INTL_CONFIG = void 0;

var _tslib = require("tslib");

var _intlMessageformat = require("intl-messageformat");

var _fastMemoize = _interopRequireWildcard(require("@formatjs/fast-memoize"));

var _error = require("./error");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function filterProps(props, whitelist, defaults) {
  if (defaults === void 0) {
    defaults = {};
  }

  return whitelist.reduce(function (filtered, name) {
    if (name in props) {
      filtered[name] = props[name];
    } else if (name in defaults) {
      filtered[name] = defaults[name];
    }

    return filtered;
  }, {});
}

var defaultErrorHandler = function (error) {
  if ("development" !== 'production') {
    console.error(error);
  }
};

var DEFAULT_INTL_CONFIG = {
  formats: {},
  messages: {},
  timeZone: undefined,
  defaultLocale: 'en',
  defaultFormats: {},
  onError: defaultErrorHandler
};
exports.DEFAULT_INTL_CONFIG = DEFAULT_INTL_CONFIG;

function createIntlCache() {
  return {
    dateTime: {},
    number: {},
    message: {},
    relativeTime: {},
    pluralRules: {},
    list: {},
    displayNames: {}
  };
}

function createFastMemoizeCache(store) {
  return {
    create: function () {
      return {
        has: function (key) {
          return key in store;
        },
        get: function (key) {
          return store[key];
        },
        set: function (key, value) {
          store[key] = value;
        }
      };
    }
  };
}
/**
 * Create intl formatters and populate cache
 * @param cache explicit cache to prevent leaking memory
 */


function createFormatters(cache) {
  if (cache === void 0) {
    cache = createIntlCache();
  }

  var RelativeTimeFormat = Intl.RelativeTimeFormat;
  var ListFormat = Intl.ListFormat;
  var DisplayNames = Intl.DisplayNames;
  var getDateTimeFormat = (0, _fastMemoize.default)(function () {
    var _a;

    var args = [];

    for (var _i = 0; _i < arguments.length; _i++) {
      args[_i] = arguments[_i];
    }

    return new ((_a = Intl.DateTimeFormat).bind.apply(_a, (0, _tslib.__spreadArray)([void 0], args)))();
  }, {
    cache: createFastMemoizeCache(cache.dateTime),
    strategy: _fastMemoize.strategies.variadic
  });
  var getNumberFormat = (0, _fastMemoize.default)(function () {
    var _a;

    var args = [];

    for (var _i = 0; _i < arguments.length; _i++) {
      args[_i] = arguments[_i];
    }

    return new ((_a = Intl.NumberFormat).bind.apply(_a, (0, _tslib.__spreadArray)([void 0], args)))();
  }, {
    cache: createFastMemoizeCache(cache.number),
    strategy: _fastMemoize.strategies.variadic
  });
  var getPluralRules = (0, _fastMemoize.default)(function () {
    var _a;

    var args = [];

    for (var _i = 0; _i < arguments.length; _i++) {
      args[_i] = arguments[_i];
    }

    return new ((_a = Intl.PluralRules).bind.apply(_a, (0, _tslib.__spreadArray)([void 0], args)))();
  }, {
    cache: createFastMemoizeCache(cache.pluralRules),
    strategy: _fastMemoize.strategies.variadic
  });
  return {
    getDateTimeFormat: getDateTimeFormat,
    getNumberFormat: getNumberFormat,
    getMessageFormat: (0, _fastMemoize.default)(function (message, locales, overrideFormats, opts) {
      return new _intlMessageformat.IntlMessageFormat(message, locales, overrideFormats, (0, _tslib.__assign)({
        formatters: {
          getNumberFormat: getNumberFormat,
          getDateTimeFormat: getDateTimeFormat,
          getPluralRules: getPluralRules
        }
      }, opts || {}));
    }, {
      cache: createFastMemoizeCache(cache.message),
      strategy: _fastMemoize.strategies.variadic
    }),
    getRelativeTimeFormat: (0, _fastMemoize.default)(function () {
      var args = [];

      for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }

      return new (RelativeTimeFormat.bind.apply(RelativeTimeFormat, (0, _tslib.__spreadArray)([void 0], args)))();
    }, {
      cache: createFastMemoizeCache(cache.relativeTime),
      strategy: _fastMemoize.strategies.variadic
    }),
    getPluralRules: getPluralRules,
    getListFormat: (0, _fastMemoize.default)(function () {
      var args = [];

      for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }

      return new (ListFormat.bind.apply(ListFormat, (0, _tslib.__spreadArray)([void 0], args)))();
    }, {
      cache: createFastMemoizeCache(cache.list),
      strategy: _fastMemoize.strategies.variadic
    }),
    getDisplayNames: (0, _fastMemoize.default)(function () {
      var args = [];

      for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
      }

      return new (DisplayNames.bind.apply(DisplayNames, (0, _tslib.__spreadArray)([void 0], args)))();
    }, {
      cache: createFastMemoizeCache(cache.displayNames),
      strategy: _fastMemoize.strategies.variadic
    })
  };
}

function getNamedFormat(formats, type, name, onError) {
  var formatType = formats && formats[type];
  var format;

  if (formatType) {
    format = formatType[name];
  }

  if (format) {
    return format;
  }

  onError(new _error.UnsupportedFormatterError("No " + type + " format named: " + name));
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","intl-messageformat":"../node_modules/intl-messageformat/lib/index.js","@formatjs/fast-memoize":"../node_modules/@formatjs/fast-memoize/lib/index.js","./error":"../node_modules/@formatjs/intl/lib/src/error.js"}],"../node_modules/@formatjs/intl/lib/src/message.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatMessage = formatMessage;

var _tslib = require("tslib");

var _ecma402Abstract = require("@formatjs/ecma402-abstract");

var _intlMessageformat = require("intl-messageformat");

var _error = require("./error");

var _icuMessageformatParser = require("@formatjs/icu-messageformat-parser");

function setTimeZoneInOptions(opts, timeZone) {
  return Object.keys(opts).reduce(function (all, k) {
    all[k] = (0, _tslib.__assign)({
      timeZone: timeZone
    }, opts[k]);
    return all;
  }, {});
}

function deepMergeOptions(opts1, opts2) {
  var keys = Object.keys((0, _tslib.__assign)((0, _tslib.__assign)({}, opts1), opts2));
  return keys.reduce(function (all, k) {
    all[k] = (0, _tslib.__assign)((0, _tslib.__assign)({}, opts1[k] || {}), opts2[k] || {});
    return all;
  }, {});
}

function deepMergeFormatsAndSetTimeZone(f1, timeZone) {
  if (!timeZone) {
    return f1;
  }

  var mfFormats = _intlMessageformat.IntlMessageFormat.formats;
  return (0, _tslib.__assign)((0, _tslib.__assign)((0, _tslib.__assign)({}, mfFormats), f1), {
    date: deepMergeOptions(setTimeZoneInOptions(mfFormats.date, timeZone), setTimeZoneInOptions(f1.date || {}, timeZone)),
    time: deepMergeOptions(setTimeZoneInOptions(mfFormats.time, timeZone), setTimeZoneInOptions(f1.time || {}, timeZone))
  });
}

function formatMessage(_a, state, messageDescriptor, values, opts) {
  var locale = _a.locale,
      formats = _a.formats,
      messages = _a.messages,
      defaultLocale = _a.defaultLocale,
      defaultFormats = _a.defaultFormats,
      onError = _a.onError,
      timeZone = _a.timeZone,
      defaultRichTextElements = _a.defaultRichTextElements;

  if (messageDescriptor === void 0) {
    messageDescriptor = {
      id: ''
    };
  }

  var msgId = messageDescriptor.id,
      defaultMessage = messageDescriptor.defaultMessage; // `id` is a required field of a Message Descriptor.

  (0, _ecma402Abstract.invariant)(!!msgId, "[@formatjs/intl] An `id` must be provided to format a message. You can either:\n1. Configure your build toolchain with [babel-plugin-formatjs](https://formatjs.io/docs/tooling/babel-plugin)\nor [@formatjs/ts-transformer](https://formatjs.io/docs/tooling/ts-transformer) OR\n2. Configure your `eslint` config to include [eslint-plugin-formatjs](https://formatjs.io/docs/tooling/linter#enforce-id)\nto autofix this issue");
  var id = String(msgId);
  var message = // In case messages is Object.create(null)
  // e.g import('foo.json') from webpack)
  // See https://github.com/formatjs/formatjs/issues/1914
  messages && Object.prototype.hasOwnProperty.call(messages, id) && messages[id]; // IMPORTANT: Hot path if `message` is AST with a single literal node

  if (Array.isArray(message) && message.length === 1 && message[0].type === _icuMessageformatParser.TYPE.literal) {
    return message[0].value;
  } // IMPORTANT: Hot path straight lookup for performance


  if (!values && message && typeof message === 'string' && !defaultRichTextElements) {
    return message.replace(/'\{(.*?)\}'/gi, "{$1}");
  }

  values = (0, _tslib.__assign)((0, _tslib.__assign)({}, defaultRichTextElements), values || {});
  formats = deepMergeFormatsAndSetTimeZone(formats, timeZone);
  defaultFormats = deepMergeFormatsAndSetTimeZone(defaultFormats, timeZone);

  if (!message) {
    if (!defaultMessage || locale && locale.toLowerCase() !== defaultLocale.toLowerCase()) {
      // This prevents warnings from littering the console in development
      // when no `messages` are passed into the <IntlProvider> for the
      // default locale.
      onError(new _error.MissingTranslationError(messageDescriptor, locale));
    }

    if (defaultMessage) {
      try {
        var formatter = state.getMessageFormat(defaultMessage, defaultLocale, defaultFormats, opts);
        return formatter.format(values);
      } catch (e) {
        onError(new _error.MessageFormatError("Error formatting default message for: \"" + id + "\", rendering default message verbatim", locale, messageDescriptor, e));
        return typeof defaultMessage === 'string' ? defaultMessage : id;
      }
    }

    return id;
  } // We have the translated message


  try {
    var formatter = state.getMessageFormat(message, locale, formats, (0, _tslib.__assign)({
      formatters: state
    }, opts || {}));
    return formatter.format(values);
  } catch (e) {
    onError(new _error.MessageFormatError("Error formatting message: \"" + id + "\", using " + (defaultMessage ? 'default message' : 'id') + " as fallback.", locale, messageDescriptor, e));
  }

  if (defaultMessage) {
    try {
      var formatter = state.getMessageFormat(defaultMessage, defaultLocale, defaultFormats, opts);
      return formatter.format(values);
    } catch (e) {
      onError(new _error.MessageFormatError("Error formatting the default message for: \"" + id + "\", rendering message verbatim", locale, messageDescriptor, e));
    }
  }

  if (typeof message === 'string') {
    return message;
  }

  if (typeof defaultMessage === 'string') {
    return defaultMessage;
  }

  return id;
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","@formatjs/ecma402-abstract":"../node_modules/@formatjs/ecma402-abstract/lib/index.js","intl-messageformat":"../node_modules/intl-messageformat/lib/index.js","./error":"../node_modules/@formatjs/intl/lib/src/error.js","@formatjs/icu-messageformat-parser":"../node_modules/@formatjs/icu-messageformat-parser/lib/index.js"}],"../node_modules/@formatjs/intl/lib/src/dateTime.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFormatter = getFormatter;
exports.formatDate = formatDate;
exports.formatTime = formatTime;
exports.formatDateTimeRange = formatDateTimeRange;
exports.formatDateToParts = formatDateToParts;
exports.formatTimeToParts = formatTimeToParts;

var _tslib = require("tslib");

var _utils = require("./utils");

var _error = require("./error");

var DATE_TIME_FORMAT_OPTIONS = ['localeMatcher', 'formatMatcher', 'timeZone', 'hour12', 'weekday', 'era', 'year', 'month', 'day', 'hour', 'minute', 'second', 'timeZoneName', 'hourCycle', 'dateStyle', 'timeStyle', 'calendar', // 'dayPeriod',
'numberingSystem'];

function getFormatter(_a, type, getDateTimeFormat, options) {
  var locale = _a.locale,
      formats = _a.formats,
      onError = _a.onError,
      timeZone = _a.timeZone;

  if (options === void 0) {
    options = {};
  }

  var format = options.format;
  var defaults = (0, _tslib.__assign)((0, _tslib.__assign)({}, timeZone && {
    timeZone: timeZone
  }), format && (0, _utils.getNamedFormat)(formats, type, format, onError));
  var filteredOptions = (0, _utils.filterProps)(options, DATE_TIME_FORMAT_OPTIONS, // @ts-expect-error es2020 has a lot stuff from es2021 bleed in
  defaults);

  if (type === 'time' && !filteredOptions.hour && !filteredOptions.minute && !filteredOptions.second && !filteredOptions.timeStyle && !filteredOptions.dateStyle) {
    // Add default formatting options if hour, minute, or second isn't defined.
    filteredOptions = (0, _tslib.__assign)((0, _tslib.__assign)({}, filteredOptions), {
      hour: 'numeric',
      minute: 'numeric'
    });
  }

  return getDateTimeFormat(locale, filteredOptions);
}

function formatDate(config, getDateTimeFormat) {
  var _a = [];

  for (var _i = 2; _i < arguments.length; _i++) {
    _a[_i - 2] = arguments[_i];
  }

  var value = _a[0],
      _b = _a[1],
      options = _b === void 0 ? {} : _b;
  var date = typeof value === 'string' ? new Date(value || 0) : value;

  try {
    return getFormatter(config, 'date', getDateTimeFormat, options).format(date);
  } catch (e) {
    config.onError(new _error.IntlError(_error.IntlErrorCode.FORMAT_ERROR, 'Error formatting date.', e));
  }

  return String(date);
}

function formatTime(config, getDateTimeFormat) {
  var _a = [];

  for (var _i = 2; _i < arguments.length; _i++) {
    _a[_i - 2] = arguments[_i];
  }

  var value = _a[0],
      _b = _a[1],
      options = _b === void 0 ? {} : _b;
  var date = typeof value === 'string' ? new Date(value || 0) : value;

  try {
    return getFormatter(config, 'time', getDateTimeFormat, options).format(date);
  } catch (e) {
    config.onError(new _error.IntlError(_error.IntlErrorCode.FORMAT_ERROR, 'Error formatting time.', e));
  }

  return String(date);
}

function formatDateTimeRange(config, getDateTimeFormat) {
  var _a = [];

  for (var _i = 2; _i < arguments.length; _i++) {
    _a[_i - 2] = arguments[_i];
  }

  var from = _a[0],
      to = _a[1],
      _b = _a[2],
      options = _b === void 0 ? {} : _b;
  var timeZone = config.timeZone,
      locale = config.locale,
      onError = config.onError;
  var filteredOptions = (0, _utils.filterProps)(options, DATE_TIME_FORMAT_OPTIONS, timeZone ? {
    timeZone: timeZone
  } : {});

  try {
    return getDateTimeFormat(locale, filteredOptions).formatRange(from, to);
  } catch (e) {
    onError(new _error.IntlError(_error.IntlErrorCode.FORMAT_ERROR, 'Error formatting date time range.', e));
  }

  return String(from);
}

function formatDateToParts(config, getDateTimeFormat) {
  var _a = [];

  for (var _i = 2; _i < arguments.length; _i++) {
    _a[_i - 2] = arguments[_i];
  }

  var value = _a[0],
      _b = _a[1],
      options = _b === void 0 ? {} : _b;
  var date = typeof value === 'string' ? new Date(value || 0) : value;

  try {
    return getFormatter(config, 'date', getDateTimeFormat, options).formatToParts(date);
  } catch (e) {
    config.onError(new _error.IntlError(_error.IntlErrorCode.FORMAT_ERROR, 'Error formatting date.', e));
  }

  return [];
}

function formatTimeToParts(config, getDateTimeFormat) {
  var _a = [];

  for (var _i = 2; _i < arguments.length; _i++) {
    _a[_i - 2] = arguments[_i];
  }

  var value = _a[0],
      _b = _a[1],
      options = _b === void 0 ? {} : _b;
  var date = typeof value === 'string' ? new Date(value || 0) : value;

  try {
    return getFormatter(config, 'time', getDateTimeFormat, options).formatToParts(date);
  } catch (e) {
    config.onError(new _error.IntlError(_error.IntlErrorCode.FORMAT_ERROR, 'Error formatting time.', e));
  }

  return [];
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","./utils":"../node_modules/@formatjs/intl/lib/src/utils.js","./error":"../node_modules/@formatjs/intl/lib/src/error.js"}],"../node_modules/@formatjs/intl/lib/src/displayName.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatDisplayName = formatDisplayName;

var _utils = require("./utils");

var _intlMessageformat = require("intl-messageformat");

var _error = require("./error");

var DISPLAY_NAMES_OPTONS = ['localeMatcher', 'style', 'type', 'fallback'];

function formatDisplayName(_a, getDisplayNames, value, options) {
  var locale = _a.locale,
      onError = _a.onError;
  var DisplayNames = Intl.DisplayNames;

  if (!DisplayNames) {
    onError(new _intlMessageformat.FormatError("Intl.DisplayNames is not available in this environment.\nTry polyfilling it using \"@formatjs/intl-displaynames\"\n", _intlMessageformat.ErrorCode.MISSING_INTL_API));
  }

  var filteredOptions = (0, _utils.filterProps)(options, DISPLAY_NAMES_OPTONS);

  try {
    return getDisplayNames(locale, filteredOptions).of(value);
  } catch (e) {
    onError(new _error.IntlError(_error.IntlErrorCode.FORMAT_ERROR, 'Error formatting display name.', e));
  }
}
},{"./utils":"../node_modules/@formatjs/intl/lib/src/utils.js","intl-messageformat":"../node_modules/intl-messageformat/lib/index.js","./error":"../node_modules/@formatjs/intl/lib/src/error.js"}],"../node_modules/@formatjs/intl/lib/src/list.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatList = formatList;
exports.formatListToParts = formatListToParts;

var _tslib = require("tslib");

var _utils = require("./utils");

var _intlMessageformat = require("intl-messageformat");

var _error = require("./error");

var LIST_FORMAT_OPTIONS = ['localeMatcher', 'type', 'style'];
var now = Date.now();

function generateToken(i) {
  return now + "_" + i + "_" + now;
}

function formatList(opts, getListFormat, values, options) {
  if (options === void 0) {
    options = {};
  }

  var results = formatListToParts(opts, getListFormat, values, options).reduce(function (all, el) {
    var val = el.value;

    if (typeof val !== 'string') {
      all.push(val);
    } else if (typeof all[all.length - 1] === 'string') {
      all[all.length - 1] += val;
    } else {
      all.push(val);
    }

    return all;
  }, []);
  return results.length === 1 ? results[0] : results;
}

function formatListToParts(_a, getListFormat, values, options) {
  var locale = _a.locale,
      onError = _a.onError;

  if (options === void 0) {
    options = {};
  }

  var ListFormat = Intl.ListFormat;

  if (!ListFormat) {
    onError(new _intlMessageformat.FormatError("Intl.ListFormat is not available in this environment.\nTry polyfilling it using \"@formatjs/intl-listformat\"\n", _intlMessageformat.ErrorCode.MISSING_INTL_API));
  }

  var filteredOptions = (0, _utils.filterProps)(options, LIST_FORMAT_OPTIONS);

  try {
    var richValues_1 = {};
    var serializedValues = values.map(function (v, i) {
      if (typeof v === 'object') {
        var id = generateToken(i);
        richValues_1[id] = v;
        return id;
      }

      return String(v);
    });
    return getListFormat(locale, filteredOptions).formatToParts(serializedValues).map(function (part) {
      return part.type === 'literal' ? part : (0, _tslib.__assign)((0, _tslib.__assign)({}, part), {
        value: richValues_1[part.value] || part.value
      });
    });
  } catch (e) {
    onError(new _error.IntlError(_error.IntlErrorCode.FORMAT_ERROR, 'Error formatting list.', e));
  } // @ts-ignore


  return values;
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","./utils":"../node_modules/@formatjs/intl/lib/src/utils.js","intl-messageformat":"../node_modules/intl-messageformat/lib/index.js","./error":"../node_modules/@formatjs/intl/lib/src/error.js"}],"../node_modules/@formatjs/intl/lib/src/plural.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatPlural = formatPlural;

var _utils = require("./utils");

var _error = require("./error");

var _intlMessageformat = require("intl-messageformat");

var PLURAL_FORMAT_OPTIONS = ['localeMatcher', 'type'];

function formatPlural(_a, getPluralRules, value, options) {
  var locale = _a.locale,
      onError = _a.onError;

  if (options === void 0) {
    options = {};
  }

  if (!Intl.PluralRules) {
    onError(new _intlMessageformat.FormatError("Intl.PluralRules is not available in this environment.\nTry polyfilling it using \"@formatjs/intl-pluralrules\"\n", _intlMessageformat.ErrorCode.MISSING_INTL_API));
  }

  var filteredOptions = (0, _utils.filterProps)(options, PLURAL_FORMAT_OPTIONS);

  try {
    return getPluralRules(locale, filteredOptions).select(value);
  } catch (e) {
    onError(new _error.MessageFormatError('Error formatting plural.', e));
  }

  return 'other';
}
},{"./utils":"../node_modules/@formatjs/intl/lib/src/utils.js","./error":"../node_modules/@formatjs/intl/lib/src/error.js","intl-messageformat":"../node_modules/intl-messageformat/lib/index.js"}],"../node_modules/@formatjs/intl/lib/src/relativeTime.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatRelativeTime = formatRelativeTime;

var _utils = require("./utils");

var _intlMessageformat = require("intl-messageformat");

var _error = require("./error");

var RELATIVE_TIME_FORMAT_OPTIONS = ['numeric', 'style'];

function getFormatter(_a, getRelativeTimeFormat, options) {
  var locale = _a.locale,
      formats = _a.formats,
      onError = _a.onError;

  if (options === void 0) {
    options = {};
  }

  var format = options.format;
  var defaults = !!format && (0, _utils.getNamedFormat)(formats, 'relative', format, onError) || {};
  var filteredOptions = (0, _utils.filterProps)(options, RELATIVE_TIME_FORMAT_OPTIONS, defaults);
  return getRelativeTimeFormat(locale, filteredOptions);
}

function formatRelativeTime(config, getRelativeTimeFormat, value, unit, options) {
  if (options === void 0) {
    options = {};
  }

  if (!unit) {
    unit = 'second';
  }

  var RelativeTimeFormat = Intl.RelativeTimeFormat;

  if (!RelativeTimeFormat) {
    config.onError(new _intlMessageformat.FormatError("Intl.RelativeTimeFormat is not available in this environment.\nTry polyfilling it using \"@formatjs/intl-relativetimeformat\"\n", _intlMessageformat.ErrorCode.MISSING_INTL_API));
  }

  try {
    return getFormatter(config, getRelativeTimeFormat, options).format(value, unit);
  } catch (e) {
    config.onError(new _error.MessageFormatError('Error formatting relative time.', e));
  }

  return String(value);
}
},{"./utils":"../node_modules/@formatjs/intl/lib/src/utils.js","intl-messageformat":"../node_modules/intl-messageformat/lib/index.js","./error":"../node_modules/@formatjs/intl/lib/src/error.js"}],"../node_modules/@formatjs/intl/lib/src/number.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFormatter = getFormatter;
exports.formatNumber = formatNumber;
exports.formatNumberToParts = formatNumberToParts;

var _utils = require("./utils");

var _error = require("./error");

var NUMBER_FORMAT_OPTIONS = ['localeMatcher', 'style', 'currency', 'currencyDisplay', 'unit', 'unitDisplay', 'useGrouping', 'minimumIntegerDigits', 'minimumFractionDigits', 'maximumFractionDigits', 'minimumSignificantDigits', 'maximumSignificantDigits', // ES2020 NumberFormat
'compactDisplay', 'currencyDisplay', 'currencySign', 'notation', 'signDisplay', 'unit', 'unitDisplay', 'numberingSystem'];

function getFormatter(_a, getNumberFormat, options) {
  var locale = _a.locale,
      formats = _a.formats,
      onError = _a.onError;

  if (options === void 0) {
    options = {};
  }

  var format = options.format;
  var defaults = format && (0, _utils.getNamedFormat)(formats, 'number', format, onError) || {};
  var filteredOptions = (0, _utils.filterProps)(options, NUMBER_FORMAT_OPTIONS, defaults);
  return getNumberFormat(locale, filteredOptions);
}

function formatNumber(config, getNumberFormat, value, options) {
  if (options === void 0) {
    options = {};
  }

  try {
    return getFormatter(config, getNumberFormat, options).format(value);
  } catch (e) {
    config.onError(new _error.IntlError(_error.IntlErrorCode.FORMAT_ERROR, 'Error formatting number.', e));
  }

  return String(value);
}

function formatNumberToParts(config, getNumberFormat, value, options) {
  if (options === void 0) {
    options = {};
  }

  try {
    return getFormatter(config, getNumberFormat, options).formatToParts(value);
  } catch (e) {
    config.onError(new _error.IntlError(_error.IntlErrorCode.FORMAT_ERROR, 'Error formatting number.', e));
  }

  return [];
}
},{"./utils":"../node_modules/@formatjs/intl/lib/src/utils.js","./error":"../node_modules/@formatjs/intl/lib/src/error.js"}],"../node_modules/@formatjs/intl/lib/src/create-intl.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createIntl = createIntl;

var _tslib = require("tslib");

var _utils = require("./utils");

var _error = require("./error");

var _number = require("./number");

var _relativeTime = require("./relativeTime");

var _dateTime = require("./dateTime");

var _plural = require("./plural");

var _message = require("./message");

var _list = require("./list");

var _displayName = require("./displayName");

function messagesContainString(messages) {
  var firstMessage = messages ? messages[Object.keys(messages)[0]] : undefined;
  return typeof firstMessage === 'string';
}

function verifyConfigMessages(config) {
  if (config.defaultRichTextElements && messagesContainString(config.messages || {})) {
    console.warn("[@formatjs/intl] \"defaultRichTextElements\" was specified but \"message\" was not pre-compiled. \nPlease consider using \"@formatjs/cli\" to pre-compile your messages for performance.\nFor more details see https://formatjs.io/docs/getting-started/message-distribution");
  }
}
/**
 * Create intl object
 * @param config intl config
 * @param cache cache for formatter instances to prevent memory leak
 */


function createIntl(config, cache) {
  var formatters = (0, _utils.createFormatters)(cache);
  var resolvedConfig = (0, _tslib.__assign)((0, _tslib.__assign)({}, _utils.DEFAULT_INTL_CONFIG), config);
  var locale = resolvedConfig.locale,
      defaultLocale = resolvedConfig.defaultLocale,
      onError = resolvedConfig.onError;

  if (!locale) {
    if (onError) {
      onError(new _error.InvalidConfigError("\"locale\" was not configured, using \"" + defaultLocale + "\" as fallback. See https://formatjs.io/docs/react-intl/api#intlshape for more details"));
    } // Since there's no registered locale data for `locale`, this will
    // fallback to the `defaultLocale` to make sure things can render.
    // The `messages` are overridden to the `defaultProps` empty object
    // to maintain referential equality across re-renders. It's assumed
    // each <FormattedMessage> contains a `defaultMessage` prop.


    resolvedConfig.locale = resolvedConfig.defaultLocale || 'en';
  } else if (!Intl.NumberFormat.supportedLocalesOf(locale).length && onError) {
    onError(new _error.MissingDataError("Missing locale data for locale: \"" + locale + "\" in Intl.NumberFormat. Using default locale: \"" + defaultLocale + "\" as fallback. See https://formatjs.io/docs/react-intl#runtime-requirements for more details"));
  } else if (!Intl.DateTimeFormat.supportedLocalesOf(locale).length && onError) {
    onError(new _error.MissingDataError("Missing locale data for locale: \"" + locale + "\" in Intl.DateTimeFormat. Using default locale: \"" + defaultLocale + "\" as fallback. See https://formatjs.io/docs/react-intl#runtime-requirements for more details"));
  }

  verifyConfigMessages(resolvedConfig);
  return (0, _tslib.__assign)((0, _tslib.__assign)({}, resolvedConfig), {
    formatters: formatters,
    formatNumber: _number.formatNumber.bind(null, resolvedConfig, formatters.getNumberFormat),
    formatNumberToParts: _number.formatNumberToParts.bind(null, resolvedConfig, formatters.getNumberFormat),
    formatRelativeTime: _relativeTime.formatRelativeTime.bind(null, resolvedConfig, formatters.getRelativeTimeFormat),
    formatDate: _dateTime.formatDate.bind(null, resolvedConfig, formatters.getDateTimeFormat),
    formatDateToParts: _dateTime.formatDateToParts.bind(null, resolvedConfig, formatters.getDateTimeFormat),
    formatTime: _dateTime.formatTime.bind(null, resolvedConfig, formatters.getDateTimeFormat),
    formatDateTimeRange: _dateTime.formatDateTimeRange.bind(null, resolvedConfig, formatters.getDateTimeFormat),
    formatTimeToParts: _dateTime.formatTimeToParts.bind(null, resolvedConfig, formatters.getDateTimeFormat),
    formatPlural: _plural.formatPlural.bind(null, resolvedConfig, formatters.getPluralRules),
    formatMessage: _message.formatMessage.bind(null, resolvedConfig, formatters),
    formatList: _list.formatList.bind(null, resolvedConfig, formatters.getListFormat),
    formatListToParts: _list.formatListToParts.bind(null, resolvedConfig, formatters.getListFormat),
    formatDisplayName: _displayName.formatDisplayName.bind(null, resolvedConfig, formatters.getDisplayNames)
  });
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","./utils":"../node_modules/@formatjs/intl/lib/src/utils.js","./error":"../node_modules/@formatjs/intl/lib/src/error.js","./number":"../node_modules/@formatjs/intl/lib/src/number.js","./relativeTime":"../node_modules/@formatjs/intl/lib/src/relativeTime.js","./dateTime":"../node_modules/@formatjs/intl/lib/src/dateTime.js","./plural":"../node_modules/@formatjs/intl/lib/src/plural.js","./message":"../node_modules/@formatjs/intl/lib/src/message.js","./list":"../node_modules/@formatjs/intl/lib/src/list.js","./displayName":"../node_modules/@formatjs/intl/lib/src/displayName.js"}],"../node_modules/@formatjs/intl/lib/index.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  defineMessages: true,
  defineMessage: true,
  createIntlCache: true,
  filterProps: true,
  DEFAULT_INTL_CONFIG: true,
  createFormatters: true,
  getNamedFormat: true,
  formatMessage: true,
  formatDate: true,
  formatDateToParts: true,
  formatTime: true,
  formatTimeToParts: true,
  formatDisplayName: true,
  formatList: true,
  formatPlural: true,
  formatRelativeTime: true,
  formatNumber: true,
  formatNumberToParts: true,
  createIntl: true
};
exports.defineMessages = defineMessages;
exports.defineMessage = defineMessage;
Object.defineProperty(exports, "createIntlCache", {
  enumerable: true,
  get: function () {
    return _utils.createIntlCache;
  }
});
Object.defineProperty(exports, "filterProps", {
  enumerable: true,
  get: function () {
    return _utils.filterProps;
  }
});
Object.defineProperty(exports, "DEFAULT_INTL_CONFIG", {
  enumerable: true,
  get: function () {
    return _utils.DEFAULT_INTL_CONFIG;
  }
});
Object.defineProperty(exports, "createFormatters", {
  enumerable: true,
  get: function () {
    return _utils.createFormatters;
  }
});
Object.defineProperty(exports, "getNamedFormat", {
  enumerable: true,
  get: function () {
    return _utils.getNamedFormat;
  }
});
Object.defineProperty(exports, "formatMessage", {
  enumerable: true,
  get: function () {
    return _message.formatMessage;
  }
});
Object.defineProperty(exports, "formatDate", {
  enumerable: true,
  get: function () {
    return _dateTime.formatDate;
  }
});
Object.defineProperty(exports, "formatDateToParts", {
  enumerable: true,
  get: function () {
    return _dateTime.formatDateToParts;
  }
});
Object.defineProperty(exports, "formatTime", {
  enumerable: true,
  get: function () {
    return _dateTime.formatTime;
  }
});
Object.defineProperty(exports, "formatTimeToParts", {
  enumerable: true,
  get: function () {
    return _dateTime.formatTimeToParts;
  }
});
Object.defineProperty(exports, "formatDisplayName", {
  enumerable: true,
  get: function () {
    return _displayName.formatDisplayName;
  }
});
Object.defineProperty(exports, "formatList", {
  enumerable: true,
  get: function () {
    return _list.formatList;
  }
});
Object.defineProperty(exports, "formatPlural", {
  enumerable: true,
  get: function () {
    return _plural.formatPlural;
  }
});
Object.defineProperty(exports, "formatRelativeTime", {
  enumerable: true,
  get: function () {
    return _relativeTime.formatRelativeTime;
  }
});
Object.defineProperty(exports, "formatNumber", {
  enumerable: true,
  get: function () {
    return _number.formatNumber;
  }
});
Object.defineProperty(exports, "formatNumberToParts", {
  enumerable: true,
  get: function () {
    return _number.formatNumberToParts;
  }
});
Object.defineProperty(exports, "createIntl", {
  enumerable: true,
  get: function () {
    return _createIntl.createIntl;
  }
});

var _types = require("./src/types");

Object.keys(_types).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _types[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _types[key];
    }
  });
});

var _utils = require("./src/utils");

var _error = require("./src/error");

Object.keys(_error).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _error[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _error[key];
    }
  });
});

var _message = require("./src/message");

var _dateTime = require("./src/dateTime");

var _displayName = require("./src/displayName");

var _list = require("./src/list");

var _plural = require("./src/plural");

var _relativeTime = require("./src/relativeTime");

var _number = require("./src/number");

var _createIntl = require("./src/create-intl");

function defineMessages(msgs) {
  return msgs;
}

function defineMessage(msg) {
  return msg;
}
},{"./src/types":"../node_modules/@formatjs/intl/lib/src/types.js","./src/utils":"../node_modules/@formatjs/intl/lib/src/utils.js","./src/error":"../node_modules/@formatjs/intl/lib/src/error.js","./src/message":"../node_modules/@formatjs/intl/lib/src/message.js","./src/dateTime":"../node_modules/@formatjs/intl/lib/src/dateTime.js","./src/displayName":"../node_modules/@formatjs/intl/lib/src/displayName.js","./src/list":"../node_modules/@formatjs/intl/lib/src/list.js","./src/plural":"../node_modules/@formatjs/intl/lib/src/plural.js","./src/relativeTime":"../node_modules/@formatjs/intl/lib/src/relativeTime.js","./src/number":"../node_modules/@formatjs/intl/lib/src/number.js","./src/create-intl":"../node_modules/@formatjs/intl/lib/src/create-intl.js"}],"../node_modules/react-intl/lib/src/utils.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.invariantIntlContext = invariantIntlContext;
exports.assignUniqueKeysToParts = assignUniqueKeysToParts;
exports.shallowEqual = shallowEqual;
exports.DEFAULT_INTL_CONFIG = void 0;

var _tslib = require("tslib");

var React = _interopRequireWildcard(require("react"));

var _ecma402Abstract = require("@formatjs/ecma402-abstract");

var _intl = require("@formatjs/intl");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function invariantIntlContext(intl) {
  (0, _ecma402Abstract.invariant)(intl, '[React Intl] Could not find required `intl` object. ' + '<IntlProvider> needs to exist in the component ancestry.');
}

var DEFAULT_INTL_CONFIG = (0, _tslib.__assign)((0, _tslib.__assign)({}, _intl.DEFAULT_INTL_CONFIG), {
  textComponent: React.Fragment
});
/**
 * Takes a `formatXMLElementFn`, and composes it in function, which passes
 * argument `parts` through, assigning unique key to each part, to prevent
 * "Each child in a list should have a unique "key"" React error.
 * @param formatXMLElementFn
 */

exports.DEFAULT_INTL_CONFIG = DEFAULT_INTL_CONFIG;

function assignUniqueKeysToParts(formatXMLElementFn) {
  return function (parts) {
    // eslint-disable-next-line prefer-rest-params
    return formatXMLElementFn(React.Children.toArray(parts));
  };
}

function shallowEqual(objA, objB) {
  if (objA === objB) {
    return true;
  }

  if (!objA || !objB) {
    return false;
  }

  var aKeys = Object.keys(objA);
  var bKeys = Object.keys(objB);
  var len = aKeys.length;

  if (bKeys.length !== len) {
    return false;
  }

  for (var i = 0; i < len; i++) {
    var key = aKeys[i];

    if (objA[key] !== objB[key] || !Object.prototype.hasOwnProperty.call(objB, key)) {
      return false;
    }
  }

  return true;
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","react":"../node_modules/react/index.js","@formatjs/ecma402-abstract":"../node_modules/@formatjs/ecma402-abstract/lib/index.js","@formatjs/intl":"../node_modules/@formatjs/intl/lib/index.js"}],"../node_modules/react-intl/lib/src/components/injectIntl.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = injectIntl;
exports.Context = exports.Provider = void 0;

var _tslib = require("tslib");

var React = _interopRequireWildcard(require("react"));

var _hoistNonReactStatics = _interopRequireDefault(require("hoist-non-react-statics"));

var _utils = require("../utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
} // TODO: We should provide initial value here


var IntlContext = React.createContext(null);
var IntlConsumer = IntlContext.Consumer,
    IntlProvider = IntlContext.Provider;
var Provider = IntlProvider;
exports.Provider = Provider;
var Context = IntlContext;
exports.Context = Context;

function injectIntl(WrappedComponent, options) {
  var _a = options || {},
      _b = _a.intlPropName,
      intlPropName = _b === void 0 ? 'intl' : _b,
      _c = _a.forwardRef,
      forwardRef = _c === void 0 ? false : _c,
      _d = _a.enforceContext,
      enforceContext = _d === void 0 ? true : _d;

  var WithIntl = function (props) {
    return React.createElement(IntlConsumer, null, function (intl) {
      var _a;

      if (enforceContext) {
        (0, _utils.invariantIntlContext)(intl);
      }

      var intlProp = (_a = {}, _a[intlPropName] = intl, _a);
      return React.createElement(WrappedComponent, (0, _tslib.__assign)({}, props, intlProp, {
        ref: forwardRef ? props.forwardedRef : null
      }));
    });
  };

  WithIntl.displayName = "injectIntl(" + getDisplayName(WrappedComponent) + ")";
  WithIntl.WrappedComponent = WrappedComponent;

  if (forwardRef) {
    return (0, _hoistNonReactStatics.default)(React.forwardRef(function (props, ref) {
      return React.createElement(WithIntl, (0, _tslib.__assign)({}, props, {
        forwardedRef: ref
      }));
    }), WrappedComponent);
  }

  return (0, _hoistNonReactStatics.default)(WithIntl, WrappedComponent);
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","react":"../node_modules/react/index.js","hoist-non-react-statics":"../node_modules/hoist-non-react-statics/dist/hoist-non-react-statics.cjs.js","../utils":"../node_modules/react-intl/lib/src/utils.js"}],"../node_modules/react-intl/lib/src/components/useIntl.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = useIntl;

var React = _interopRequireWildcard(require("react"));

var _injectIntl = require("./injectIntl");

var _utils = require("../utils");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function useIntl() {
  var intl = React.useContext(_injectIntl.Context);
  (0, _utils.invariantIntlContext)(intl);
  return intl;
}
},{"react":"../node_modules/react/index.js","./injectIntl":"../node_modules/react-intl/lib/src/components/injectIntl.js","../utils":"../node_modules/react-intl/lib/src/utils.js"}],"../node_modules/react-intl/lib/src/components/createFormattedComponent.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createFormattedDateTimePartsComponent = createFormattedDateTimePartsComponent;
exports.createFormattedComponent = createFormattedComponent;
exports.FormattedListParts = exports.FormattedNumberParts = void 0;

var _tslib = require("tslib");

var React = _interopRequireWildcard(require("react"));

var _useIntl = _interopRequireDefault(require("./useIntl"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var DisplayName;

(function (DisplayName) {
  DisplayName["formatDate"] = "FormattedDate";
  DisplayName["formatTime"] = "FormattedTime";
  DisplayName["formatNumber"] = "FormattedNumber";
  DisplayName["formatList"] = "FormattedList"; // Note that this DisplayName is the locale display name, not to be confused with
  // the name of the enum, which is for React component display name in dev tools.

  DisplayName["formatDisplayName"] = "FormattedDisplayName";
})(DisplayName || (DisplayName = {}));

var DisplayNameParts;

(function (DisplayNameParts) {
  DisplayNameParts["formatDate"] = "FormattedDateParts";
  DisplayNameParts["formatTime"] = "FormattedTimeParts";
  DisplayNameParts["formatNumber"] = "FormattedNumberParts";
  DisplayNameParts["formatList"] = "FormattedListParts";
})(DisplayNameParts || (DisplayNameParts = {}));

var FormattedNumberParts = function (props) {
  var intl = (0, _useIntl.default)();
  var value = props.value,
      children = props.children,
      formatProps = (0, _tslib.__rest)(props, ["value", "children"]);
  return children(intl.formatNumberToParts(value, formatProps));
};

exports.FormattedNumberParts = FormattedNumberParts;
FormattedNumberParts.displayName = 'FormattedNumberParts';

var FormattedListParts = function (props) {
  var intl = (0, _useIntl.default)();
  var value = props.value,
      children = props.children,
      formatProps = (0, _tslib.__rest)(props, ["value", "children"]);
  return children(intl.formatListToParts(value, formatProps));
};

exports.FormattedListParts = FormattedListParts;
FormattedNumberParts.displayName = 'FormattedNumberParts';

function createFormattedDateTimePartsComponent(name) {
  var ComponentParts = function (props) {
    var intl = (0, _useIntl.default)();
    var value = props.value,
        children = props.children,
        formatProps = (0, _tslib.__rest)(props, ["value", "children"]);
    var date = typeof value === 'string' ? new Date(value || 0) : value;
    var formattedParts = name === 'formatDate' ? intl.formatDateToParts(date, formatProps) : intl.formatTimeToParts(date, formatProps);
    return children(formattedParts);
  };

  ComponentParts.displayName = DisplayNameParts[name];
  return ComponentParts;
}

function createFormattedComponent(name) {
  var Component = function (props) {
    var intl = (0, _useIntl.default)();
    var value = props.value,
        children = props.children,
        formatProps = (0, _tslib.__rest)(props // TODO: fix TS type definition for localeMatcher upstream
    , ["value", "children"]); // TODO: fix TS type definition for localeMatcher upstream

    var formattedValue = intl[name](value, formatProps);

    if (typeof children === 'function') {
      return children(formattedValue);
    }

    var Text = intl.textComponent || React.Fragment;
    return React.createElement(Text, null, formattedValue);
  };

  Component.displayName = DisplayName[name];
  return Component;
}
},{"tslib":"../node_modules/tslib/tslib.es6.js","react":"../node_modules/react/index.js","./useIntl":"../node_modules/react-intl/lib/src/components/useIntl.js"}],"../node_modules/react-intl/lib/src/components/provider.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.createIntl = void 0;

var _tslib = require("tslib");

var React = _interopRequireWildcard(require("react"));

var _injectIntl = require("./injectIntl");

var _utils = require("../utils");

var _intl = require("@formatjs/intl");

var _intlMessageformat = require("intl-messageformat");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

/*
 * Copyright 2015, Yahoo Inc.
 * Copyrights licensed under the New BSD License.
 * See the accompanying LICENSE file for terms.
 */
function processIntlConfig(config) {
  return {
    locale: config.locale,
    timeZone: config.timeZone,
    formats: config.formats,
    textComponent: config.textComponent,
    messages: config.messages,
    defaultLocale: config.defaultLocale,
    defaultFormats: config.defaultFormats,
    onError: config.onError,
    wrapRichTextChunksInFragment: config.wrapRichTextChunksInFragment,
    defaultRichTextElements: config.defaultRichTextElements
  };
}

function assignUniqueKeysToFormatXMLElementFnArgument(values) {
  if (!values) {
    return values;
  }

  return Object.keys(values).reduce(function (acc, k) {
    var v = values[k];
    acc[k] = (0, _intlMessageformat.isFormatXMLElementFn)(v) ? (0, _utils.assignUniqueKeysToParts)(v) : v;
    return acc;
  }, {});
}

var formatMessage = function (config, formatters, descriptor, rawValues) {
  var rest = [];

  for (var _i = 4; _i < arguments.length; _i++) {
    rest[_i - 4] = arguments[_i];
  }

  var values = assignUniqueKeysToFormatXMLElementFnArgument(rawValues);

  var chunks = _intl.formatMessage.apply(void 0, (0, _tslib.__spreadArray)([config, formatters, descriptor, values], rest));

  if (Array.isArray(chunks)) {
    return React.Children.toArray(chunks);
  }

  return chunks;
};
/**
 * Create intl object
 * @param config intl config
 * @param cache cache for formatter instances to prevent memory leak
 */


var createIntl = function (_a, cache) {
  var rawDefaultRichTextElements = _a.defaultRichTextElements,
      config = (0, _tslib.__rest)(_a, ["defaultRichTextElements"]);
  var defaultRichTextElements = assignUniqueKeysToFormatXMLElementFnArgument(rawDefaultRichTextElements);
  var coreIntl = (0, _intl.createIntl)((0, _tslib.__assign)((0, _tslib.__assign)((0, _tslib.__assign)({}, _utils.DEFAULT_INTL_CONFIG), config), {
    defaultRichTextElements: defaultRichTextElements
  }), cache);
  return (0, _tslib.__assign)((0, _tslib.__assign)({}, coreIntl), {
    formatMessage: formatMessage.bind(null, {
      locale: coreIntl.locale,
      timeZone: coreIntl.timeZone,
      formats: coreIntl.formats,
      defaultLocale: coreIntl.defaultLocale,
      defaultFormats: coreIntl.defaultFormats,
      messages: coreIntl.messages,
      onError: coreIntl.onError,
      defaultRichTextElements: defaultRichTextElements
    }, coreIntl.formatters)
  });
};

exports.createIntl = createIntl;

var IntlProvider =
/** @class */
function (_super) {
  (0, _tslib.__extends)(IntlProvider, _super);

  function IntlProvider() {
    var _this = _super !== null && _super.apply(this, arguments) || this;

    _this.cache = (0, _intl.createIntlCache)();
    _this.state = {
      cache: _this.cache,
      intl: createIntl(processIntlConfig(_this.props), _this.cache),
      prevConfig: processIntlConfig(_this.props)
    };
    return _this;
  }

  IntlProvider.getDerivedStateFromProps = function (props, _a) {
    var prevConfig = _a.prevConfig,
        cache = _a.cache;
    var config = processIntlConfig(props);

    if (!(0, _utils.shallowEqual)(prevConfig, config)) {
      return {
        intl: createIntl(config, cache),
        prevConfig: config
      };
    }

    return null;
  };

  IntlProvider.prototype.render = function () {
    (0, _utils.invariantIntlContext)(this.state.intl);
    return React.createElement(_injectIntl.Provider, {
      value: this.state.intl
    }, this.props.children);
  };

  IntlProvider.displayName = 'IntlProvider';
  IntlProvider.defaultProps = _utils.DEFAULT_INTL_CONFIG;
  return IntlProvider;
}(React.PureComponent);

var _default = IntlProvider;
exports.default = _default;
},{"tslib":"../node_modules/tslib/tslib.es6.js","react":"../node_modules/react/index.js","./injectIntl":"../node_modules/react-intl/lib/src/components/injectIntl.js","../utils":"../node_modules/react-intl/lib/src/utils.js","@formatjs/intl":"../node_modules/@formatjs/intl/lib/index.js","intl-messageformat":"../node_modules/intl-messageformat/lib/index.js"}],"../node_modules/react-intl/lib/src/components/relative.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tslib = require("tslib");

var React = _interopRequireWildcard(require("react"));

var _ecma402Abstract = require("@formatjs/ecma402-abstract");

var _useIntl = _interopRequireDefault(require("./useIntl"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

/*
 * Copyright 2015, Yahoo Inc.
 * Copyrights licensed under the New BSD License.
 * See the accompanying LICENSE file for terms.
 */
var MINUTE = 60;
var HOUR = 60 * 60;
var DAY = 60 * 60 * 24;

function selectUnit(seconds) {
  var absValue = Math.abs(seconds);

  if (absValue < MINUTE) {
    return 'second';
  }

  if (absValue < HOUR) {
    return 'minute';
  }

  if (absValue < DAY) {
    return 'hour';
  }

  return 'day';
}

function getDurationInSeconds(unit) {
  switch (unit) {
    case 'second':
      return 1;

    case 'minute':
      return MINUTE;

    case 'hour':
      return HOUR;

    default:
      return DAY;
  }
}

function valueToSeconds(value, unit) {
  if (!value) {
    return 0;
  }

  switch (unit) {
    case 'second':
      return value;

    case 'minute':
      return value * MINUTE;

    default:
      return value * HOUR;
  }
}

var INCREMENTABLE_UNITS = ['second', 'minute', 'hour'];

function canIncrement(unit) {
  if (unit === void 0) {
    unit = 'second';
  }

  return INCREMENTABLE_UNITS.includes(unit);
}

var SimpleFormattedRelativeTime = function (props) {
  var _a = (0, _useIntl.default)(),
      formatRelativeTime = _a.formatRelativeTime,
      Text = _a.textComponent;

  var children = props.children,
      value = props.value,
      unit = props.unit,
      otherProps = (0, _tslib.__rest)(props, ["children", "value", "unit"]);
  var formattedRelativeTime = formatRelativeTime(value || 0, unit, otherProps);

  if (typeof children === 'function') {
    return children(formattedRelativeTime);
  }

  if (Text) {
    return React.createElement(Text, null, formattedRelativeTime);
  }

  return React.createElement(React.Fragment, null, formattedRelativeTime);
};

var FormattedRelativeTime = function (_a) {
  var value = _a.value,
      unit = _a.unit,
      updateIntervalInSeconds = _a.updateIntervalInSeconds,
      otherProps = (0, _tslib.__rest)(_a, ["value", "unit", "updateIntervalInSeconds"]);
  (0, _ecma402Abstract.invariant)(!updateIntervalInSeconds || !!(updateIntervalInSeconds && canIncrement(unit)), 'Cannot schedule update with unit longer than hour');

  var _b = React.useState(),
      prevUnit = _b[0],
      setPrevUnit = _b[1];

  var _c = React.useState(0),
      prevValue = _c[0],
      setPrevValue = _c[1];

  var _d = React.useState(0),
      currentValueInSeconds = _d[0],
      setCurrentValueInSeconds = _d[1];

  var updateTimer;

  if (unit !== prevUnit || value !== prevValue) {
    setPrevValue(value || 0);
    setPrevUnit(unit);
    setCurrentValueInSeconds(canIncrement(unit) ? valueToSeconds(value, unit) : 0);
  }

  React.useEffect(function () {
    function clearUpdateTimer() {
      clearTimeout(updateTimer);
    }

    clearUpdateTimer(); // If there's no interval and we cannot increment this unit, do nothing

    if (!updateIntervalInSeconds || !canIncrement(unit)) {
      return clearUpdateTimer;
    } // Figure out the next interesting time


    var nextValueInSeconds = currentValueInSeconds - updateIntervalInSeconds;
    var nextUnit = selectUnit(nextValueInSeconds); // We've reached the max auto incrementable unit, don't schedule another update

    if (nextUnit === 'day') {
      return clearUpdateTimer;
    }

    var unitDuration = getDurationInSeconds(nextUnit);
    var remainder = nextValueInSeconds % unitDuration;
    var prevInterestingValueInSeconds = nextValueInSeconds - remainder;
    var nextInterestingValueInSeconds = prevInterestingValueInSeconds >= currentValueInSeconds ? prevInterestingValueInSeconds - unitDuration : prevInterestingValueInSeconds;
    var delayInSeconds = Math.abs(nextInterestingValueInSeconds - currentValueInSeconds);

    if (currentValueInSeconds !== nextInterestingValueInSeconds) {
      updateTimer = setTimeout(function () {
        return setCurrentValueInSeconds(nextInterestingValueInSeconds);
      }, delayInSeconds * 1e3);
    }

    return clearUpdateTimer;
  }, [currentValueInSeconds, updateIntervalInSeconds, unit]);
  var currentValue = value || 0;
  var currentUnit = unit;

  if (canIncrement(unit) && typeof currentValueInSeconds === 'number' && updateIntervalInSeconds) {
    currentUnit = selectUnit(currentValueInSeconds);
    var unitDuration = getDurationInSeconds(currentUnit);
    currentValue = Math.round(currentValueInSeconds / unitDuration);
  }

  return React.createElement(SimpleFormattedRelativeTime, (0, _tslib.__assign)({
    value: currentValue,
    unit: currentUnit
  }, otherProps));
};

FormattedRelativeTime.displayName = 'FormattedRelativeTime';
FormattedRelativeTime.defaultProps = {
  value: 0,
  unit: 'second'
};
var _default = FormattedRelativeTime;
exports.default = _default;
},{"tslib":"../node_modules/tslib/tslib.es6.js","react":"../node_modules/react/index.js","@formatjs/ecma402-abstract":"../node_modules/@formatjs/ecma402-abstract/lib/index.js","./useIntl":"../node_modules/react-intl/lib/src/components/useIntl.js"}],"../node_modules/react-intl/lib/src/components/plural.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var React = _interopRequireWildcard(require("react"));

var _useIntl = _interopRequireDefault(require("./useIntl"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

/*
 * Copyright 2015, Yahoo Inc.
 * Copyrights licensed under the New BSD License.
 * See the accompanying LICENSE file for terms.
 */
var FormattedPlural = function (props) {
  var _a = (0, _useIntl.default)(),
      formatPlural = _a.formatPlural,
      Text = _a.textComponent;

  var value = props.value,
      other = props.other,
      children = props.children;
  var pluralCategory = formatPlural(value, props);
  var formattedPlural = props[pluralCategory] || other;

  if (typeof children === 'function') {
    return children(formattedPlural);
  }

  if (Text) {
    return React.createElement(Text, null, formattedPlural);
  } // Work around @types/react where React.FC cannot return string


  return formattedPlural;
};

FormattedPlural.defaultProps = {
  type: 'cardinal'
};
FormattedPlural.displayName = 'FormattedPlural';
var _default = FormattedPlural;
exports.default = _default;
},{"react":"../node_modules/react/index.js","./useIntl":"../node_modules/react-intl/lib/src/components/useIntl.js"}],"../node_modules/react-intl/lib/src/components/message.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tslib = require("tslib");

var React = _interopRequireWildcard(require("react"));

var _useIntl = _interopRequireDefault(require("./useIntl"));

var _utils = require("../utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

/*
 * Copyright 2015, Yahoo Inc.
 * Copyrights licensed under the New BSD License.
 * See the accompanying LICENSE file for terms.
 */
function areEqual(prevProps, nextProps) {
  var values = prevProps.values,
      otherProps = (0, _tslib.__rest)(prevProps, ["values"]);
  var nextValues = nextProps.values,
      nextOtherProps = (0, _tslib.__rest)(nextProps, ["values"]);
  return (0, _utils.shallowEqual)(nextValues, values) && (0, _utils.shallowEqual)(otherProps, nextOtherProps);
}

function FormattedMessage(props) {
  var intl = (0, _useIntl.default)();
  var formatMessage = intl.formatMessage,
      _a = intl.textComponent,
      Text = _a === void 0 ? React.Fragment : _a;
  var id = props.id,
      description = props.description,
      defaultMessage = props.defaultMessage,
      values = props.values,
      children = props.children,
      _b = props.tagName,
      Component = _b === void 0 ? Text : _b,
      ignoreTag = props.ignoreTag;
  var descriptor = {
    id: id,
    description: description,
    defaultMessage: defaultMessage
  };
  var nodes = formatMessage(descriptor, values, {
    ignoreTag: ignoreTag
  });

  if (!Array.isArray(nodes)) {
    nodes = [nodes];
  }

  if (typeof children === 'function') {
    return children(nodes);
  }

  if (Component) {
    return React.createElement(Component, null, React.Children.toArray(nodes));
  }

  return React.createElement(React.Fragment, null, nodes);
}

FormattedMessage.displayName = 'FormattedMessage';
var MemoizedFormattedMessage = React.memo(FormattedMessage, areEqual);
MemoizedFormattedMessage.displayName = 'MemoizedFormattedMessage';
var _default = MemoizedFormattedMessage;
exports.default = _default;
},{"tslib":"../node_modules/tslib/tslib.es6.js","react":"../node_modules/react/index.js","./useIntl":"../node_modules/react-intl/lib/src/components/useIntl.js","../utils":"../node_modules/react-intl/lib/src/utils.js"}],"../node_modules/react-intl/lib/src/components/dateTimeRange.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _tslib = require("tslib");

var React = _interopRequireWildcard(require("react"));

var _useIntl = _interopRequireDefault(require("./useIntl"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var FormattedDateTimeRange = function (props) {
  var intl = (0, _useIntl.default)();
  var from = props.from,
      to = props.to,
      children = props.children,
      formatProps = (0, _tslib.__rest)(props, ["from", "to", "children"]);
  var formattedValue = intl.formatDateTimeRange(from, to, formatProps);

  if (typeof children === 'function') {
    return children(formattedValue);
  }

  var Text = intl.textComponent || React.Fragment;
  return React.createElement(Text, null, formattedValue);
};

FormattedDateTimeRange.displayName = 'FormattedDateTimeRange';
var _default = FormattedDateTimeRange;
exports.default = _default;
},{"tslib":"../node_modules/tslib/tslib.es6.js","react":"../node_modules/react/index.js","./useIntl":"../node_modules/react-intl/lib/src/components/useIntl.js"}],"../node_modules/react-intl/lib/index.js":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.defineMessages = defineMessages;
exports.defineMessage = defineMessage;
Object.defineProperty(exports, "FormattedNumberParts", {
  enumerable: true,
  get: function () {
    return _createFormattedComponent.FormattedNumberParts;
  }
});
Object.defineProperty(exports, "FormattedListParts", {
  enumerable: true,
  get: function () {
    return _createFormattedComponent.FormattedListParts;
  }
});
Object.defineProperty(exports, "createIntlCache", {
  enumerable: true,
  get: function () {
    return _intl.createIntlCache;
  }
});
Object.defineProperty(exports, "UnsupportedFormatterError", {
  enumerable: true,
  get: function () {
    return _intl.UnsupportedFormatterError;
  }
});
Object.defineProperty(exports, "InvalidConfigError", {
  enumerable: true,
  get: function () {
    return _intl.InvalidConfigError;
  }
});
Object.defineProperty(exports, "MissingDataError", {
  enumerable: true,
  get: function () {
    return _intl.MissingDataError;
  }
});
Object.defineProperty(exports, "MessageFormatError", {
  enumerable: true,
  get: function () {
    return _intl.MessageFormatError;
  }
});
Object.defineProperty(exports, "MissingTranslationError", {
  enumerable: true,
  get: function () {
    return _intl.MissingTranslationError;
  }
});
Object.defineProperty(exports, "ReactIntlErrorCode", {
  enumerable: true,
  get: function () {
    return _intl.IntlErrorCode;
  }
});
Object.defineProperty(exports, "ReactIntlError", {
  enumerable: true,
  get: function () {
    return _intl.IntlError;
  }
});
Object.defineProperty(exports, "injectIntl", {
  enumerable: true,
  get: function () {
    return _injectIntl.default;
  }
});
Object.defineProperty(exports, "RawIntlProvider", {
  enumerable: true,
  get: function () {
    return _injectIntl.Provider;
  }
});
Object.defineProperty(exports, "IntlContext", {
  enumerable: true,
  get: function () {
    return _injectIntl.Context;
  }
});
Object.defineProperty(exports, "useIntl", {
  enumerable: true,
  get: function () {
    return _useIntl.default;
  }
});
Object.defineProperty(exports, "IntlProvider", {
  enumerable: true,
  get: function () {
    return _provider.default;
  }
});
Object.defineProperty(exports, "createIntl", {
  enumerable: true,
  get: function () {
    return _provider.createIntl;
  }
});
Object.defineProperty(exports, "FormattedRelativeTime", {
  enumerable: true,
  get: function () {
    return _relative.default;
  }
});
Object.defineProperty(exports, "FormattedPlural", {
  enumerable: true,
  get: function () {
    return _plural.default;
  }
});
Object.defineProperty(exports, "FormattedMessage", {
  enumerable: true,
  get: function () {
    return _message.default;
  }
});
Object.defineProperty(exports, "FormattedDateTimeRange", {
  enumerable: true,
  get: function () {
    return _dateTimeRange.default;
  }
});
exports.FormattedTimeParts = exports.FormattedDateParts = exports.FormattedDisplayName = exports.FormattedList = exports.FormattedNumber = exports.FormattedTime = exports.FormattedDate = void 0;

var _createFormattedComponent = require("./src/components/createFormattedComponent");

var _intl = require("@formatjs/intl");

var _injectIntl = _interopRequireWildcard(require("./src/components/injectIntl"));

var _useIntl = _interopRequireDefault(require("./src/components/useIntl"));

var _provider = _interopRequireWildcard(require("./src/components/provider"));

var _relative = _interopRequireDefault(require("./src/components/relative"));

var _plural = _interopRequireDefault(require("./src/components/plural"));

var _message = _interopRequireDefault(require("./src/components/message"));

var _dateTimeRange = _interopRequireDefault(require("./src/components/dateTimeRange"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function defineMessages(msgs) {
  return msgs;
}

function defineMessage(msg) {
  return msg;
}

// IMPORTANT: Explicit here to prevent api-extractor from outputing `import('./src/types').CustomFormatConfig`
var FormattedDate = (0, _createFormattedComponent.createFormattedComponent)('formatDate');
exports.FormattedDate = FormattedDate;
var FormattedTime = (0, _createFormattedComponent.createFormattedComponent)('formatTime');
exports.FormattedTime = FormattedTime;
var FormattedNumber = (0, _createFormattedComponent.createFormattedComponent)('formatNumber');
exports.FormattedNumber = FormattedNumber;
var FormattedList = (0, _createFormattedComponent.createFormattedComponent)('formatList');
exports.FormattedList = FormattedList;
var FormattedDisplayName = (0, _createFormattedComponent.createFormattedComponent)('formatDisplayName');
exports.FormattedDisplayName = FormattedDisplayName;
var FormattedDateParts = (0, _createFormattedComponent.createFormattedDateTimePartsComponent)('formatDate');
exports.FormattedDateParts = FormattedDateParts;
var FormattedTimeParts = (0, _createFormattedComponent.createFormattedDateTimePartsComponent)('formatTime');
exports.FormattedTimeParts = FormattedTimeParts;
},{"./src/components/createFormattedComponent":"../node_modules/react-intl/lib/src/components/createFormattedComponent.js","@formatjs/intl":"../node_modules/@formatjs/intl/lib/index.js","./src/components/injectIntl":"../node_modules/react-intl/lib/src/components/injectIntl.js","./src/components/useIntl":"../node_modules/react-intl/lib/src/components/useIntl.js","./src/components/provider":"../node_modules/react-intl/lib/src/components/provider.js","./src/components/relative":"../node_modules/react-intl/lib/src/components/relative.js","./src/components/plural":"../node_modules/react-intl/lib/src/components/plural.js","./src/components/message":"../node_modules/react-intl/lib/src/components/message.js","./src/components/dateTimeRange":"../node_modules/react-intl/lib/src/components/dateTimeRange.js"}],"components/pages/IntlPage/IntlPage.tsx":[function(require,module,exports) {
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _context = _interopRequireDefault(require("src/stores/context"));

var _reactIntl = require("react-intl");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var IntlPage = function IntlPage(props) {
  var state = (0, _react.useContext)(_context.default).state;

  var i18n = function i18n(key) {
    console.log(props);
    return props.intl.formatMessage({
      id: key
    });
  };

  return _react.default.createElement("div", null, _react.default.createElement("h1", null, "Hello ", state.userInfo.firstName), _react.default.createElement("div", null, i18n('backHome')));
};

var _default = (0, _reactIntl.injectIntl)(IntlPage);

exports.default = _default;
},{"react":"../node_modules/react/index.js","src/stores/context":"stores/context.tsx","react-intl":"../node_modules/react-intl/lib/index.js"}],"../node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "63474" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../node_modules/parcel-bundler/src/builtins/hmr-runtime.js"], null)
//# sourceMappingURL=/IntlPage.c86df74d.js.map