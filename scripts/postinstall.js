/* eslint-env node */
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fs = require('fs');
/**
 * The two modules below are shipped to the npm registry with modern syntax that
 * does not work in Internet Explorer. Unfortunatly it's very hard to tell parcel
 * to transpile files inside node_modules.
 *
 * WARNING: be careful when updating query-string, some newer versions do not work
 * well with use-query-params.
 *
 * We use to have a local copy, of these two modules in src/ modified by us and
 * aliased in package.json: https://parceljs.org/module_resolution.html#aliases
 *
 * But aliasing was not working in TypeScript / jest (parcel is using babel)...
 *
 * This postinstall script is following tips from
 * https://github.com/parcel-bundler/parcel/issues/1655 in order to force parcel/babel
 * to transpile the two modules.
 *
 * TypeScript/jest work as intended. Maybe parcel v2 will simplify things?
 */
// const modules = ['query-string', 'strict-uri-encode'];
// const source = '.browserslistrc.packages';
// const target = '.browserslistrc';

// modules.forEach((name) => {
//   fs.copyFileSync(source, `node_modules/${name}/${target}`);
//   console.log(`copied ${source} to node_modules/${name}/${target}`);
// });
