import React, { FC } from 'react';
import { Link, LinkProps } from 'react-router-dom';
import styles from './Button.scss';

export interface ButtonBaseProps {
	/** Defines the color scheme and overall look of the button. */
	variant?: 'blue' | 'green' | 'dark' | 'pink' | 'red' | 'subtle' | 'text';

	/** Specifies the dimensions of the button. */
	size?: 'small' | 'medium' | 'large';

	/** Applies padding rules that makes the button a perfect square when used with only one Font Awesome icon as content. */
	iconPadding?: boolean;

	/** Makes the button take up the full width of its parent. */
	fullWidth?: boolean;

	/** Disables the button. */
	disabled?: boolean;
}

export interface ButtonProps extends React.HTMLAttributes<HTMLButtonElement>, ButtonBaseProps {}
export const Button = React.forwardRef<HTMLButtonElement, ButtonProps>(function Button(
	{ variant = 'subtle', size = 'medium', children, iconPadding, fullWidth, ...rest },
	ref
) {
	return (
		<button
			ref={ref}
			className={styles.Button}
			data-variant={variant}
			data-size={size}
			data-icon-padding={iconPadding}
			data-fullwidth={fullWidth}
			{...rest}
		>
			{children}
		</button>
	);
});

export interface LinkButtonProps extends React.AnchorHTMLAttributes<HTMLAnchorElement>, ButtonBaseProps {}
export const LinkButton: FC<LinkButtonProps> = ({
	variant = 'subtle',
	size = 'medium',
	children,
	iconPadding,
	fullWidth,
	...rest
}) => {
	return (
		<a
			className={styles.Button}
			data-variant={variant}
			data-size={size}
			data-icon-padding={iconPadding}
			data-fullwidth={fullWidth}
			{...rest}
		>
			{children}
		</a>
	);
};

export interface RouterLinkButtonProps extends LinkProps, ButtonBaseProps {}
export const RouterLinkButton: FC<RouterLinkButtonProps> = ({
	variant = 'subtle',
	size = 'medium',
	children,
	iconPadding,
	fullWidth,
	...rest
}) => {
	return (
		<Link
			className={styles.Button}
			data-variant={variant}
			data-size={size}
			data-icon-padding={iconPadding}
			data-fullwidth={fullWidth}
			{...rest}
		>
			{children}
		</Link>
	);
};
