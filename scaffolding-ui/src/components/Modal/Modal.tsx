import React from 'react';
import { createPortal } from 'react-dom';
import FocusLock from 'react-focus-lock';
import styles from './Modal.scss';

export interface ModalProps {
	width?: string;
	pureChildren?: boolean;
}

export const Modal: React.FC<ModalProps> = ({ children, width, pureChildren }) => {
	return createPortal(
		<FocusLock returnFocus>
			<div className={styles.background}>
				{pureChildren ? (
					<>{children}</>
				) : (
					<div style={{ maxWidth: width || '760px' }} className={styles.modal}>
						{children}
					</div>
				)}
			</div>
		</FocusLock>,
		document.body
	);
};
