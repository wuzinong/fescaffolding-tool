module.exports = {
	printWidth: 120, // 80 is from the past century
	singleQuote: true,
	trailingComma: 'none', // Improves some git diff
	useTabs: true,
};
