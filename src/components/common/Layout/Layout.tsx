import React, {  useContext } from 'react';
import {Block} from 'scaffolding-ui/src/components/Block/Block';
import GlobalContext from 'src/stores/context';

interface ILayoutProps{
    children:React.ReactNode;
}

const Layout = ({children}:ILayoutProps)=>{
    const { state } = useContext(GlobalContext);
    return <Block>
        <Block>This is the header and user email is {state.userInfo.email}</Block>
        <Block>{children}</Block>
        <Block>This is the footer</Block>
    </Block>
}

export default Layout;