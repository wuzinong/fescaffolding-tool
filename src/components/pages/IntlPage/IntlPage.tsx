import React, { FunctionComponent, useContext, useState } from 'react';
import GlobalContext from 'src/stores/context';
import { injectIntl } from 'react-intl';

const IntlPage: FunctionComponent = (props: any) => {
	const { state } = useContext(GlobalContext);
	const i18n = (key: string) => {
		console.log(props);
		return props.intl.formatMessage({ id: key });
	};
	return (
		<div>
			<h1>Hello {state.userInfo.firstName}</h1>
			<div>{i18n('backHome')}</div>
		</div>
	);
};

export default injectIntl(IntlPage);
