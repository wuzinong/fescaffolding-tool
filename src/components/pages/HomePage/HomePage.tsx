import React, { FunctionComponent, useContext, useState } from 'react';
import GlobalContext from 'src/stores/context';

const HomePage: FunctionComponent = () => {
	const { state } = useContext(GlobalContext);

	return (
		<div>
			This is the home page
			<h1>{state.userInfo.firstName}</h1>
		</div>
	);
};

export default HomePage;
