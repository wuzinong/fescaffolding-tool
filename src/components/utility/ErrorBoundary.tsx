import React from 'react';
import { ErrorBoundary } from 'react-error-boundary';

export interface Props {
	message?: string;
	children: React.ReactNode;
}

export default function CustomErrorBoundary(props: Props) {
	function ErrorFallback() {
		return (
			<div style={{ padding: '30px', background: 'red', color: 'white', fontSize: '17px', width: '100%' }}>
				{props.message || 'Something went wrong.'}
			</div>
		);
	}

	return <ErrorBoundary FallbackComponent={ErrorFallback}>{props.children}</ErrorBoundary>;
}
