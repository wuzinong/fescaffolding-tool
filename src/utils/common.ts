/** Detects if it is an iOS device. Special case for newest iPads. */
export function isIOS() {
  return (
    [
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod'
    ].includes(navigator.platform) ||
    (navigator.userAgent.includes('Mac') && 'ontouchend' in document) // iPad on iOS 13 detection
  );
}

/**
 * Checks whether browser is IE or not
 * Uses trick to check for non-standard (IE) document property by "casting" document to any first
 */
export function isIE() {
  const anyDocument: any = document;
  return anyDocument.documentMode ? true : false;
}
