const ms = {
  month: 31 * 24 * 60 * 60 * 1000,
  day: 24 * 60 * 60 * 1000,
  hour: 60 * 60 * 1000,
  min: 60 * 1000,
  sec: 1000
};

// creates an object with values for each time period based on total miliseconds
export const convertMsToTimePeriods = (totalMs: number) => {
  const floor = (num: number) => Math.floor(num);

  return {
    months: floor(totalMs / ms.month),
    days: floor(totalMs / ms.day) % 31,
    hours: floor(totalMs / ms.hour) % 24,
    mins: floor(totalMs / ms.min) % 60
  };
};

// Convert a UTC string to local time
export const convertUTCToLocalTime = (time: string): string => {
  return new Date(time).toLocaleString(undefined, {
    month: '2-digit',
    day: '2-digit',
    year: 'numeric',
    hour: '2-digit',
    minute: 'numeric',
    second: 'numeric',
    hour12: false
  });
};
