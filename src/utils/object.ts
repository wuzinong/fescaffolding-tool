export const getNested = (object: object, path: string, separator = '.') =>
  path
    .replace('[', separator)
    .replace(']', '')
    .split(separator)
    .reduce((obj: any, prop: string) => obj[prop], object);

export function isEmpty<T>(item: T[] | T | object) {
  if (Array.isArray(item)) {
    return item.length === 0;
  }
  if (typeof item === 'object') {
    return Object.keys(item).length === 0;
  }
  return true;
}

export const omit = (item: any, key: string | number) => {
  if (Array.isArray(item)) {
    return item.filter((it) => it === key);
  }

  if (typeof item === 'object') {
    const { [key]: omittedProp, ...obj } = item;
    return obj;
  }

  return item;
};
