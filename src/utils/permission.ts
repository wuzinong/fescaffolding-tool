export const hasPermission = (
  userPermissions: string[],
  permissions: string[]
) => {
  if (!userPermissions || userPermissions.length === 0) {
    return false;
  }
  const hasPermission: boolean = permissions.some((permission: string) => {
    return userPermissions.indexOf(permission) >= 0;
  });
  return hasPermission;
};
