export const dateColSorter = (a: any, b: any) => {
  let aTimeStamp = 0;
  let bTimeStamp = 0;
  if (a !== null) {
    aTimeStamp = new Date(a).getTime();
  }
  if (b !== null) {
    bTimeStamp = new Date(b).getTime();
  }
  return aTimeStamp - bTimeStamp;
};
