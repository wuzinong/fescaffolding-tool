/**
 * Removes whitespace from both sides. Optionally also removes all consecutive whitespaces
 * @param text - text being trimmed
 * @param removeConsecutive - removes consecutive whitespaces replacing them with single ones
 */
export function trim(text: string, removeConsecutive = false) {
  let trimmedText = text.trim();

  if (removeConsecutive) {
    trimmedText = trimmedText.replace(/\s{2,}/g, ' ');
  }

  return trimmedText;
}

/**
 * Check if value exist in text, if exist, return true, if not exist, return false
 */
export function valueExistInText(value: string, text: string | number) {
  return (
    text.toString().toLowerCase().indexOf(value.toString().toLowerCase()) !== -1
  );
}

/**
 * Transforms the first letter of a string into lower-case, assuming that the text is PascalCase
 */
export function toCamelCase(text: string) {
  return text && text.charAt(0).toLowerCase() + text.slice(1);
}

/**
 * Validates that value is an url
 * @param value data input
 * @returns boolean
 */
export function validateUrl(value: string) {
  try {
    const temp = new URL(value);
    return temp != null;
  } catch (_) {
    return false;
  }
}
