import React from 'react';
import { getCulture } from 'src/utils';
export interface IUser {
	firstName: string;
	lastName: string;
	email: string;
	id: string;
}
export interface IPermission {
	id: string;
	name: string;
	key: string;
	description: string;
	group: string;
}

export interface IRole {
	id: string;
	name: string;
	description: string;
}
export type TPermission = IPermission;
export type TUser = IUser & {
	name: string;
};
export type TRole = IRole & {
	permissions: IPermission[];
};

export type StateType = {
	lang: string;
	antiforgery: {
		header: string;
		token: string;
	};
	userInfo: TUser;
};

export type ActionType = {
	type: string;
	state: StateType;
};

export type StateAndDispatch = {
	state: StateType;
	dispatch?: React.Dispatch<ActionType>;
};

export const UserContext = React.createContext<StateAndDispatch>({
	state: {
		// lang: getCulture(),
		lang: 'en',
		antiforgery: {
			header: '',
			token: ''
		},
		userInfo: {
			id: '',
			firstName: '',
			lastName: '',
			email: '',
			permissions: [],
			roles: [],
			name: ''
		}
	}
});

export default UserContext;
