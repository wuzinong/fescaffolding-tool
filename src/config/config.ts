import { NODE_ENV, REACT_APP_SECRET_ENV } from './env';

export const isDev = NODE_ENV === 'development';
export const isMVP = REACT_APP_SECRET_ENV === 'MVP';
export const baseUrl = process.env.REACT_APP_BASE_URL;
export const apiUrl = process.env.REACT_APP_API_URL;
export const storageKey = 'scaffolding.token';
export const appVersion = process.env.REACT_APP_VERSION;
export const scaffoldingKey = 'a21ddd3c-22e4-4b73-a3e9-1cc93adde3fb';

export const libraryTokens = {
	GA: 'G-FT30WC7SBG',
	Map: 'pk.eyJ1IjoiYnJhbmd1IiwiYSI6ImNranRubXBydzAxNGIycm9idmN3eGozMmcifQ.iZ4IHtJai5upIOx68cM8ag',
	Hotjar: 2205116
};
