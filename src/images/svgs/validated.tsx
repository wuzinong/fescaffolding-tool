
import React from 'react';

const ValidatedIcon = ()=>{
    return <svg  focusable="false"  width="15" height="15" viewBox="0 0 15 15">
    <defs>
        <path id="a" d="M7.583 1.857L3.028 6.412l-1.61-1.61a.195.195 0 0 0-.277 0l-.184.184a.195.195 0 0 0 0 .276L2.89 7.194c.076.077.2.077.276 0l4.877-4.877a.195.195 0 0 0 0-.276l-.184-.184a.195.195 0 0 0-.276 0z"/>
    </defs>
    <g fill="none" fillRule="evenodd">
        <path fill="#6A9C6C" d="M7.5 13.757l-2.113.94-1.27-1.933-2.285-.353-.024-2.312L.076 8.567l1.23-1.957-.628-2.226 2.093-.981.674-2.212 2.292.305L7.5 0l1.763 1.496 2.292-.305.674 2.212 2.093.981-.629 2.226 1.23 1.957-1.731 1.532-.024 2.312-2.285.353-1.27 1.932z"/>
        <use fill="#FFF" transform="translate(3.5 3)" />
    </g>
</svg>

}

export default ValidatedIcon;