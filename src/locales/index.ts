import zh from './zh_CN';
import en from './en_US';

const messages: any = {
  zh,
  en
};

export default messages;
