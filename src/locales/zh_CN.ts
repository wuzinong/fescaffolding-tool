const Zh_CN = {
	dashboard: '控制台',
	newItem: '创建项目',
	page404: '错误页',
	home: '首页',
	allProjects: '所有项目',
	faq: '常见问题',
	privacy: '隐私协议',
	termsOfUse: '用户条款',
	pageNotExist: '抱歉，您所要访问的页面不存在或无权限访问该页面',
	backHome: '返回首页',
	manageAccount: '管理账户',
	loginout: '退出',
	selectLang: '选择语言',
	usermanagement: '用户管理',
	rolemanagement: '角色管理',
	permissionmanagement: '权限管理',
	map: '地图',
	chart: '图表'
};
export default Zh_CN;
