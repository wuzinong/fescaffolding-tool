const en_US = {
	home: 'Home',
	dashboard: 'Dashboard',
	newItem: 'New Item',
	page404: '404 Page',
	allProjects: 'All Projects',
	faq: 'FAQ',
	privacy: 'Privacy',
	termsOfUse: 'Terms of Use',
	pageNotExist: 'Sorry, the page you visited does not exist or you may not have the permission.',
	backHome: 'Back Home',
	manageAccount: 'Manage Account',
	loginout: 'Loginout',
	selectLang: 'Select Lanaguage',
	usermanagement: 'User Management',
	rolemanagement: 'Role Management',
	permissionmanagement: 'Permission Management',
	map: 'Map',
	chart: 'Chart'
};

export default en_US;
