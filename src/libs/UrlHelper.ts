﻿import UrlUtility from 'src/utils/UrlUtility';
import config from 'src/config';

export default class UrlHelper {
	static home = '/';
	static get myHome(): string {
		return config.menuItems.home;
	}
	static preview = (id: string): string => `/preview/${id}`;
}
