﻿import { TrialDuration } from './storeService/ApiResponse';

export default class ApiUrlHelper {
	private _apiRoot: string;

	constructor(apiRoot: string) {
		this._apiRoot = apiRoot;
	}

	get products() {
		return `${this._apiRoot}/api/products`;
	}

	cardById(id: string) {
		return `${this._apiRoot}/api/card/${id}`;
	}
}
