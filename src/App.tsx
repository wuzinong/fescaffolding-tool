import 'core-js/stable';
import React, { lazy, Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Switch, Redirect, BrowserRouter, withRouter, Route } from 'react-router-dom';
import { ConfigProvider, Spin } from 'antd';
import en_US from 'antd/es/locale-provider/en_US';
import zh_CN from 'antd/es/locale-provider/zh_CN';
import { Helmet } from 'react-helmet';
import { IntlProvider } from 'react-intl';
import GlobalContext from 'src/stores/context';
import GlobalProvider from 'src/stores/provider';
import HomePage from './components/pages/HomePage/HomePage';
import messages from 'src/locales/index';
import Layout from 'src/components/common/Layout/Layout';
import ErrorBoundary from 'src/components/utility/ErrorBoundary';
const antdLocales: any = {
	zh: zh_CN,
	en: en_US
};

const IntlPage = lazy(() => import('src/components/pages/IntlPage/IntlPage'));

const App = () => {
	return (
		<BrowserRouter>
			<Helmet>
				<meta charSet="utf-8" />
				<title>Scaffolding App</title>
				<meta name="keywords" content="Scaffolding" />
			</Helmet>
			<Suspense fallback={<div>Loading</div>}>
				<GlobalProvider>
					<GlobalContext.Consumer>
						{(context: any) => {
							const result = (
								<ErrorBoundary>
									<Switch>
										<Redirect exact from="/" to="/Home"></Redirect>
										<Route path="/Home" component={HomePage} />
										<Route path="/Intl" component={IntlPage} />
										<Redirect to="/"></Redirect>
									</Switch>
								</ErrorBoundary>
							);
							//Use this one if need to support Intl
							// const result = (
							// 	<ConfigProvider locale={antdLocales['en' || context.state.lang]}>
							// 		<IntlProvider locale={context.state.lang} messages={messages[context.state.lang]}>
							// 			<Switch>
							// 				<Redirect exact from="/" to="/Home"></Redirect>
							// 				<Route path="/Home" component={HomePage} />
							// 				<Route path="/Intl" component={IntlPage} />
							// 				<Redirect to="/"></Redirect>
							// 			</Switch>
							// 		</IntlProvider>
							// 	</ConfigProvider>
							// );
							return <Layout>{result}</Layout> ;
						}}
					</GlobalContext.Consumer>
				</GlobalProvider>
			</Suspense>
		</BrowserRouter>
	);
};

ReactDOM.render(<App />, document.getElementById('app'));
